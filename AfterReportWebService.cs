﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using EasyFrame.NewCommon.Model;
using System.Web.Services.Protocols;
using System.Data;
using EasyFrame.Redis;
using EasyFrame.NewCommon;
using System.Linq.Expressions;
using EasyFrame.NewCommon.Common;
using EasyFrame.Model;
using System.Reflection;
using System.Text;
using System.Data.SqlClient;
/**************************************************************************

作者: zengzh

日期:2016-10-26

说明:后台报表接口

**************************************************************************/
/// <summary>
///AfterReportWebService 的摘要说明
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
//若要允许使用 ASP.NET AJAX 从脚本中调用此 Web 服务，请取消对下行的注释。 
// [System.Web.Script.Services.ScriptService]
public class AfterReportWebService : System.Web.Services.WebService
{
    /// <summary>
    /// Redis操作帮助类
    /// </summary>
    private RedisHelper redisHelper = new RedisHelper();
    public AfterReportWebService()
    {

        //如果使用设计的组件，请取消注释以下行 
        //InitializeComponent(); 
    }
    public EncryptionSoapHeader encryptionSoapHeader = new EncryptionSoapHeader();
    /// <summary>
    /// 验证权限
    /// </summary>
    /// <returns></returns>
    private string ValidateAuthority()
    {
        string strMsg = string.Empty;
        if (!encryptionSoapHeader.IsValid(out  strMsg))
        {
            return strMsg;
        }
        return strMsg;
    }
    /// <summary>
    /// 资金异动入款表获取条件
    /// </summary>
    /// <param name="redisInAccount">redis_in_account表</param>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strType">类型</param>
    /// <param name="strRecordCode">流水号</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">截止日期</param>
    /// <param name="strUserId">会员ID</param>
    /// <returns></returns>
    private bool GetInAccountCondition(redis_in_account redisInAccount, string strIdentityId, string strType, string strRecordCode, string strStartDate, string strEndDate, string strUserId)
    {
        bool boolResult = true;
        //站长ID
        boolResult &= redisInAccount.identityid == strIdentityId;
        //状态
        boolResult &= redisInAccount.state == 1;
        //类型
        if (!string.IsNullOrWhiteSpace(strType) && strType != "-1")
        {
            if (strType == "3")
            {
                boolResult &= false;
            }
            else
            {
                boolResult &= redisInAccount.type == int.Parse(strType);
            }
        }
        //会员Id
        if (!string.IsNullOrWhiteSpace(strUserId))
        {
            boolResult &= redisInAccount.user_id == int.Parse(strUserId);
        }
        //流水号
        if (!string.IsNullOrWhiteSpace(strRecordCode))
        {
            boolResult &= redisInAccount.record_code == strRecordCode;
        }
        //日期范围查询
        if (!string.IsNullOrWhiteSpace(strStartDate) && !string.IsNullOrWhiteSpace(strEndDate))
        {
            boolResult &= redisInAccount.add_time >= DateTime.Parse(strStartDate) && redisInAccount.add_time <= DateTime.Parse(strEndDate);
        }
        return boolResult;
    }
    /// <summary>
    /// 资金异动出款表获取条件
    /// </summary>
    /// <param name="redisOutAccount">redis_out_account表</param>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strType">类型</param>
    /// <param name="strRecordCode">流水号</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">截止日期</param>
    /// <param name="strUserId">会员ID</param>
    /// <returns></returns>
    private bool GetOutAccountCondition(redis_out_account redisOutAccount, string strIdentityId, string strType, string strRecordCode, string strStartDate, string strEndDate, string strUserId)
    {
        bool boolResult = true;
        //站长ID
        boolResult &= redisOutAccount.identityid == strIdentityId;
        //状态
        boolResult &= redisOutAccount.state == 1;
        boolResult |= redisOutAccount.type == 3;
        //类型
        if (!string.IsNullOrWhiteSpace(strType) && strType != "-1")
        {
            boolResult &= redisOutAccount.type == int.Parse(strType);
        }
        //会员Id
        if (!string.IsNullOrWhiteSpace(strUserId))
        {
            boolResult &= redisOutAccount.user_id == int.Parse(strUserId);
        }
        //流水号
        if (!string.IsNullOrWhiteSpace(strRecordCode))
        {
            boolResult &= redisOutAccount.record_code == strRecordCode;
        }
        //日期范围查询
        if (!string.IsNullOrWhiteSpace(strStartDate) && !string.IsNullOrWhiteSpace(strEndDate))
        {
            boolResult &= redisOutAccount.add_time >= DateTime.Parse(strStartDate) && redisOutAccount.add_time <= DateTime.Parse(strEndDate);
        }
        return boolResult;
    }
    /// <summary>
    /// 获取交易记录查询条件
    /// </summary>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strType">类型</param>
    /// <param name="strRecordCode">流水号</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">截止日期</param>
    /// <param name="strUserId">会员ID</param>
    /// <returns></returns>
    private string GetTransactionWhereSql(string strIdentityId, string strType, string strRecordCode, string strStartDate, string strEndDate, string strUserId)
    {
        string strResult = string.Empty;
        if (!string.IsNullOrWhiteSpace(strIdentityId))
        {
            strResult += " and identityid='" + strIdentityId + "'";
        }
        if (!string.IsNullOrWhiteSpace(strType) && strType != "-1")
        {
            strResult += " and type=" + strType;
        }
        if (!string.IsNullOrWhiteSpace(strRecordCode))
        {
            strResult += " and record_code='" + strRecordCode + "'";
        }
        if (!string.IsNullOrWhiteSpace(strStartDate) && !string.IsNullOrWhiteSpace(strEndDate))
        {
            strResult += " and add_time>='" + DateTime.Parse(strStartDate).ToString("s") + "' and add_time<='" + DateTime.Parse(strEndDate).ToString("s") + "'";
        }
        if (!string.IsNullOrWhiteSpace(strUserId))
        {
            strResult += " and user_id=" + strUserId;
        }
        return strResult;
    }
    /// <summary>
    /// 资金异动入账表的实体类转换成DataTable字典
    /// </summary>
    /// <returns></returns>
    private Dictionary<string, string> GetInAccountPropertyName()
    {
        Dictionary<string, string> dicPropertyName = new Dictionary<string, string>();
        dicPropertyName.Add("InAccountId", "id");
        dicPropertyName.Add("user_id", "user_id");
        dicPropertyName.Add("User_name", "User_name");
        dicPropertyName.Add("record_code", "record_code");
        dicPropertyName.Add("add_time2", "add_time");
        dicPropertyName.Add("type", "type");
        dicPropertyName.Add("typeName", "typeName");
        dicPropertyName.Add("type2", "type2");
        dicPropertyName.Add("money", "in_money");
        dicPropertyName.Add("out_money", "out_money");
        dicPropertyName.Add("state", "state");
        dicPropertyName.Add("after_money", "after_money");
        dicPropertyName.Add("commt", "commt");
        dicPropertyName.Add("identityid", "identityid");
        dicPropertyName.Add("SourceName", "SourceName");
        return dicPropertyName;
    }
    /// <summary>
    /// 资金异动出账表的实体类转换成DataTable字典
    /// </summary>
    /// <returns></returns>
    private Dictionary<string, string> GetOutAccountPropertyName()
    {
        Dictionary<string, string> dicPropertyName = new Dictionary<string, string>();
        dicPropertyName.Add("OutAccountId", "id");
        dicPropertyName.Add("user_id", "user_id");
        dicPropertyName.Add("User_name", "User_name");
        dicPropertyName.Add("record_code", "record_code");
        dicPropertyName.Add("add_time2", "add_time");
        dicPropertyName.Add("type", "type");
        dicPropertyName.Add("typeName", "typeName");
        dicPropertyName.Add("type2", "type2");
        dicPropertyName.Add("in_money", "in_money");
        dicPropertyName.Add("money", "out_money");
        dicPropertyName.Add("state", "state");
        dicPropertyName.Add("after_money", "after_money");
        dicPropertyName.Add("commt", "commt");
        dicPropertyName.Add("identityid", "identityid");
        dicPropertyName.Add("SourceName", "SourceName");
        return dicPropertyName;
    }
    /// <summary>
    /// 合并资金异动入账表
    /// </summary>
    /// <param name="inAccountDataTable">sqlite资金异动入账表</param>
    /// <param name="redisInAccountDataTable">redis资金异动入账表</param>
    /// <returns></returns>
    private DataTable MergeInAccountDataTable(DataTable inAccountDataTable, DataTable redisInAccountDataTable)
    {
        EasyFrame.NewCommon.Common.DataTableHelper dataTableHelper = new EasyFrame.NewCommon.Common.DataTableHelper();
        if (redisInAccountDataTable.Rows.Count > 0)
        {
            inAccountDataTable.Merge(redisInAccountDataTable.Copy());
        }
        if (inAccountDataTable.Rows.Count > 0)
        {
            inAccountDataTable = inAccountDataTable.Select("", "id desc").CopyToDataTable();
        }
        return inAccountDataTable;
    }
    /// <summary>
    /// 获取交易记录
    /// </summary>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strType">类型</param>
    /// <param name="strRecordCode">流水号</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">截止日期</param>
    /// <param name="strUserId">会员ID</param>
    /// <param name="pageIndex">当前页号</param>
    ///<param name="pageSize">每一页多少条记录</param>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public DataSet GetTransactionData(string strIdentityId, string strType, string strRecordCode, string strStartDate, string strEndDate, string strUserId, int pageIndex, int pageSize)
    {
        DataSet dataSet = new DataSet();
        int totalCount = 0;
        try
        {
            //验证是否有权访问  
            string strMsg = ValidateAuthority();
            if (!string.IsNullOrWhiteSpace(strMsg))
            {
                return dataSet;
            }
            Log.Info("查询交易记录开始时间：" + DateTime.Now.ToString());
            int pageCount = pageIndex * pageSize;
            var redisClient = GlobalVariable.cacheRedisClient;
            var redisClientKey = GlobalVariable.cacheRedisClientKey;
            EasyFrame.NewCommon.Common.DataTableHelper dataTableHelper = new EasyFrame.NewCommon.Common.DataTableHelper();
            //Redis资金异动入款表
            Log.Info("获取Redis资金异动入款表开始时间：" + DateTime.Now.ToString());
            Expression<Func<redis_in_account, bool>> inAccountExpression = n => GetInAccountCondition(n, strIdentityId, strType, strRecordCode, strStartDate, strEndDate, strUserId);
            //List<redis_in_account> redisInAccountList = redisClient.GetAll<redis_in_account>().Where(inAccountExpression.Compile()).ToList();
            DateTime dtStartDate = DateTime.Parse(strStartDate);
            DateTime dtEndDate = DateTime.Parse(strEndDate);
            List<redis_in_account> redisInAccountList = redisHelper.Search<redis_in_account>(redisClient, redisClientKey, dtStartDate, dtEndDate, strIdentityId).Where(inAccountExpression.Compile()).ToList();
            Log.Info("获取Redis资金异动入款表结束时间：" + DateTime.Now.ToString());
            int redisInAccountListCount = redisInAccountList.Count;
            totalCount += redisInAccountListCount;
            DataTable dataTable = null;
            Dictionary<string, string> dicInAccountPropertyName = GetInAccountPropertyName();
            //Sqlite资金异动入款表
            Log.Info("获取sqlite资金异动入款表开始时间：" + DateTime.Now.ToString());
            string strWhereSql = GetTransactionWhereSql(strIdentityId, strType, strRecordCode, strStartDate, strEndDate, strUserId);
            string strSql = @"select InAccountId as id, user_id,'' as User_name, record_code, add_time2 AS add_time, type,'' as typeName, type2, Cast(money as varchar(50)) as in_money,
                              '---' as out_money, state, after_money, commt, identityid, SourceName from redis_in_account where state=1 " + strWhereSql;
            GlobalVariable.mSQLiteCommand.CommandText = strSql;
            SQLiteHelper sqliteHelper = new SQLiteHelper();
            DataTable inAccountDataTable = sqliteHelper.ExecuteDataset(GlobalVariable.mSQLiteCommand).Tables[0];
            Log.Info("获取sqlite资金异动入款表结束时间：" + DateTime.Now.ToString());
            int inAccountDataTableCount = inAccountDataTable.Rows.Count;
            totalCount += inAccountDataTableCount;
            inAccountDataTable.Columns["User_name"].DataType = typeof(string);
            inAccountDataTable.Columns["typeName"].DataType = typeof(string);
            inAccountDataTable.Columns["in_money"].DataType = typeof(string);
            inAccountDataTable.Columns["out_money"].DataType = typeof(string);
            //List转换成DataTable
            Log.Info("List转换成DataTable开始时间：" + DateTime.Now.ToString());
            DataTable redisInAccountDataTable = dataTableHelper.ConvertListToDataTableInDictionary<redis_in_account>(redisInAccountList, inAccountDataTable, dicInAccountPropertyName);
            Log.Info("List转换成DataTable结束时间：" + DateTime.Now.ToString());
            //1、Redis资金异动入账表大于分页总记录时，只取Redis资金异动入账表
            bool isPagination = true;
            if (redisInAccountListCount >= pageCount)
            {
                if (redisInAccountDataTable.Rows.Count > 0)
                {
                    redisInAccountDataTable = redisInAccountDataTable.Select("", "add_time desc").CopyToDataTable();
                }
                dataTable = dataTableHelper.SetDataTablePagination(redisInAccountDataTable, pageIndex, pageSize);
                isPagination = false ;
            }
            //2、Redis资金异动入账表+sqlite资金入账表的总数大于分页总记录时，取Redis资金异动入账表+sqlite资金入账表的数据
            if (redisInAccountListCount < pageCount && totalCount >= pageCount)
            {
                var tempInAccountDataTable = MergeInAccountDataTable(inAccountDataTable, redisInAccountDataTable);
                if (tempInAccountDataTable.Rows.Count > 0)
                {
                    tempInAccountDataTable = tempInAccountDataTable.Select("", "add_time desc").CopyToDataTable();
                }
                dataTable = dataTableHelper.SetDataTablePagination(tempInAccountDataTable, pageIndex, pageSize);
                isPagination = false;
            }
            //Redis资金异动出款表
            Log.Info("获取Redis资金异动出款表开始时间：" + DateTime.Now.ToString());
            Expression<Func<redis_out_account, bool>> outAccountExpression = n => GetOutAccountCondition(n, strIdentityId, strType, strRecordCode, strStartDate, strEndDate, strUserId);
            //List<redis_out_account> redisOutAccountList = redisClient.GetAll<redis_out_account>().Where(outAccountExpression.Compile()).ToList();
            List<redis_out_account> redisOutAccountList = redisHelper.Search<redis_out_account>(redisClient, redisClientKey, dtStartDate, dtEndDate, strIdentityId).Where(outAccountExpression.Compile()).ToList();
            Log.Info("获取Redis资金异动出款表结束时间：" + DateTime.Now.ToString());
            int redisOutAccountListCount = redisOutAccountList.Count;
            totalCount += redisOutAccountListCount;
            //Sqlite资金异动出款表
            Log.Info("获取sqlite资金异动出款表开始时间：" + DateTime.Now.ToString());
            strSql = @"select OutAccountId as id, [user_id],'' as User_name, record_code, add_time2 AS add_time, type,'' as typeName, type2,'---' as in_money,
                       Cast([money] as varchar(50)) as out_money, [state], after_money, commt, identityid, SourceName from redis_out_account where (state=1 or type=3) " + strWhereSql;
            GlobalVariable.mSQLiteCommand.CommandText = strSql;
            DataTable outAccountDataTable = sqliteHelper.ExecuteDataset(GlobalVariable.mSQLiteCommand).Tables[0];
            Log.Info("获取sqlite资金异动出款表结束时间：" + DateTime.Now.ToString());
            int outAccountDataTableCount = outAccountDataTable.Rows.Count;
            totalCount += outAccountDataTable.Rows.Count;
            outAccountDataTable.Columns["User_name"].DataType = typeof(string);
            outAccountDataTable.Columns["typeName"].DataType = typeof(string);
            outAccountDataTable.Columns["in_money"].DataType = typeof(string);
            outAccountDataTable.Columns["out_money"].DataType = typeof(string);
            Dictionary<string, string> dicOutAccountPropertyName = GetOutAccountPropertyName();
            //List转换成DataTable
            DataTable redisOutAccountDataTable = dataTableHelper.ConvertListToDataTableInDictionary<redis_out_account>(redisOutAccountList, outAccountDataTable, dicOutAccountPropertyName);
            //3、Redis资金异动入账表+sqlite资金入账表+Redis资金出账表的总数大于分页总记录时，取Redis资金异动入账表+sqlite资金入账表+Redis资金出账表数据
            if (redisInAccountListCount + inAccountDataTableCount < pageCount && redisInAccountListCount + inAccountDataTableCount + redisOutAccountListCount >= pageCount)
            {
                var tempInAccountDataTable = MergeInAccountDataTable(inAccountDataTable, redisInAccountDataTable);
                var tempOutAccountDataTable = MergeInAccountDataTable(tempInAccountDataTable, redisOutAccountDataTable);
                if (tempOutAccountDataTable.Rows.Count > 0)
                {
                    tempOutAccountDataTable = tempOutAccountDataTable.Select("", "add_time desc").CopyToDataTable();
                }
                dataTable = dataTableHelper.SetDataTablePagination(tempOutAccountDataTable, pageIndex, pageSize);
                isPagination = false;
            }
            if (isPagination)
            {
                var tempInAccountDataTable = MergeInAccountDataTable(inAccountDataTable, redisInAccountDataTable);
                var tempOutAccountDataTable = MergeInAccountDataTable(tempInAccountDataTable, redisOutAccountDataTable);
                var tempDataTable = MergeInAccountDataTable(tempOutAccountDataTable, outAccountDataTable);
                if (tempDataTable.Rows.Count > 0)
                {
                    tempDataTable = tempDataTable.Select("", "add_time desc").CopyToDataTable();
                }
                dataTable = dataTableHelper.SetDataTablePagination(tempDataTable, pageIndex, pageSize);
            }
            //保存记录总数量的表
            DataTable totalDataTable = new DataTable();
            DataColumn dataColumn = new DataColumn();
            dataColumn.ColumnName = "totalCount";
            dataColumn.DataType = typeof(Int64);
            totalDataTable.Columns.Add(dataColumn);
            DataRow newDataRow = totalDataTable.NewRow();
            newDataRow["totalCount"] = totalCount;
            totalDataTable.Rows.Add(newDataRow);
            dataSet.Tables.Add(totalDataTable);
            if (dataTable.Rows.Count < 1)
            {
                dataSet.Tables.Add(dataTable);
                return dataSet;
            }
            DataTable dictionDataTable = DBcon.DbHelperSQL.Query("select Code,Name from dt_diction").Tables[0];
            string strUserIdWhereSql = string.Empty;
            Dictionary<string, string> dicUsers = new Dictionary<string, string>();
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                string strTempUserId = dataTable.Rows[i]["user_id"].ToString();
                string strType2 = dataTable.Rows[i]["type2"].ToString();
                string strInMoney = dataTable.Rows[i]["in_money"].ToString();
                //入款
                if (strInMoney != "---" && !string.IsNullOrWhiteSpace(strInMoney))
                {
                    dataTable.Rows[i]["in_money"] = Convert.ToDecimal(strInMoney).ToString("#0.00");
                }
                else
                {
                    dataTable.Rows[i]["in_money"] = "---";
                }
                //出款
                string strOutMoney = dataTable.Rows[i]["out_money"].ToString();
                if (strOutMoney != "---" && !string.IsNullOrWhiteSpace(strOutMoney))
                {
                    dataTable.Rows[i]["out_money"] = Convert.ToDecimal(strOutMoney).ToString("#0.00");
                }
                else
                {
                    dataTable.Rows[i]["out_money"] = "---";
                }
                DataRow[] dataRows = dictionDataTable.Select("Code='" + strType2 + "'");
                if (dataRows == null || dataRows.Length < 1)
                    continue;

                dataTable.Rows[i]["typeName"] = dataRows[0]["Name"].ToString();
                if (!dicUsers.ContainsKey(strTempUserId))
                {
                    dicUsers.Add(strTempUserId, strTempUserId);
                    strUserIdWhereSql += strTempUserId + ",";
                }
            }
            strUserIdWhereSql = strUserIdWhereSql.Substring(0, strUserIdWhereSql.Length - 1);
            DataTable userDataTable = DBcon.DbHelperSQL.Query("select id,user_name from dt_users where id in (" + strUserIdWhereSql + ")").Tables[0];
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                string strTempUserId = dataTable.Rows[i]["user_id"].ToString();
                DataRow[] dataRows = userDataTable.Select("id='" + strTempUserId + "'");
                if (dataRows == null || dataRows.Length < 1)
                    continue;

                dataTable.Rows[i]["User_name"] = dataRows[0]["user_name"].ToString();
            }
            dataSet.Tables.Add(dataTable);
            Log.Info("查询交易记录结束时间：" + DateTime.Now.ToString());
            return dataSet;
        }
        catch (Exception ex)
        {
            Log.Error(ex);
            return null;
        }
    }
    /// <summary>
    /// 投注记录获取条件
    /// </summary>
    /// <param name="redisLotteryBettingInfo">redis_lottery_betting_info实体类</param>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strLotteryCode">彩种编码</param>
    /// <param name="strIssuseNo">投注期号</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">截止日期</param>
    /// <param name="strUserId">会员ID</param>
    /// <param name="strBettingState">投注状态</param>
    /// <returns></returns>
    private bool GetLotteryBettingInfoCondition(redis_lottery_betting_info redisLotteryBettingInfo, string strIdentityId, string strLotteryCode, string strIssuseNo,
                                                string strStartDate, string strEndDate, string strUserId, string strBettingState)
    {
        bool boolResult = true;
        //站长ID
        boolResult &= redisLotteryBettingInfo.identityid == strIdentityId;
        //彩种
        if (!string.IsNullOrWhiteSpace(strLotteryCode) && strLotteryCode != "0000")
        {
            boolResult &= redisLotteryBettingInfo.lottery_code == strLotteryCode;
        }
        //投注期号
        if (!string.IsNullOrWhiteSpace(strIssuseNo))
        {
            boolResult &= redisLotteryBettingInfo.betting_issuseNo == strIssuseNo;
        }
        //会员ID
        if (!string.IsNullOrWhiteSpace(strUserId))
        {
            boolResult &= redisLotteryBettingInfo.user_id == int.Parse(strUserId);
        }
        //投注状态
        if (!string.IsNullOrWhiteSpace(strBettingState) && strBettingState != "0")
        {
            boolResult &= redisLotteryBettingInfo.betting_state == int.Parse(strBettingState) - 1;
        }
        //日期范围查询
        if (!string.IsNullOrWhiteSpace(strStartDate) && !string.IsNullOrWhiteSpace(strEndDate))
        {
            boolResult &= redisLotteryBettingInfo.add_time >= DateTime.Parse(strStartDate) && redisLotteryBettingInfo.add_time <= DateTime.Parse(strEndDate);
        }
        return boolResult;
    }
    /// <summary>
    /// 获取投注记录查询条件
    /// </summary>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strLotteryCode">彩种编码</param>
    /// <param name="strIssuseNo">期号</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">截止日期</param>
    /// <param name="strUserId">会员ID</param>
    /// <param name="strBettingState">投注状态</param>
    /// <returns></returns>
    private string GetBetRecordWhereSql(string strIdentityId, string strLotteryCode, string strIssuseNo, string strStartDate, string strEndDate, string strUserId, string strBettingState)
    {
        string strResult = string.Empty;
        if (!string.IsNullOrWhiteSpace(strIdentityId))
        {
            strResult += " and identityid='" + strIdentityId + "'";
        }
        if (!string.IsNullOrWhiteSpace(strLotteryCode) && strLotteryCode != "0000")
        {
            strResult += " and lottery_code='" + strLotteryCode + "'";
        }
        if (!string.IsNullOrWhiteSpace(strIssuseNo))
        {
            strResult += " and betting_issuseNo='" + strIssuseNo + "'";
        }
        if (!string.IsNullOrWhiteSpace(strUserId))
        {
            strResult += " and user_id=" + strUserId;
        }
        if (!string.IsNullOrWhiteSpace(strBettingState) && strBettingState != "0")
        {
            int iBettingState = int.Parse(strBettingState) - 1;
            strResult += " and betting_state=" + iBettingState;
        }
        if (!string.IsNullOrWhiteSpace(strStartDate) && !string.IsNullOrWhiteSpace(strEndDate))
        {
            strResult += " and add_time>='" + DateTime.Parse(strStartDate).ToString("s") + "' and add_time<='" + DateTime.Parse(strEndDate).ToString("s") + "'";
        }
        return strResult;
    }
    /// <summary>
    /// 获取投注记录查询条件
    /// </summary>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strLotteryCode">彩种编码</param>
    /// <param name="strIssuseNo">期号</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">截止日期</param>
    /// <param name="strUserId">会员ID</param>
    /// <returns></returns>
    private string GetLotteryWinningRecordWhereSql(string strIdentityId, string strLotteryCode, string strIssuseNo, string strStartDate, string strEndDate, string strUserId)
    {
        string strResult = string.Empty;
        if (!string.IsNullOrWhiteSpace(strIdentityId))
        {
            strResult += " and identityid='" + strIdentityId + "'";
        }
        if (!string.IsNullOrWhiteSpace(strLotteryCode) && strLotteryCode != "0000")
        {
            strResult += " and lottery_code='" + strLotteryCode + "'";
        }
        if (!string.IsNullOrWhiteSpace(strIssuseNo))
        {
            strResult += " and issuse_no='" + strIssuseNo + "'";
        }
        if (!string.IsNullOrWhiteSpace(strUserId))
        {
            strResult += " and user_id=" + strUserId;
        }
        if (!string.IsNullOrWhiteSpace(strStartDate) && !string.IsNullOrWhiteSpace(strEndDate))
        {
            strResult += " and add_time>='" + DateTime.Parse(strStartDate).ToString("s") + "' and add_time<='" + DateTime.Parse(strEndDate).ToString("s") + "'";
        }
        return strResult;
    }
    /// <summary>
    /// 获取开奖记录查询条件
    /// </summary>
    /// <param name="strLotteryCode">彩种编码</param>
    /// <param name="strIssuseNo">期号</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">截止日期</param>
    /// <returns></returns>
    private string GetLotteryOpenWhereSql(string strLotteryCode, string strIssuseNo, string strStartDate, string strEndDate)
    {
        string strResult = string.Empty;
        if (!string.IsNullOrWhiteSpace(strLotteryCode) && strLotteryCode != "0000")
        {
            strResult += " and lotterycode='" + strLotteryCode + "'";
        }
        if (!string.IsNullOrWhiteSpace(strIssuseNo))
        {
            strResult += " and issue_no='" + strIssuseNo + "'";
        }
        if (!string.IsNullOrWhiteSpace(strStartDate) && !string.IsNullOrWhiteSpace(strEndDate))
        {
            strResult += " and add_time>='" + DateTime.Parse(strStartDate).ToString("s") + "' and add_time<='" + DateTime.Parse(strEndDate).ToString("s") + "'";
        }
        return strResult;
    }
    /// <summary>
    /// 投注记录的实体类转换成DataTable字典
    /// </summary>
    /// <returns></returns>
    private Dictionary<string, string> GetBetRecordPropertyName()
    {
        Dictionary<string, string> dicPropertyName = new Dictionary<string, string>();
        dicPropertyName.Add("LotteryBettingInfoId", "Id");
        dicPropertyName.Add("identityid", "IdentityId");
        dicPropertyName.Add("user_id", "UserId");
        dicPropertyName.Add("user_name", "UserName");
        dicPropertyName.Add("lottery_code", "LotteryName");
        dicPropertyName.Add("play_detail_code", "PlayName");
        dicPropertyName.Add("betting_issuseNo", "Issue");
        dicPropertyName.Add("betting_number", "BetContent");
        dicPropertyName.Add("open_num", "OpenNum");
        dicPropertyName.Add("betting_money", "BetMoney");
        dicPropertyName.Add("betting_count", "BetCount");
        dicPropertyName.Add("graduation_count", "Multiple");
        dicPropertyName.Add("betting_state", "OpenState");
        dicPropertyName.Add("betting_type", "PlayType");
        dicPropertyName.Add("add_time", "AddTime");
        dicPropertyName.Add("SourceName", "SourceName");
        return dicPropertyName;
    }
    /// <summary>
    /// 投注记录获取条件
    /// </summary>
    /// <param name="redisLotteryBettingInfo">redis_lottery_betting_info实体类</param>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strLotteryCode">彩种编码</param>
    /// <param name="strIssuseNo">投注期号</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">截止日期</param>
    /// <param name="strUserId">会员ID</param>
    /// <param name="strBettingState">投注状态</param>
    /// <returns></returns>
    private bool GetLotteryWinningRecordCondition(redis_lottery_winning_record redisLotteryWinningRecord, string strIdentityId, string strLotteryCode, string strIssuseNo,
                                                string strStartDate, string strEndDate, string strUserId, Dictionary<string, string> dicLotteryCode)
    {
        bool boolResult = true;
        //站长ID
        boolResult &= redisLotteryWinningRecord.identityid == strIdentityId;
        //彩种编码
        boolResult &= dicLotteryCode.ContainsKey(redisLotteryWinningRecord.lottery_code);
        //彩种
        if (!string.IsNullOrWhiteSpace(strLotteryCode) && strLotteryCode != "0000")
        {
            boolResult &= redisLotteryWinningRecord.lottery_code == strLotteryCode;
        }
        //投注期号
        if (!string.IsNullOrWhiteSpace(strIssuseNo))
        {
            boolResult &= redisLotteryWinningRecord.issuse_no == strIssuseNo;
        }
        //会员ID
        if (!string.IsNullOrWhiteSpace(strUserId))
        {
            boolResult &= redisLotteryWinningRecord.user_id == int.Parse(strUserId);
        }
        //日期范围查询
        if (!string.IsNullOrWhiteSpace(strStartDate) && !string.IsNullOrWhiteSpace(strEndDate))
        {
            boolResult &= redisLotteryWinningRecord.add_time >= DateTime.Parse(strStartDate) && redisLotteryWinningRecord.add_time <= DateTime.Parse(strEndDate);
        }
        return boolResult;
    }
    /// <summary>
    /// 投注记录获取条件
    /// </summary>
    /// <param name="redisLotteryOpen">redis_lottery_open实体类</param>
    /// <param name="strIssuseNo">投注期号</param>
    /// <param name="strStartDate">起始日期</param>
    /// <returns></returns>
    private bool GetLotteryOpenCondition(redis_lottery_open redisLotteryOpen, string strStartDate, string strEndDate)
    {
        bool boolResult = true;
        //日期范围查询
        if (!string.IsNullOrWhiteSpace(strStartDate) && !string.IsNullOrWhiteSpace(strEndDate))
        {
            boolResult &= redisLotteryOpen.add_time >= DateTime.Parse(strStartDate) && redisLotteryOpen.add_time <= DateTime.Parse(strEndDate);
        }
        return boolResult;
    }
    /// <summary>
    /// 获取投注记录
    /// </summary>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strLotteryCode">彩种编码</param>
    /// <param name="strIssuseNo">期号</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">截止日期</param>
    /// <param name="strUserId">会员ID</param>
    /// <param name="strBettingState">投注状态</param>
    /// <param name="pageIndex">当前页号</param>
    /// <param name="pageSize">每一页多少条记录</param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public DataSet GetBetRecordData(string strIdentityId, string strLotteryCode, string strIssuseNo, string strStartDate, string strEndDate, string strUserId, string strBettingState, int pageIndex, int pageSize)
    {
        DataSet dataSet = new DataSet();
        int totalCount = 0;
        try
        {
            //验证是否有权访问  
            string strMsg = ValidateAuthority();
            if (!string.IsNullOrWhiteSpace(strMsg))
            {
                return dataSet;
            }
            Log.Info("查询投注记录开始时间：" + DateTime.Now.ToString());
            int pageCount = pageIndex * pageSize;
            var redisClient = GlobalVariable.cacheRedisClient;
            var redisClientKey = GlobalVariable.cacheRedisClientKey;
            EasyFrame.NewCommon.Common.DataTableHelper dataTableHelper = new EasyFrame.NewCommon.Common.DataTableHelper();
            //投注记录表
            Log.Info("获取Redis投注记录开始时间：" + DateTime.Now.ToString());
            Expression<Func<redis_lottery_betting_info, bool>> lotteryBettingInfoExpression = n => GetLotteryBettingInfoCondition(n, strIdentityId, strLotteryCode, strIssuseNo, strStartDate, strEndDate, strUserId, strBettingState);
            //List<redis_lottery_betting_info> redisLotteryBettingInfoList = redisClient.GetAll<redis_lottery_betting_info>().Where(lotteryBettingInfoExpression.Compile()).ToList();
            DateTime dtStartDate = DateTime.Parse(strStartDate);
            DateTime dtEndDate = DateTime.Parse(strEndDate);
            List<redis_lottery_betting_info> redisLotteryBettingInfoList = redisHelper.Search<redis_lottery_betting_info>(redisClient, redisClientKey, dtStartDate, dtEndDate, strIdentityId).Where(lotteryBettingInfoExpression.Compile()).OrderByDescending(m => m.LotteryBettingInfoId).ToList();
            Log.Info("获取Redis投注记录结束时间：" + DateTime.Now.ToString());
            int redisLotteryBettingInfoLisCount = redisLotteryBettingInfoList.Count;
            totalCount += redisLotteryBettingInfoList.Count;
            DataTable lotteryBettingInfoDataTable = null;
            Log.Info("获取sqlite投注记录结束时间：" + DateTime.Now.ToString());
            string strWhereSql = GetBetRecordWhereSql(strIdentityId, strLotteryCode, strIssuseNo, strStartDate, strEndDate, strUserId, strBettingState);
            string strSql = @"select LotteryBettingInfoId as Id,identityid as IdentityId,user_id as UserId,user_name as UserName,lottery_code as LotteryName, 
                                play_detail_code as PlayName,betting_issuseNo as Issue,betting_number as BetContent,Cast(open_num as varchar(100)) as OpenNum,
                                Cast(betting_money as varchar(50)) as BetMoney,betting_count as BetCount,graduation_count as Multiple,Cast(betting_state as varchar(50)) as OpenState,
                                Cast(betting_type as varchar(50)) as PlayType,add_time as AddTime,SourceName from redis_lottery_betting_info where 1=1 " + strWhereSql + " order by LotteryBettingInfoId desc"; ;
            if (redisLotteryBettingInfoLisCount >= pageCount)
            {
                redisLotteryBettingInfoList = redisLotteryBettingInfoList.OrderByDescending(m => m.id).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
            }
            GlobalVariable.mSQLiteCommand.CommandText = strSql;
            SQLiteHelper sqliteHelper = new SQLiteHelper();
            lotteryBettingInfoDataTable = sqliteHelper.ExecuteDataset(GlobalVariable.mSQLiteCommand).Tables[0];
            Log.Info("获取sqlite投注记录结束时间：" + DateTime.Now.ToString());
            totalCount += lotteryBettingInfoDataTable.Rows.Count;
            Dictionary<string, string> dicLotteryBettingInfoPropertyName = GetBetRecordPropertyName();
            //List转换成DataTable
            Log.Info("/List转换成DataTable开始时间：" + DateTime.Now.ToString());
            DataTable redisLotteryBettingInfoDataTable = dataTableHelper.ConvertListToDataTableInDictionary<redis_lottery_betting_info>(redisLotteryBettingInfoList, lotteryBettingInfoDataTable, dicLotteryBettingInfoPropertyName);
            Log.Info("/List转换成DataTable结束时间：" + DateTime.Now.ToString());
            DataTable dataTable = null;
            if (redisLotteryBettingInfoDataTable.Rows.Count > 0 && redisLotteryBettingInfoLisCount < pageCount)
            {
                lotteryBettingInfoDataTable.Merge(redisLotteryBettingInfoDataTable.Copy());
                if (lotteryBettingInfoDataTable.Rows.Count > 0)
                {
                    lotteryBettingInfoDataTable = lotteryBettingInfoDataTable.Select("", "Id desc").CopyToDataTable();
                }
                lotteryBettingInfoDataTable = dataTableHelper.SetDataTablePagination(lotteryBettingInfoDataTable, pageIndex, pageSize);
                dataTable = lotteryBettingInfoDataTable;
            }
            else
            {
                dataTable = redisLotteryBettingInfoDataTable;
                if (dataTable.Rows.Count > 0)
                {
                    dataTable = dataTable.Select("", "Id desc").CopyToDataTable();
                }
            }
            //保存记录总数量的表
            DataTable totalDataTable = new DataTable();
            DataColumn dataColumn = new DataColumn();
            dataColumn.ColumnName = "totalCount";
            dataColumn.DataType = typeof(Int64);
            totalDataTable.Columns.Add(dataColumn);
            DataRow newDataRow = totalDataTable.NewRow();
            newDataRow["totalCount"] = totalCount;
            totalDataTable.Rows.Add(newDataRow);
            dataSet.Tables.Add(totalDataTable);
            if (dataTable.Rows.Count < 1)
            {
                dataSet.Tables.Add(dataTable);
                return dataSet;
            }
            Dictionary<string, string> dicLotteryCode = new Dictionary<string, string>();
            string strPayCodeWhereSql = string.Empty;
            string strUserWhereSql = string.Empty;
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                string strPlayName = dataTable.Rows[i]["PlayName"].ToString();
                strPayCodeWhereSql += "'" + strPlayName + "',";
                string tempUserId = dataTable.Rows[i]["UserId"].ToString();
                strUserWhereSql += tempUserId + ",";
                string tempLotteryCode = dataTable.Rows[i]["LotteryName"].ToString();
                if (dicLotteryCode.ContainsKey(tempLotteryCode))
                    continue;

                dicLotteryCode.Add(tempLotteryCode, tempLotteryCode);
            }
            //中奖记录表
            Log.Info("获取中奖记录表开始时间：" + DateTime.Now.ToString());
            Expression<Func<redis_lottery_winning_record, bool>> lotteryWinningRecordExpression = n => GetLotteryWinningRecordCondition(n, strIdentityId, strLotteryCode, strIssuseNo, strStartDate, strEndDate, strUserId, dicLotteryCode);
            //List<redis_lottery_winning_record> redisLotteryWinningRecordList = redisClient.GetAll<redis_lottery_winning_record>().Where(lotteryWinningRecordExpression.Compile()).ToList();
            List<redis_lottery_winning_record> redisLotteryWinningRecordList = redisHelper.Search<redis_lottery_winning_record>(redisClient, redisClientKey, dtStartDate, dtEndDate, strIdentityId).Where(lotteryWinningRecordExpression.Compile()).ToList();
            Log.Info("获取中奖记录表结束时间：" + DateTime.Now.ToString());
            //strLotteryCodeWhereSql = strLotteryCodeWhereSql.Substring(0, strLotteryCodeWhereSql.Length - 1);
            //string strLotteryWinningRecordWhereSql = GetLotteryWinningRecordWhereSql(strIdentityId,strLotteryCode, strIssuseNo, strStartDate, strEndDate,strUserId);
            //strSql = @"select * from redis_lottery_winning_record where 1=1 "+strLotteryWinningRecordWhereSql;
            //GlobalVariable.mSQLiteCommand.CommandText = strSql;
            //DataTable sqliteWinningRecordDataTable = sqliteHelper.ExecuteDataset(GlobalVariable.mSQLiteCommand).Tables[0];
            //DataTable redisWinningRecordDataTable= dataTableHelper.ConvertListToDataTable(redisLotteryWinningRecordList);
            //if (redisWinningRecordDataTable.Rows.Count > 0)
            //{
            //    sqliteWinningRecordDataTable.Merge(redisWinningRecordDataTable.Copy());
            //}
            //开奖记录表
            Log.Info("获取开奖记录表开始时间：" + DateTime.Now.ToString());
            Expression<Func<redis_lottery_open, bool>> lotteryOpenExpression = n => GetLotteryOpenCondition(n, strStartDate, strEndDate);
            //List<redis_lottery_open> redisLotteryOpenList = redisClient.GetAll<redis_lottery_open>().Where(lotteryOpenExpression.Compile()).ToList();
            List<redis_lottery_open> redisLotteryOpenList = redisHelper.Search<redis_lottery_open>(redisClient, redisClientKey, dtStartDate, dtEndDate, "", "").Where(m => dicLotteryCode.ContainsKey(m.lotterycode)).ToList();
            Log.Info("获取开奖记录表结束时间：" + DateTime.Now.ToString());
            //string strLotteryOpenWhereSql = GetLotteryOpenWhereSql(strLotteryCode, strIssuseNo, strStartDate, strEndDate);
            //strSql = @"select * from redis_lottery_open where lotterycode in ( " + strLotteryCodeWhereSql + ")";
            //GlobalVariable.mSQLiteCommand.CommandText = strSql;
            //DataTable sqliteLotteryOpenDataTable = sqliteHelper.ExecuteDataset(GlobalVariable.mSQLiteCommand).Tables[0];
            //DataTable redisLotteryOpenDataTable = dataTableHelper.ConvertListToDataTable(redisLotteryOpenList);
            //if (redisLotteryOpenDataTable.Rows.Count > 0)
            //{
            //    sqliteLotteryOpenDataTable.Merge(redisLotteryOpenDataTable.Copy());
            //}
            strPayCodeWhereSql = strPayCodeWhereSql.Substring(0, strPayCodeWhereSql.Length - 1);
            strUserWhereSql += strUserWhereSql.Substring(0, strUserWhereSql.Length - 1);
            Log.Info("获取玩法说明开始时间：" + DateTime.Now.ToString());
            DataTable lotteryPlayDetailDataTable = DBcon.DbHelperSQL.Query("select * from dt_lottery_play_detail where play_code in(" + strPayCodeWhereSql + ")").Tables[0];
            Log.Info("获取玩法说明结束时间：" + DateTime.Now.ToString());
            Log.Info("获取用户档案开始时间：" + DateTime.Now.ToString());
            DataTable userDataTable = DBcon.DbHelperSQL.Query("select id,user_name from dt_users where id in (" + strUserWhereSql + ")").Tables[0];
            Log.Info("获取用户档案结束时间：" + DateTime.Now.ToString());
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                string strBetId = dataTable.Rows[i]["Id"].ToString();
                string strPlayType = dataTable.Rows[i]["PlayType"].ToString();
                int intOpenState = Convert.ToInt32(dataTable.Rows[i]["OpenState"]);
                string strPlayName = dataTable.Rows[i]["PlayName"].ToString();
                string strLotteryName = dataTable.Rows[i]["LotteryName"].ToString();
                string strIssue = dataTable.Rows[i]["Issue"].ToString();
                string tempUserId = dataTable.Rows[i]["UserId"].ToString();
                //玩法类型
                if (strPlayType == "1")
                {
                    dataTable.Rows[i]["BetMoney"] = Convert.ToDecimal(dataTable.Rows[i]["BetMoney"]).ToString("#0.00");
                    dataTable.Rows[i]["PlayType"] = "普通";
                }
                else
                {
                    dataTable.Rows[i]["BetMoney"] = (Convert.ToDecimal(dataTable.Rows[i]["BetMoney"]) * Convert.ToInt32(dataTable.Rows[i]["Multiple"])).ToString("#0.00");
                    dataTable.Rows[i]["PlayType"] = "追号";
                }
                List<redis_lottery_open> tempLotteryOpenList = null;
                DataTable sqliteLotteryOpenDataTable = null;
                //DataRow[] lotteryOpenDataRow = sqliteLotteryOpenDataTable.Select("lotterycode='" + strLotteryName + "' and issue_no='" + strIssue + "'");
                //开奖状态
                switch (intOpenState)
                {
                    case 0:
                        dataTable.Rows[i]["OpenState"] = "等待开奖".ToString();
                        dataTable.Rows[i]["OpenNum"] = "---";
                        break;
                    case 1:
                        dataTable.Rows[i]["OpenState"] = "未中奖".ToString();
                        tempLotteryOpenList = redisLotteryOpenList.Where(m => m.lotterycode == strLotteryName && m.issue_no == strIssue).ToList();
                        if (tempLotteryOpenList.Count < 1)
                        {
                            strSql = @"select * from redis_lottery_open where lotterycode='" + strLotteryName + "' and issue_no='" + strIssue + "'";
                            GlobalVariable.mSQLiteCommand.CommandText = strSql;
                            sqliteLotteryOpenDataTable = sqliteHelper.ExecuteDataset(GlobalVariable.mSQLiteCommand).Tables[0];
                            if (sqliteLotteryOpenDataTable != null && sqliteLotteryOpenDataTable.Rows.Count > 0)
                            {
                                dataTable.Rows[i]["OpenNum"] = sqliteLotteryOpenDataTable.Rows[0]["lotteryopen_no"];
                            }
                            else
                            {
                                dataTable.Rows[i]["OpenNum"] = "---";
                            }
                        }
                        else
                        {

                            dataTable.Rows[i]["OpenNum"] = tempLotteryOpenList[0].lotteryopen_no;
                        }
                        break;
                    case 2:
                        List<redis_lottery_winning_record> tempLotteryWinningRecordList = redisLotteryWinningRecordList.Where(m => m.betting_id == int.Parse(strBetId)).ToList();
                        if (tempLotteryWinningRecordList.Count < 1)
                        {
                            strSql = @"select * from redis_lottery_winning_record where betting_id=" + strBetId;
                            GlobalVariable.mSQLiteCommand.CommandText = strSql;
                            DataTable sqliteWinningRecordDataTable = sqliteHelper.ExecuteDataset(GlobalVariable.mSQLiteCommand).Tables[0];
                            if (sqliteWinningRecordDataTable != null && sqliteWinningRecordDataTable.Rows.Count > 0)
                            {
                                dataTable.Rows[i]["OpenState"] = decimal.Parse(sqliteWinningRecordDataTable.Rows[0]["bonus_money"].ToString()).ToString("0.00");
                            }
                            else
                            {
                                dataTable.Rows[i]["OpenState"] = "已中奖";
                            }
                        }
                        else
                        {
                            dataTable.Rows[i]["OpenState"] = tempLotteryWinningRecordList[0].bonus_money.ToString("0.00");
                        }
                        tempLotteryOpenList = redisLotteryOpenList.Where(m => m.lotterycode == strLotteryName && m.issue_no == strIssue).ToList();
                        if (tempLotteryOpenList.Count < 1)
                        {
                            strSql = @"select * from redis_lottery_open where lotterycode='" + strLotteryName + "' and issue_no='" + strIssue + "'";
                            GlobalVariable.mSQLiteCommand.CommandText = strSql;
                            sqliteLotteryOpenDataTable = sqliteHelper.ExecuteDataset(GlobalVariable.mSQLiteCommand).Tables[0];
                            if (sqliteLotteryOpenDataTable != null && sqliteLotteryOpenDataTable.Rows.Count > 0)
                            {
                                dataTable.Rows[i]["OpenNum"] = sqliteLotteryOpenDataTable.Rows[0]["lotteryopen_no"];
                            }
                            else
                            {
                                dataTable.Rows[i]["OpenNum"] = "---";
                            }
                        }
                        else
                        {
                            dataTable.Rows[i]["OpenNum"] = tempLotteryOpenList[0].lotteryopen_no;
                        }
                        break;
                    case 3:
                        dataTable.Rows[i]["OpenState"] = "已撤单";
                        dataTable.Rows[i]["OpenNum"] = "---";
                        break;
                    default:
                        dataTable.Rows[i]["OpenState"] = "未知";
                        dataTable.Rows[i]["OpenNum"] = "---";
                        break;
                }
                DataRow[] playDetailDataRows = lotteryPlayDetailDataTable.Select("play_code='" + strPlayName + "'", "");
                if (playDetailDataRows != null && playDetailDataRows.Length > 0)
                {
                    string strPlayDetailName = playDetailDataRows[0]["play_detail_name"].ToString();
                    int index = strPlayDetailName.IndexOf(",");
                    if (index != -1)
                    {
                        dataTable.Rows[i]["LotteryName"] = strPlayDetailName.Substring(0, index);
                        dataTable.Rows[i]["PlayName"] = strPlayDetailName.Substring(index + 1);
                    }
                }
                //会员名称
                DataRow[] userDataRows = userDataTable.Select("id=" + tempUserId);
                if (userDataRows != null && userDataRows.Length > 0)
                {
                    dataTable.Rows[i]["UserName"] = userDataRows[0]["user_name"].ToString();
                }
            }
            dataSet.Tables.Add(dataTable);
            Log.Info("查询投注记录结束时间：" + DateTime.Now.ToString());
            return dataSet;
        }
        catch (Exception ex)
        {
            Log.Error(ex);
            return null;
        }
    }
    /// <summary>
    /// 获取条件
    /// </summary>
    /// <param name="reportInOutAccount"></param>
    /// <param name="listUserId">会员Id的条件集合</param>
    /// <returns></returns>
    private bool GetReportIndexCondition(dt_report_inoutaccount reportInOutAccount, string strIdentityId, string strStartDate, string strEndDate)
    {
        bool boolResult = true;
        //站长ID
        boolResult &= reportInOutAccount.identityid == strIdentityId;
        //日期范围查询
        if (!string.IsNullOrWhiteSpace(strStartDate) && !string.IsNullOrWhiteSpace(strEndDate))
        {
            boolResult &= reportInOutAccount.AddTime >= DateTime.Parse(strStartDate) && reportInOutAccount.AddTime <= DateTime.Parse(strEndDate);
        }
        return boolResult;
    }
    /// <summary>
    /// 次数报表获取条件
    /// </summary>
    /// <param name="reportNum"></param>
    /// <param name="listUserId">会员Id的条件集合</param>
    /// <returns></returns>
    private bool GetReportIndexReportNumCondition(dt_report_num reportNum, string strIdentityId, string strStartDate, string strEndDate)
    {
        bool boolResult = true;
        //站长ID
        boolResult &= reportNum.identityid == strIdentityId;
        //日期范围查询
        if (!string.IsNullOrWhiteSpace(strStartDate) && !string.IsNullOrWhiteSpace(strEndDate))
        {
            boolResult &= reportNum.AddTime >= DateTime.Parse(strStartDate) && reportNum.AddTime <= DateTime.Parse(strEndDate);
        }
        return boolResult;
    }
    /// <summary>
    /// 首充报表获取条件
    /// </summary>
    /// <param name="reportNewPrepaid"></param>
    /// <param name="listUserId">会员Id的条件集合</param>
    /// <returns></returns>
    private bool GetReportIndexReportNewPrepaidCondition(dt_report_newprepaid reportNewPrepaid, string strIdentityId, string strStartDate, string strEndDate)
    {
        bool boolResult = true;
        //站长ID
        boolResult &= reportNewPrepaid.identityid == strIdentityId;
        //日期范围查询
        if (!string.IsNullOrWhiteSpace(strStartDate) && !string.IsNullOrWhiteSpace(strEndDate))
        {
            boolResult &= reportNewPrepaid.AddTime >= DateTime.Parse(strStartDate) && reportNewPrepaid.AddTime <= DateTime.Parse(strEndDate);
        }
        return boolResult;
    }
    /// <summary>
    /// 在线人员获取条件
    /// </summary>
    /// <param name="reportNewPrepaid"></param>
    /// <param name="listUserId">会员Id的条件集合</param>
    /// <returns></returns>
    private bool GetReportIndexOnLineCondition(EasyFrame.NewCommon.Model.dt_sys_online sysOnline, string strIdentityId)
    {
        bool boolResult = true;
        //站长ID
        boolResult &= sysOnline.identityid == strIdentityId;
        TimeSpan timeSpan = DateTime.Now - sysOnline.login_time;
        boolResult &= timeSpan.TotalMinutes <= 5;
        return boolResult;
    }
    /// <summary>
    /// 获取特定字符串中的金额 格式：如0.00/0人
    /// </summary>
    /// <param name="strMoney">字符串</param>
    /// <returns></returns>
    public decimal GetMoney(object bjMoney)
    {
        if (bjMoney == null || string.IsNullOrWhiteSpace(bjMoney.ToString()))
            return 0;

        string strMoney = bjMoney.ToString();
        decimal fMoney = 0;
        int index = strMoney.IndexOf("/");
        // string tempMoney = strMoney.Substring(0, index);
        string tempMoney = strMoney;
        fMoney = decimal.Parse(tempMoney);
        return fMoney;
    }
    /// <summary>
    /// 获取特定字符串中的人数 格式：如0.00/0人
    /// </summary>
    /// <param name="strNum">字符串</param>
    /// <returns></returns>
    public int GetNum(object bjNum)
    {
        if (bjNum == null || string.IsNullOrWhiteSpace(bjNum.ToString()))
            return 0;

        string strNum = bjNum.ToString();
        int iNum = 0;
        int index = strNum.IndexOf("/");
        string tempNum = strNum.Substring(index + 1);
        iNum = int.Parse(tempNum.Substring(0, tempNum.Length - 1));
        return iNum;
    }
    /// <summary>
    /// 实体类赋值
    /// </summary>
    /// <param name="sourceEntity">源实体类</param>
    /// <param name="purposeEntity">目的实体类</param>
    /// <param name="dicUsers">人数统计校验字典</param>
    /// <param name="strUserId">会员Id</param>
    /// <param name="strCombination">实体类字段名称比较时拼接的字符串</param>
    /// <param name="isInt">是否整数</param>
    private void SetEntityValue(object sourceEntity, object purposeEntity, Dictionary<string, int> dicUsers, Dictionary<string, int> dicNum, string strUserId, string strCombination = "", bool isInt = false)
    {
        PropertyInfo[] sourcePropertyInfos = sourceEntity.GetType().GetProperties();
        PropertyInfo[] purposePropertyInfos = purposeEntity.GetType().GetProperties();
        foreach (PropertyInfo sourcePropertyInfo in sourcePropertyInfos)
        {
            foreach (PropertyInfo purposePropertyInfo in purposePropertyInfos)
            {
                if (strCombination + sourcePropertyInfo.Name != purposePropertyInfo.Name)
                    continue;

                decimal fMoney = decimal.Parse(sourcePropertyInfo.GetValue(sourceEntity, null).ToString());
                //人数
                int iNum = 0;
                object bjPurposeValue = purposePropertyInfo.GetValue(purposeEntity, null);
                if (fMoney > 0)
                {
                    if (!dicUsers.ContainsKey(strUserId + "_" + purposePropertyInfo.Name))
                    {
                        iNum++;
                        dicUsers.Add(strUserId + "_" + purposePropertyInfo.Name, iNum);
                        if (!dicNum.ContainsKey(purposePropertyInfo.Name))
                        {
                            dicNum.Add(purposePropertyInfo.Name, 1);
                        }
                        else
                        {
                            dicNum[purposePropertyInfo.Name]++;
                        }
                    }
                }
                if (dicNum.ContainsKey(purposePropertyInfo.Name))
                {
                    iNum = dicNum[purposePropertyInfo.Name];
                }
                if (bjPurposeValue != null && !string.IsNullOrWhiteSpace(bjPurposeValue.ToString()))
                {
                    //根据文本格式进行处理计算得出值
                    DealValue(bjPurposeValue.ToString(), ref fMoney, ref  iNum);
                }
                if (isInt)
                {
                    purposePropertyInfo.SetValue(purposeEntity, fMoney.ToString("0") + "/" + iNum + "人", null);
                    continue;
                }
                purposePropertyInfo.SetValue(purposeEntity, fMoney.ToString("0.00") + "/" + iNum + "人", null);
            }
        }
    }
    /// <summary>
    /// 根据文本格式进行处理计算得出值
    /// </summary>
    /// <param name="strValue">文本，格式如：100.00/12人 </param>
    /// <param name="fMoney">金额</param>
    /// <param name="iNum">人数</param>
    private void DealValue(string strValue, ref decimal fMoney, ref int iNum)
    {
        int index = strValue.IndexOf("/");
        fMoney += decimal.Parse(strValue.Substring(0, index));
        string strTempValue = strValue.Substring(index + 1);
        //index = strTempValue.IndexOf("人");
        //iNum += int.Parse(strTempValue.Substring(0, index));
    }
    /// <summary>
    /// 计算抽成
    /// 抽成计算方式：进阶递减
    ///损益每超200万，超出的部分降1%
    ///1000万以上，超出的部分 按5%计算
    ///示例：
    ///损益250万 抽成计算公式200*0.1+50*0.09=24.5万
    ///损益450万 抽成计算公式200*0.1+200*0.09+50*0.08 = 42万
    ///损益650万 抽成计算公式200*0.1+200*0.09+200*0.08+50*0.07=57.5万
    ///损益850万 抽成计算公式200*0.1+200*0.09+200*0.08+200*0.07+50*0.06=71万
    ///损益1050万抽成计算公式200*0.1+200*0.09+200*0.08+200*0.07+200*0.06+50*0.05=82.5万
    /// </summary>
    /// <param name="fProfitAndLoss"></param>
    /// <returns></returns>
    public decimal ComputeTakePercentage(decimal fProfitAndLoss)
    {
        decimal fBase = 2000000;
        if (fProfitAndLoss < 0)
        {
            fProfitAndLoss = 0;
        }
        if (fProfitAndLoss <= fBase)
        {
            return fProfitAndLoss * (decimal)0.1;
        }
        int i = (int)(fProfitAndLoss / fBase);
        decimal fMoney = (decimal)fBase * (decimal)0.1;
        decimal fRemainder = 0;
        for (int k = 0; k < i; k++)
        {
            decimal fTempRemainder = fProfitAndLoss - fRemainder - fBase;
            fMoney += fTempRemainder * (decimal)(0.1 - (k + 1) * 0.01);
            fRemainder += fTempRemainder;
        }
        return fMoney;
    }
    /// <summary>
    /// 获取综合报表数据
    /// </summary>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">截止日期</param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public DataSet GetReportIndexData(string strIdentityId, string strStartDate, string strEndDate)
    {
        string conn_str = "Data Source=13.75.93.215;uid=report2;pwd=123456@qq.com;database=dafacloud;Connect Timeout=30000;";
        DataTableHelper dataTableHelper = new DataTableHelper();
        DataSet dataSet = new DataSet();
        DataTable dt_clod = new DataTable();
        List<ReportSummary> listReportSummary = new List<ReportSummary>();
        List<ReportSummary_new> listReportSummary_new = new List<ReportSummary_new>();
        ReportSummary reportSummary = new ReportSummary();
        ReportSummary_new resum_new = new ReportSummary_new();
        try
        {
            DataTable dt_sum = new DataTable();
            DataTableHelper dth = new DataTableHelper();

            //判斷時間是否要切割
            //起始時間要查冷庫資料 且 結束時間要查熱庫資料
            if (Convert.ToDateTime(strStartDate) < System.DateTime.Now.AddDays(-1).Date && Convert.ToDateTime(strEndDate) > System.DateTime.Now.AddDays(-1).Date)
            {
                //dt_clod
                string selectclod_exec = "exec [dbo].[dsp_selectclod_ReportSummary] @identityid ='" + strIdentityId + "',@clod_maxday='" + strStartDate + "',@hot_minday='" + strEndDate + "'";
                SqlConnection conn = new SqlConnection(conn_str);
                dt_clod = DBcon.DbHelperSQL.Query(conn, selectclod_exec);
                //dt_hot
                string selecthot_exec = "exec [dbo].[dsp_selecthot_ReportSummary] @identityid ='" + strIdentityId + "',@clod_maxday='" + strStartDate + "',@hot_minday='" + strEndDate + "'";
                SqlDataAdapter da_hot = new SqlDataAdapter(selecthot_exec, conn);
                conn.Open();
                DataTable dt_hot = DBcon.DbHelperSQL.Query(conn, selecthot_exec);
                dt_clod.Merge(dt_hot);
                //sum
                
  
                for (int i = 0; i < dt_clod.Rows.Count; i++)
                {
              
                    Dictionary<string, PropertyInfo> dicPropertyInfo = new Dictionary<string, PropertyInfo>();
                    PropertyInfo[] propertyInfos = resum_new.GetType().GetProperties();
                    foreach (PropertyInfo propertyInfo in propertyInfos)
                    {
                        if (!dicPropertyInfo.ContainsKey(propertyInfo.Name))
                        {
                            dicPropertyInfo.Add(propertyInfo.Name, propertyInfo);
                        }
                    }
                    for (int j = 0; j < dt_clod.Rows[i].Table.Columns.Count; j++)
                    {

                        string strColumnName = dt_clod.Rows[i].Table.Columns[j].ColumnName;
                        if (!dicPropertyInfo.ContainsKey(strColumnName))
                            continue;

                        decimal ovalue =  Convert.ToDecimal(resum_new.GetType().GetProperties()[j].GetValue(resum_new, null));
                        object bjValue = dt_clod.Rows[i][strColumnName];
                        if (bjValue == null || bjValue is DBNull|| dt_clod.Rows[i][strColumnName] == null)
                            continue;
                        decimal bjValue2 = Convert.ToDecimal(dt_clod.Rows[i][strColumnName])+ ovalue;
                        dicPropertyInfo[strColumnName].SetValue(resum_new, bjValue2, null);
                    }
                   
                }
            }
            //起始時間查冷庫資料 且 結束時間要查冷庫
            else if (Convert.ToDateTime(strStartDate) < System.DateTime.Now.AddDays(-1).Date && Convert.ToDateTime(strEndDate) <= System.DateTime.Now.AddDays(-1).Date)
            {
                string select_exec = "exec [dbo].[dsp_selectclod_ReportSummary] @identityid ='" + strIdentityId + "',@clod_maxday='" + strStartDate + "',@hot_minday='" + strEndDate + "'";
                SqlConnection conn = new SqlConnection(conn_str);
                dt_clod = DBcon.DbHelperSQL.Query(conn, select_exec);

                for (int i = 0; i < dt_clod.Rows.Count; i++)
                {
                    dataTableHelper.ConvertDataRowToModel(dt_clod.Rows[i], resum_new);
                }
            }
            //起始時間查熱庫資料 且 結束時間要查熱庫
            else
            {
                string select_exec = "exec [dbo].[dsp_selecthot_ReportSummary] @identityid ='"+strIdentityId+"',@clod_maxday='"+strStartDate+"',@hot_minday='"+strEndDate+"'";
                SqlConnection conn = new SqlConnection(conn_str);
                dt_clod = DBcon.DbHelperSQL.Query(conn, select_exec);
                for (int i = 0; i < dt_clod.Rows.Count; i++)
                {
                    dataTableHelper.ConvertDataRowToModel(dt_clod.Rows[i], resum_new);
                }
            }








            string tempStartTime = DateTime.Now.Year + "-" + DateTime.Now.Month + "-01";
            string strLastMonth = DateTime.Parse(tempStartTime).AddMonths(-1).ToString("yyyy-MM");
            //本月起止日期
            string strMonth = DateTime.Parse(tempStartTime).ToString("yyyy-MM");


            // dt_clod = DBcon.DbHelperSQL.Query()
           
           


            //string strTotalDiscount = "0.00/0人";
            decimal strTotalDiscount = 0;
            if ((resum_new.Out_ActivityDiscountAccount!=0) && (resum_new.Out_OtherDiscountAccount!=0))
            {
                decimal fActivityDiscountAccountMoney = GetMoney(resum_new.Out_ActivityDiscountAccount);
                decimal fOtherDiscountAccountMoney = GetMoney(resum_new.Out_OtherDiscountAccount);
                decimal fTotalDiscountMoney = fActivityDiscountAccountMoney + fOtherDiscountAccountMoney;
                strTotalDiscount = fTotalDiscountMoney;//.ToString("0.00") + "/"+ dicTotalDiscount.Count + "人";
            }
            resum_new.TotalDiscount = strTotalDiscount;
            //代理总额＝代理工资 +代理分红                   13      14 
            //当前盈利=投注－中奖－返点－活动－其他优惠－代理分红－日工资+拒绝出款+行政提出
            decimal fOut_BettingAccount = GetMoney(resum_new.Out_BettingAccount);
            decimal fNowProfit = 0;
            if ((resum_new.NowProfit!=0))
            {
                fNowProfit = resum_new.NowProfit;
            }
            fNowProfit = fOut_BettingAccount;
            fNowProfit -= GetMoney(resum_new.Out_RebateAccount);
            fNowProfit -= GetMoney(resum_new.Out_WinningAccount);
            fNowProfit -= GetMoney(resum_new.Out_ActivityDiscountAccount);
            fNowProfit -= GetMoney(resum_new.Out_OtherDiscountAccount);
            fNowProfit -= GetMoney(resum_new.Bonus);
            fNowProfit -= GetMoney(resum_new.Wages);
            fNowProfit += GetMoney(resum_new.Out_RefuseAccount);
            fNowProfit += GetMoney(resum_new.Out_AdministrationAccount);
            resum_new.NowProfit = fNowProfit;
            //当前盈率＝当前盈利/投注*100、
            decimal fNowProfitRate = 0;
            if (fOut_BettingAccount != 0)
            {
                fNowProfitRate = fNowProfit / fOut_BettingAccount * 100;
            }
            resum_new.NowProfitRate = fNowProfitRate;
            ////会员余额
            decimal fTotlaBalance = 0;
            DataTable userCapital = DBcon.DbHelperSQL.Query("select sum(use_money) as use_money from dt_user_capital where identityid='" + strIdentityId + "' and flog=0").Tables[0];
            if (userCapital != null && userCapital.Rows.Count > 0)
            {
                fTotlaBalance = decimal.Parse(userCapital.Rows[0]["use_money"].ToString());
            }
            resum_new.TotlaBalance = fTotlaBalance;

            //新增会员
            DataTable registerDataTable = DBcon.DbHelperSQL.Query(@"select count(id) as num from dt_users_register  where identityid='" + strIdentityId +
                                                                    @"' and AddTime between '" + strStartDate + "' and '" + strEndDate + "' and Tester=0").Tables[0];
            int iNewMemberNum = 0;
            object bjNewMemberNum = registerDataTable.Rows[0]["num"];
            if (bjNewMemberNum != null)
            {
                iNewMemberNum = int.Parse(bjNewMemberNum.ToString());
            }
            resum_new.NewMemberNum = iNewMemberNum;


            listReportSummary_new.Add(resum_new);
            DataTable dataTable = dataTableHelper.ConvertListToDataTable(listReportSummary_new);
            dataSet.Tables.Add(dataTable);






            //var redisClient = GlobalVariable.redisClient;
            //var redisClientKey = GlobalVariable.redisClientKey;
            ////Expression<Func<dt_report_inoutaccount, bool>> expression = n => GetReportIndexCondition(n, strIdentityId, strStartDate, strEndDate);
            ////var inOutAccountList = redisClient.GetAll<dt_report_inoutaccount>().Where(expression.Compile()).ToList();
            //DateTime dtStartTime = DateTime.Parse(strStartDate);
            //DateTime dtEndTime = DateTime.Parse(strEndDate);
            //Log.Info("获取出入账开始时间：" + DateTime.Now.ToString());
            //List<dt_report_inoutaccount> inOutAccountList = redisHelper.Search<dt_report_inoutaccount>(redisClient, redisClientKey, dtStartTime, dtEndTime, strIdentityId).ToList();
            //Log.Info("获取出入账结束时间：" + DateTime.Now.ToString());
            ////多条件组合
            ////Expression<Func<dt_report_num, bool>> expressionReportNum = n => GetReportIndexReportNumCondition(n, strIdentityId, strStartDate, strEndDate);
            ////var reportNumList = redisClient.GetAll<dt_report_num>().Where(expressionReportNum.Compile()).ToList();
            //Log.Info("获取出次数统计表开始时间：" + DateTime.Now.ToString());
            //List<dt_report_num> reportNumList = redisHelper.Search<dt_report_num>(redisClient, redisClientKey, dtStartTime, dtEndTime,strIdentityId).ToList();
            //Log.Info("获取出次数统计表结束时间：" + DateTime.Now.ToString());
            //Dictionary<string, int> dicUsers = new Dictionary<string, int>();
            //Dictionary<string, int> dicNum = new Dictionary<string, int>();
            ////上月起止日期
            //string tempStartTime = DateTime.Now.Year + "-" + DateTime.Now.Month + "-01";
            //string strLastMonth = DateTime.Parse(tempStartTime).AddMonths(-1).ToString("yyyy-MM");
            ////本月起止日期
            //string strMonth = DateTime.Parse(tempStartTime).ToString("yyyy-MM");
            ////上月统计
            ////List<dt_user_month_summary> lastMonthSummaryList = redisClient.GetAll<dt_user_month_summary>().Where(m => m.YearMonth == strLastMonth && m.identityid == strIdentityId).ToList();
            //Log.Info("获取上月统计开始时间：" + DateTime.Now.ToString());
            //List<dt_user_month_summary> lastMonthSummaryList = redisHelper.SearchByYearMonth<dt_user_month_summary>(redisClient, redisClientKey,strIdentityId, strLastMonth).ToList();
            //Log.Info("获取上月统计结束时间：" + DateTime.Now.ToString());
            //if (lastMonthSummaryList != null)
            //{
            //    for (int i = 0; i < lastMonthSummaryList.Count; i++)
            //    {
            //        //会员Id
            //        string strUserId = lastMonthSummaryList[i].user_id.ToString();
            //        SetEntityValue(lastMonthSummaryList[i], reportSummary, dicUsers, dicNum, strUserId, "LastMonth");
            //        //上月盈利=投注－中奖－返点－活动－其他优惠－代理分红－日工资+拒绝出款+行政提出
            //        decimal fMonthProfitLossNum = GetNum(reportSummary.LastMonthProfitLoss);
            //        decimal fMonthProfitLossMoney = GetMoney(reportSummary.LastMonthOut_BettingAccount);
            //        fMonthProfitLossMoney -= GetMoney(reportSummary.LastMonthOut_WinningAccount);
            //        fMonthProfitLossMoney -= GetMoney(reportSummary.LastMonthOut_RebateAccount);
            //        fMonthProfitLossMoney -= GetMoney(reportSummary.LastMonthOut_ActivityDiscountAccount);
            //        fMonthProfitLossMoney -= GetMoney(reportSummary.LastMonthOut_OtherDiscountAccount);
            //        fMonthProfitLossMoney -= GetMoney(reportSummary.LastMonthBonus);
            //        fMonthProfitLossMoney -= GetMoney(reportSummary.LastMonthWages);
            //        fMonthProfitLossMoney += GetMoney(reportSummary.LastMonthOut_RefuseAccount);
            //        fMonthProfitLossMoney += GetMoney(reportSummary.LastMonthOut_AdministrationAccount);
            //        reportSummary.LastMonthProfitLoss = fMonthProfitLossMoney + "/" + fMonthProfitLossNum + "人";
            //    }
            //}
            ////本月统计
            ////List<dt_user_month_summary> monthSummaryList = redisClient.GetAll<dt_user_month_summary>().Where(m => m.YearMonth == strMonth && m.identityid == strIdentityId).ToList();
            //Log.Info("获取本月统计开始时间：" + DateTime.Now.ToString());
            //List<dt_user_month_summary> monthSummaryList = redisHelper.SearchByYearMonth<dt_user_month_summary>(redisClient, redisClientKey,strIdentityId, strMonth).ToList();
            //Log.Info("获取本月统计结束时间：" + DateTime.Now.ToString());
            //if (monthSummaryList != null)
            //{
            //    for (int i = 0; i < monthSummaryList.Count; i++)
            //    {
            //        //会员Id
            //        string strUserId = monthSummaryList[i].user_id.ToString();
            //        SetEntityValue(monthSummaryList[i], reportSummary, dicUsers, dicNum, strUserId, "Month");
            //        //本月盈利=投注－中奖－返点－活动－其他优惠－代理分红－日工资+拒绝出款+行政提出
            //        decimal fMonthProfitLossNum = GetNum(reportSummary.MonthProfitLoss);
            //        decimal fMonthProfitLossMoney = GetMoney(reportSummary.MonthOut_BettingAccount);
            //        fMonthProfitLossMoney -= GetMoney(reportSummary.MonthOut_WinningAccount);
            //        fMonthProfitLossMoney -= GetMoney(reportSummary.MonthOut_RebateAccount);
            //        fMonthProfitLossMoney -= GetMoney(reportSummary.MonthOut_ActivityDiscountAccount);
            //        fMonthProfitLossMoney -= GetMoney(reportSummary.MonthOut_OtherDiscountAccount);
            //        fMonthProfitLossMoney -= GetMoney(reportSummary.MonthBonus);
            //        fMonthProfitLossMoney -= GetMoney(reportSummary.MonthWages);
            //        fMonthProfitLossMoney += GetMoney(reportSummary.MonthOut_RefuseAccount);
            //        fMonthProfitLossMoney += GetMoney(reportSummary.MonthOut_AdministrationAccount);
            //        reportSummary.MonthProfitLoss = fMonthProfitLossMoney + "/" + fMonthProfitLossNum + "人";
            //        //本月盈率＝本月盈利/投注*100
            //        decimal fMonthProfitRate = 0;
            //        decimal fMonthProfitLoss = GetMoney(reportSummary.MonthProfitLoss);
            //        decimal fMonthBetMoney = GetMoney(reportSummary.MonthOut_BettingAccount);
            //        if (fMonthBetMoney != 0)
            //        {
            //            fMonthProfitRate = fMonthProfitLoss / fMonthBetMoney * 100;
            //        }
            //        reportSummary.MonthProfitRate = fMonthProfitRate + "%";
            //    }
            //}
            //int inOutAccountListCount = inOutAccountList.Count;
            ////用来保存验证人数
            //string strUserIdWhereSql = string.Empty;
            //for (var i = 0; i < inOutAccountListCount; i++)
            //{
            //    //会员Id
            //    string strUserId = inOutAccountList[i].user_id.ToString();
            //    strUserIdWhereSql += strUserId + ",";
            //    //实体类赋值
            //    SetEntityValue(inOutAccountList[i], reportSummary, dicUsers, dicNum, strUserId);
            //}
            ////存款总额＝快捷充值+网银充值+支付宝充值+微信充值+人工存款
            //decimal fTotalInMoney = GetMoney(reportSummary.In_AlipayPrepaid);
            //fTotalInMoney += GetMoney(reportSummary.In_ArtificialDeposit);
            //fTotalInMoney += GetMoney(reportSummary.In_BankPrepaid);
            //fTotalInMoney += GetMoney(reportSummary.In_FastPrepaid);
            //fTotalInMoney += GetMoney(reportSummary.In_WeChatPrepaid);
            ////存款总额的人数
            //Dictionary<string, string> dicTotalInNum = new Dictionary<string, string>();
            ////人工提出的人数
            //Dictionary<string, string> dicTotalArtificialOut = new Dictionary<string, string>();
            ////优惠总额的人数
            //Dictionary<string, string> dicTotalDiscount = new Dictionary<string, string>();
            ////代理总额的人数
            //Dictionary<string, string> dicTotalAgent = new Dictionary<string, string>();
            //foreach (KeyValuePair<string, int> keyUsers in dicUsers)
            //{
            //    int index = keyUsers.Key.IndexOf("_");
            //    if (index == -1)
            //        continue;

            //    string strUserId = keyUsers.Key.Substring(0, index);
            //    string strPropertyName = keyUsers.Key.Substring(index + 1);
            //    //统计各种充值方式的人数，去除重复人数（一个人有可能使用不同的充值方式充值）
            //    if (strPropertyName == "In_AlipayPrepaid" || strPropertyName == "In_ArtificialDeposit" ||
            //        strPropertyName == "In_BankPrepaid" || strPropertyName == "In_FastPrepaid" || strPropertyName == "In_WeChatPrepaid")
            //    {
            //        if (!dicTotalInNum.ContainsKey(strUserId))
            //        {
            //            dicTotalInNum.Add(strUserId, strUserId);
            //        }
            //        continue;
            //    }
            //    //统计人工提出的人数，去除重复人数
            //    if (strPropertyName == "Out_MistakeAccount" || strPropertyName == "Out_AdministrationAccount")
            //    {
            //        if (!dicTotalArtificialOut.ContainsKey(strUserId))
            //        {
            //            dicTotalArtificialOut.Add(strUserId, strUserId);
            //        }
            //        continue;
            //    }
            //    //统计优惠总额的人数，去除重复人数
            //    if (strPropertyName == "Out_ActivityDiscountAccount" || strPropertyName == "Out_OtherDiscountAccount")
            //    {
            //        if (!dicTotalDiscount.ContainsKey(strUserId))
            //        {
            //            dicTotalDiscount.Add(strUserId, strUserId);
            //        }
            //        continue;
            //    }
            //    //统计代理总额的人数，去除重复人数
            //    if (strPropertyName == "Wages" || strPropertyName == "Bonus")
            //    {
            //        if (!dicTotalAgent.ContainsKey(strUserId))
            //        {
            //            dicTotalAgent.Add(strUserId, strUserId);
            //        }
            //        continue;
            //    }
            //}
            //reportSummary.TotalInMoney = fTotalInMoney.ToString("0.00") + "/" + dicTotalInNum.Count + "人";
            //for (var i = 0; i < reportNumList.Count; i++)
            //{
            //    //会员Id
            //    string strUserId = reportNumList[i].user_id.ToString();
            //    SetEntityValue(reportNumList[i], reportSummary, dicUsers, dicNum, strUserId, "", true);
            //}
            ////新增会员
            //DataTable registerDataTable = DBcon.DbHelperSQL.Query(@"select count(id) as num from dt_users_register  where identityid='" + strIdentityId +
            //                                                        @"' and AddTime between '" + strStartDate + "' and '" + strEndDate + "' and Tester=0").Tables[0];
            //int iNewMemberNum = 0;
            //object bjNewMemberNum = registerDataTable.Rows[0]["num"];
            //if (bjNewMemberNum != null)
            //{
            //    iNewMemberNum = int.Parse(bjNewMemberNum.ToString());
            //}
            //reportSummary.NewMemberNum = iNewMemberNum.ToString();
            ////首充人员
            //Expression<Func<dt_report_newprepaid, bool>> expressionReportNewPrepaid = n => GetReportIndexReportNewPrepaidCondition(n, strIdentityId, strStartDate, strEndDate);
            ////var reportNewPreapdiList = redisClient.GetAll<dt_report_newprepaid>().Where(expressionReportNewPrepaid.Compile()).ToList();
            //Log.Info("获取首充人员开始时间：" + DateTime.Now.ToString());
            //List<dt_report_newprepaid> reportNewPreapdiList = redisHelper.SearchByUserId<dt_report_newprepaid>(redisClient, redisClientKey, "").Where(expressionReportNewPrepaid.Compile()).ToList();
            //Log.Info("获取首充人员结束时间：" + DateTime.Now.ToString());
            //reportSummary.NewPrepaidNum = reportNewPreapdiList.Count.ToString();
            ////在线人员
            //var redisOnlineClientConfig = GlobalVariable.onlineRedisClient;
            //Expression<Func<EasyFrame.NewCommon.Model.dt_sys_online, bool>> expressionOnLine = n => GetReportIndexOnLineCondition(n, strIdentityId);
            //var onlineList = redisOnlineClientConfig.GetAll<EasyFrame.NewCommon.Model.dt_sys_online>().Where(expressionOnLine.Compile()).ToList();
            //redisOnlineClientConfig.Quit();
            //redisOnlineClientConfig.Dispose();
            //redisClient.Quit();
            //redisClient.Dispose();
            //redisClientKey.Quit();
            //redisClientKey.Dispose();
            //reportSummary.OnLinessNum = onlineList.Count.ToString();
            //listReportSummary.Add(reportSummary);
            ////入款次数=快捷支付次数+支付宝付款次数+银行转账次数+微信支付次数+人工存款次数
            //decimal fInMoneyNumMoney = GetMoney(reportSummary.In_AlipayPrepaid_Num) + GetMoney(reportSummary.In_ArtificialDeposit_Num) +
            //                              GetMoney(reportSummary.In_BankPrepaid_Num) + GetMoney(reportSummary.In_FastPrepaid_Num) + GetMoney(reportSummary.In_WeChatPrepaid_Num);

            //reportSummary.InMoneyNum = fInMoneyNumMoney.ToString("0") + "/" + dicTotalInNum.Count + "人";
            ////人工提出＝误存提出+行政提出
            //string strTotalArtificialOut = "0.00/0人";
            //decimal fTotalArtificialOutMoney = GetMoney(reportSummary.Out_MistakeAccount) + GetMoney(reportSummary.Out_AdministrationAccount);
            ////int iTotalArtificialOutNum = GetNum(reportSummary.Out_MistakeAccount) + GetNum(reportSummary.Out_AdministrationAccount);
            //strTotalArtificialOut = fTotalArtificialOutMoney.ToString("0.00") + "/" + dicTotalArtificialOut.Count + "人";
            //reportSummary.TotalArtificialOut = strTotalArtificialOut;
            ////会员余额
            //decimal fTotlaBalance = 0;
            //DataTable userCapital = DBcon.DbHelperSQL.Query("select sum(use_money) as use_money from dt_user_capital where identityid='" + strIdentityId + "' and flog=0").Tables[0];
            //if (userCapital != null && userCapital.Rows.Count > 0)
            //{
            //    fTotlaBalance = decimal.Parse(userCapital.Rows[0]["use_money"].ToString());
            //}
            //reportSummary.TotlaBalance = fTotlaBalance.ToString("0.00");
            ////入款时间
            //FormatHelper formHelper = new FormatHelper();
            //double fInMoneyTime = 0;
            //if (!string.IsNullOrWhiteSpace(reportSummary.InMoneyTime))
            //{
            //    decimal fTime = GetMoney(reportSummary.InMoneyTime);
            //    double fNum = inOutAccountList.Where(m => m.AddTime >= DateTime.Parse(strStartDate) && m.AddTime <= DateTime.Parse(strEndDate)).Sum(m => m.InMoneyNum);
            //    if (fNum != 0)
            //    {
            //        fInMoneyTime = (double)fTime / fNum;
            //    }
            //}
            //reportSummary.InMoneyTime = formHelper.FormatSecond(fInMoneyTime);
            ////出款时间
            //double fOutMoneyTime = 0;
            //if (!string.IsNullOrWhiteSpace(reportSummary.InMoneyTime))
            //{
            //    decimal fTime = GetMoney(reportSummary.OutMoneyTime);
            //    double fNum = inOutAccountList.Where(m => m.AddTime >= DateTime.Parse(strStartDate) && m.AddTime <= DateTime.Parse(strEndDate)).Sum(m => m.OutMoneyNum);
            //    if (fNum != 0)
            //    {
            //        fOutMoneyTime = (double)fTime / fNum;
            //    }
            //}
            //reportSummary.OutMoneyTime = formHelper.FormatSecond(fOutMoneyTime);
            ////优惠总额＝活动优惠+其他优惠
            //string strTotalDiscount = "0.00/0人";
            //if (!string.IsNullOrWhiteSpace(reportSummary.Out_ActivityDiscountAccount) && !string.IsNullOrWhiteSpace(reportSummary.Out_OtherDiscountAccount))
            //{
            //    decimal fActivityDiscountAccountMoney = GetMoney(reportSummary.Out_ActivityDiscountAccount);
            //    decimal fOtherDiscountAccountMoney = GetMoney(reportSummary.Out_OtherDiscountAccount);
            //    decimal fTotalDiscountMoney = fActivityDiscountAccountMoney + fOtherDiscountAccountMoney;
            //    strTotalDiscount = fTotalDiscountMoney.ToString("0.00") + "/" + dicTotalDiscount.Count + "人";
            //}
            //reportSummary.TotalDiscount = strTotalDiscount;
            ////代理总额＝代理工资 +代理分红                   13      14 
            ////当前盈利=投注－中奖－返点－活动－其他优惠－代理分红－日工资+拒绝出款+行政提出
            //decimal fOut_BettingAccount = GetMoney(reportSummary.Out_BettingAccount);
            //decimal fNowProfit = 0;
            //if (!string.IsNullOrWhiteSpace(reportSummary.NowProfit))
            //{
            //    fNowProfit = decimal.Parse(reportSummary.NowProfit);
            //}
            //fNowProfit = fOut_BettingAccount;
            //fNowProfit -= GetMoney(reportSummary.Out_RebateAccount);
            //fNowProfit -= GetMoney(reportSummary.Out_WinningAccount);
            //fNowProfit -= GetMoney(reportSummary.Out_ActivityDiscountAccount);
            //fNowProfit -= GetMoney(reportSummary.Out_OtherDiscountAccount);
            //fNowProfit -= GetMoney(reportSummary.Bonus);
            //fNowProfit -= GetMoney(reportSummary.Wages);
            //fNowProfit += GetMoney(reportSummary.Out_RefuseAccount);
            //fNowProfit += GetMoney(reportSummary.Out_AdministrationAccount);
            //reportSummary.NowProfit = fNowProfit.ToString("0.00");
            ////当前盈率＝当前盈利/投注*100、
            //decimal fNowProfitRate = 0;
            //if (fOut_BettingAccount != 0)
            //{
            //    fNowProfitRate = fNowProfit / fOut_BettingAccount * 100;
            //}
            //reportSummary.NowProfitRate = fNowProfitRate.ToString("0.00");
            ////本月盈利
            //reportSummary.MonthProfitLoss = GetMoney(reportSummary.MonthProfitLoss).ToString("0.00");
            ////上月盈利
            //reportSummary.LastMonthProfitLoss = GetMoney(reportSummary.LastMonthProfitLoss).ToString("0.00");
            ////当日损益
            //decimal fCurrentProfitAndLoss = GetMoney(reportSummary.Out_BettingAccount) - GetMoney(reportSummary.Out_WinningAccount);
            //reportSummary.CurrentProfitAndLoss = fCurrentProfitAndLoss.ToString("0.00");
            ////本月损益
            //decimal fMonthProfitAndLoss = GetMoney(reportSummary.MonthOut_BettingAccount) - GetMoney(reportSummary.MonthOut_WinningAccount);
            //reportSummary.MonthProfitAndLoss = fMonthProfitAndLoss.ToString("0.00");
            ////上月损益
            //decimal fLastMonthProfitAndLoss = GetMoney(reportSummary.LastMonthOut_BettingAccount) - GetMoney(reportSummary.LastMonthOut_WinningAccount);
            //reportSummary.LastMonthProfitAndLoss = fLastMonthProfitAndLoss.ToString("0.00");
            ////平台抽成
            //decimal fTakePercentage = ComputeTakePercentage(fLastMonthProfitAndLoss);
            //reportSummary.TakePercentage = fTakePercentage.ToString("0.00");
            //DataTableHelper dataTableHelper = new DataTableHelper();
            //DataTable dataTable = dataTableHelper.ConvertListToDataTable(listReportSummary);
            //dataSet.Tables.Add(dataTable);
            //Log.Info("查询综合报表结束时间：" + DateTime.Now.ToString());
            return dataSet;
        }
        catch (Exception ex)
        {
            Log.Error(ex);
            return null;
        }
    }
    //[WebMethod]
    //[SoapHeader("encryptionSoapHeader")]
    //public DataSet GetReportIndexData(string strIdentityId, string strStartDate, string strEndDate)
    //{
    //    DataSet dataSet = new DataSet();
    //    List<ReportSummary> listReportSummary = new List<ReportSummary>();
    //    ReportSummary reportSummary = new ReportSummary();
    //    try
    //    {
    //        //验证是否有权访问  
    //        //string strMsg = ValidateAuthority();
    //        //if (!string.IsNullOrWhiteSpace(strMsg))
    //        //{
    //        //    return dataSet;
    //        //}
    //        var redisClient = GlobalVariable.redisClient;
    //        var redisClientKey = GlobalVariable.redisClientKey;
    //        //Expression<Func<dt_report_inoutaccount, bool>> expression = n => GetReportIndexCondition(n, strIdentityId, strStartDate, strEndDate);
    //        //var inOutAccountList = redisClient.GetAll<dt_report_inoutaccount>().Where(expression.Compile()).ToList();
    //        DateTime dtStartTime = DateTime.Parse(strStartDate);
    //        DateTime dtEndTime = DateTime.Parse(strEndDate);
    //        Log.Info("获取出入账开始时间：" + DateTime.Now.ToString());
    //        List<dt_report_inoutaccount> inOutAccountList = redisHelper.Search<dt_report_inoutaccount>(redisClient, redisClientKey, dtStartTime, dtEndTime, strIdentityId).ToList();
    //        Log.Info("获取出入账结束时间：" + DateTime.Now.ToString());
    //        //多条件组合
    //        //Expression<Func<dt_report_num, bool>> expressionReportNum = n => GetReportIndexReportNumCondition(n, strIdentityId, strStartDate, strEndDate);
    //        //var reportNumList = redisClient.GetAll<dt_report_num>().Where(expressionReportNum.Compile()).ToList();
    //        Log.Info("获取出次数统计表开始时间：" + DateTime.Now.ToString());
    //        List<dt_report_num> reportNumList = redisHelper.Search<dt_report_num>(redisClient, redisClientKey, dtStartTime, dtEndTime, strIdentityId).ToList();
    //        Log.Info("获取出次数统计表结束时间：" + DateTime.Now.ToString());
    //        Dictionary<string, int> dicUsers = new Dictionary<string, int>();
    //        Dictionary<string, int> dicNum = new Dictionary<string, int>();
    //        //上月起止日期
    //        string tempStartTime = DateTime.Now.Year + "-" + DateTime.Now.Month + "-01";
    //        string strLastMonth = DateTime.Parse(tempStartTime).AddMonths(-1).ToString("yyyy-MM");
    //        //本月起止日期
    //        string strMonth = DateTime.Parse(tempStartTime).ToString("yyyy-MM");
    //        //上月统计
    //        //List<dt_user_month_summary> lastMonthSummaryList = redisClient.GetAll<dt_user_month_summary>().Where(m => m.YearMonth == strLastMonth && m.identityid == strIdentityId).ToList();
    //        Log.Info("获取上月统计开始时间：" + DateTime.Now.ToString());
    //        List<dt_user_month_summary> lastMonthSummaryList = redisHelper.SearchByYearMonth<dt_user_month_summary>(redisClient, redisClientKey, strIdentityId, strLastMonth).ToList();
    //        Log.Info("获取上月统计结束时间：" + DateTime.Now.ToString());
    //        if (lastMonthSummaryList != null)
    //        {
    //            for (int i = 0; i < lastMonthSummaryList.Count; i++)
    //            {
    //                //会员Id
    //                string strUserId = lastMonthSummaryList[i].user_id.ToString();
    //                SetEntityValue(lastMonthSummaryList[i], reportSummary, dicUsers, dicNum, strUserId, "LastMonth");
    //                //上月盈利=投注－中奖－返点－活动－其他优惠－代理分红－日工资+拒绝出款+行政提出
    //                decimal fMonthProfitLossNum = GetNum(reportSummary.LastMonthProfitLoss);
    //                decimal fMonthProfitLossMoney = GetMoney(reportSummary.LastMonthOut_BettingAccount);
    //                fMonthProfitLossMoney -= GetMoney(reportSummary.LastMonthOut_WinningAccount);
    //                fMonthProfitLossMoney -= GetMoney(reportSummary.LastMonthOut_RebateAccount);
    //                fMonthProfitLossMoney -= GetMoney(reportSummary.LastMonthOut_ActivityDiscountAccount);
    //                fMonthProfitLossMoney -= GetMoney(reportSummary.LastMonthOut_OtherDiscountAccount);
    //                fMonthProfitLossMoney -= GetMoney(reportSummary.LastMonthBonus);
    //                fMonthProfitLossMoney -= GetMoney(reportSummary.LastMonthWages);
    //                fMonthProfitLossMoney += GetMoney(reportSummary.LastMonthOut_RefuseAccount);
    //                fMonthProfitLossMoney += GetMoney(reportSummary.LastMonthOut_AdministrationAccount);
    //                reportSummary.LastMonthProfitLoss = fMonthProfitLossMoney + "/" + fMonthProfitLossNum + "人";
    //            }
    //        }
    //        //本月统计
    //        //List<dt_user_month_summary> monthSummaryList = redisClient.GetAll<dt_user_month_summary>().Where(m => m.YearMonth == strMonth && m.identityid == strIdentityId).ToList();
    //        Log.Info("获取本月统计开始时间：" + DateTime.Now.ToString());
    //        List<dt_user_month_summary> monthSummaryList = redisHelper.SearchByYearMonth<dt_user_month_summary>(redisClient, redisClientKey, strIdentityId, strMonth).ToList();
    //        Log.Info("获取本月统计结束时间：" + DateTime.Now.ToString());
    //        if (monthSummaryList != null)
    //        {
    //            for (int i = 0; i < monthSummaryList.Count; i++)
    //            {
    //                //会员Id
    //                string strUserId = monthSummaryList[i].user_id.ToString();
    //                SetEntityValue(monthSummaryList[i], reportSummary, dicUsers, dicNum, strUserId, "Month");
    //                //本月盈利=投注－中奖－返点－活动－其他优惠－代理分红－日工资+拒绝出款+行政提出
    //                decimal fMonthProfitLossNum = GetNum(reportSummary.MonthProfitLoss);
    //                decimal fMonthProfitLossMoney = GetMoney(reportSummary.MonthOut_BettingAccount);
    //                fMonthProfitLossMoney -= GetMoney(reportSummary.MonthOut_WinningAccount);
    //                fMonthProfitLossMoney -= GetMoney(reportSummary.MonthOut_RebateAccount);
    //                fMonthProfitLossMoney -= GetMoney(reportSummary.MonthOut_ActivityDiscountAccount);
    //                fMonthProfitLossMoney -= GetMoney(reportSummary.MonthOut_OtherDiscountAccount);
    //                fMonthProfitLossMoney -= GetMoney(reportSummary.MonthBonus);
    //                fMonthProfitLossMoney -= GetMoney(reportSummary.MonthWages);
    //                fMonthProfitLossMoney += GetMoney(reportSummary.MonthOut_RefuseAccount);
    //                fMonthProfitLossMoney += GetMoney(reportSummary.MonthOut_AdministrationAccount);
    //                reportSummary.MonthProfitLoss = fMonthProfitLossMoney + "/" + fMonthProfitLossNum + "人";
    //                //本月盈率＝本月盈利/投注*100
    //                decimal fMonthProfitRate = 0;
    //                decimal fMonthProfitLoss = GetMoney(reportSummary.MonthProfitLoss);
    //                decimal fMonthBetMoney = GetMoney(reportSummary.MonthOut_BettingAccount);
    //                if (fMonthBetMoney != 0)
    //                {
    //                    fMonthProfitRate = fMonthProfitLoss / fMonthBetMoney * 100;
    //                }
    //                reportSummary.MonthProfitRate = fMonthProfitRate + "%";
    //            }
    //        }
    //        int inOutAccountListCount = inOutAccountList.Count;
    //        //用来保存验证人数
    //        string strUserIdWhereSql = string.Empty;
    //        for (var i = 0; i < inOutAccountListCount; i++)
    //        {
    //            //会员Id
    //            string strUserId = inOutAccountList[i].user_id.ToString();
    //            strUserIdWhereSql += strUserId + ",";
    //            //实体类赋值
    //            SetEntityValue(inOutAccountList[i], reportSummary, dicUsers, dicNum, strUserId);
    //        }
    //        //存款总额＝快捷充值+网银充值+支付宝充值+微信充值+人工存款
    //        decimal fTotalInMoney = GetMoney(reportSummary.In_AlipayPrepaid);
    //        fTotalInMoney += GetMoney(reportSummary.In_ArtificialDeposit);
    //        fTotalInMoney += GetMoney(reportSummary.In_BankPrepaid);
    //        fTotalInMoney += GetMoney(reportSummary.In_FastPrepaid);
    //        fTotalInMoney += GetMoney(reportSummary.In_WeChatPrepaid);
    //        //存款总额的人数
    //        Dictionary<string, string> dicTotalInNum = new Dictionary<string, string>();
    //        //人工提出的人数
    //        Dictionary<string, string> dicTotalArtificialOut = new Dictionary<string, string>();
    //        //优惠总额的人数
    //        Dictionary<string, string> dicTotalDiscount = new Dictionary<string, string>();
    //        //代理总额的人数
    //        Dictionary<string, string> dicTotalAgent = new Dictionary<string, string>();
    //        foreach (KeyValuePair<string, int> keyUsers in dicUsers)
    //        {
    //            int index = keyUsers.Key.IndexOf("_");
    //            if (index == -1)
    //                continue;

    //            string strUserId = keyUsers.Key.Substring(0, index);
    //            string strPropertyName = keyUsers.Key.Substring(index + 1);
    //            //统计各种充值方式的人数，去除重复人数（一个人有可能使用不同的充值方式充值）
    //            if (strPropertyName == "In_AlipayPrepaid" || strPropertyName == "In_ArtificialDeposit" ||
    //                strPropertyName == "In_BankPrepaid" || strPropertyName == "In_FastPrepaid" || strPropertyName == "In_WeChatPrepaid")
    //            {
    //                if (!dicTotalInNum.ContainsKey(strUserId))
    //                {
    //                    dicTotalInNum.Add(strUserId, strUserId);
    //                }
    //                continue;
    //            }
    //            //统计人工提出的人数，去除重复人数
    //            if (strPropertyName == "Out_MistakeAccount" || strPropertyName == "Out_AdministrationAccount")
    //            {
    //                if (!dicTotalArtificialOut.ContainsKey(strUserId))
    //                {
    //                    dicTotalArtificialOut.Add(strUserId, strUserId);
    //                }
    //                continue;
    //            }
    //            //统计优惠总额的人数，去除重复人数
    //            if (strPropertyName == "Out_ActivityDiscountAccount" || strPropertyName == "Out_OtherDiscountAccount")
    //            {
    //                if (!dicTotalDiscount.ContainsKey(strUserId))
    //                {
    //                    dicTotalDiscount.Add(strUserId, strUserId);
    //                }
    //                continue;
    //            }
    //            //统计代理总额的人数，去除重复人数
    //            if (strPropertyName == "Wages" || strPropertyName == "Bonus")
    //            {
    //                if (!dicTotalAgent.ContainsKey(strUserId))
    //                {
    //                    dicTotalAgent.Add(strUserId, strUserId);
    //                }
    //                continue;
    //            }
    //        }
    //        reportSummary.TotalInMoney = fTotalInMoney.ToString("0.00") + "/" + dicTotalInNum.Count + "人";
    //        for (var i = 0; i < reportNumList.Count; i++)
    //        {
    //            //会员Id
    //            string strUserId = reportNumList[i].user_id.ToString();
    //            SetEntityValue(reportNumList[i], reportSummary, dicUsers, dicNum, strUserId, "", true);
    //        }
    //        //新增会员
    //        DataTable registerDataTable = DBcon.DbHelperSQL.Query(@"select count(id) as num from dt_users_register  where identityid='" + strIdentityId +
    //                                                                @"' and AddTime between '" + strStartDate + "' and '" + strEndDate + "' and Tester=0").Tables[0];
    //        int iNewMemberNum = 0;
    //        object bjNewMemberNum = registerDataTable.Rows[0]["num"];
    //        if (bjNewMemberNum != null)
    //        {
    //            iNewMemberNum = int.Parse(bjNewMemberNum.ToString());
    //        }
    //        reportSummary.NewMemberNum = iNewMemberNum.ToString();
    //        //首充人员
    //        Expression<Func<dt_report_newprepaid, bool>> expressionReportNewPrepaid = n => GetReportIndexReportNewPrepaidCondition(n, strIdentityId, strStartDate, strEndDate);
    //        //var reportNewPreapdiList = redisClient.GetAll<dt_report_newprepaid>().Where(expressionReportNewPrepaid.Compile()).ToList();
    //        Log.Info("获取首充人员开始时间：" + DateTime.Now.ToString());
    //        List<dt_report_newprepaid> reportNewPreapdiList = redisHelper.SearchByUserId<dt_report_newprepaid>(redisClient, redisClientKey, "").Where(expressionReportNewPrepaid.Compile()).ToList();
    //        Log.Info("获取首充人员结束时间：" + DateTime.Now.ToString());
    //        reportSummary.NewPrepaidNum = reportNewPreapdiList.Count.ToString();
    //        //在线人员
    //        var redisOnlineClientConfig = GlobalVariable.onlineRedisClient;
    //        Expression<Func<EasyFrame.NewCommon.Model.dt_sys_online, bool>> expressionOnLine = n => GetReportIndexOnLineCondition(n, strIdentityId);
    //        var onlineList = redisOnlineClientConfig.GetAll<EasyFrame.NewCommon.Model.dt_sys_online>().Where(expressionOnLine.Compile()).ToList();
    //        redisOnlineClientConfig.Quit();
    //        redisOnlineClientConfig.Dispose();
    //        redisClient.Quit();
    //        redisClient.Dispose();
    //        redisClientKey.Quit();
    //        redisClientKey.Dispose();
    //        reportSummary.OnLinessNum = onlineList.Count.ToString();
    //        listReportSummary.Add(reportSummary);
    //        //入款次数=快捷支付次数+支付宝付款次数+银行转账次数+微信支付次数+人工存款次数
    //        decimal fInMoneyNumMoney = GetMoney(reportSummary.In_AlipayPrepaid_Num) + GetMoney(reportSummary.In_ArtificialDeposit_Num) +
    //                                      GetMoney(reportSummary.In_BankPrepaid_Num) + GetMoney(reportSummary.In_FastPrepaid_Num) + GetMoney(reportSummary.In_WeChatPrepaid_Num);

    //        reportSummary.InMoneyNum = fInMoneyNumMoney.ToString("0") + "/" + dicTotalInNum.Count + "人";
    //        //人工提出＝误存提出+行政提出
    //        string strTotalArtificialOut = "0.00/0人";
    //        decimal fTotalArtificialOutMoney = GetMoney(reportSummary.Out_MistakeAccount) + GetMoney(reportSummary.Out_AdministrationAccount);
    //        //int iTotalArtificialOutNum = GetNum(reportSummary.Out_MistakeAccount) + GetNum(reportSummary.Out_AdministrationAccount);
    //        strTotalArtificialOut = fTotalArtificialOutMoney.ToString("0.00") + "/" + dicTotalArtificialOut.Count + "人";
    //        reportSummary.TotalArtificialOut = strTotalArtificialOut;
    //        //会员余额
    //        decimal fTotlaBalance = 0;
    //        DataTable userCapital = DBcon.DbHelperSQL.Query("select sum(use_money) as use_money from dt_user_capital where identityid='" + strIdentityId + "' and flog=0").Tables[0];
    //        if (userCapital != null && userCapital.Rows.Count > 0)
    //        {
    //            fTotlaBalance = decimal.Parse(userCapital.Rows[0]["use_money"].ToString());
    //        }
    //        reportSummary.TotlaBalance = fTotlaBalance.ToString("0.00");
    //        //入款时间
    //        FormatHelper formHelper = new FormatHelper();
    //        double fInMoneyTime = 0;
    //        if (!string.IsNullOrWhiteSpace(reportSummary.InMoneyTime))
    //        {
    //            decimal fTime = GetMoney(reportSummary.InMoneyTime);
    //            double fNum = inOutAccountList.Where(m => m.AddTime >= DateTime.Parse(strStartDate) && m.AddTime <= DateTime.Parse(strEndDate)).Sum(m => m.InMoneyNum);
    //            if (fNum != 0)
    //            {
    //                fInMoneyTime = (double)fTime / fNum;
    //            }
    //        }
    //        reportSummary.InMoneyTime = formHelper.FormatSecond(fInMoneyTime);
    //        //出款时间
    //        double fOutMoneyTime = 0;
    //        if (!string.IsNullOrWhiteSpace(reportSummary.InMoneyTime))
    //        {
    //            decimal fTime = GetMoney(reportSummary.OutMoneyTime);
    //            double fNum = inOutAccountList.Where(m => m.AddTime >= DateTime.Parse(strStartDate) && m.AddTime <= DateTime.Parse(strEndDate)).Sum(m => m.OutMoneyNum);
    //            if (fNum != 0)
    //            {
    //                fOutMoneyTime = (double)fTime / fNum;
    //            }
    //        }
    //        reportSummary.OutMoneyTime = formHelper.FormatSecond(fOutMoneyTime);
    //        //优惠总额＝活动优惠+其他优惠
    //        string strTotalDiscount = "0.00/0人";
    //        if (!string.IsNullOrWhiteSpace(reportSummary.Out_ActivityDiscountAccount) && !string.IsNullOrWhiteSpace(reportSummary.Out_OtherDiscountAccount))
    //        {
    //            decimal fActivityDiscountAccountMoney = GetMoney(reportSummary.Out_ActivityDiscountAccount);
    //            decimal fOtherDiscountAccountMoney = GetMoney(reportSummary.Out_OtherDiscountAccount);
    //            decimal fTotalDiscountMoney = fActivityDiscountAccountMoney + fOtherDiscountAccountMoney;
    //            strTotalDiscount = fTotalDiscountMoney.ToString("0.00") + "/" + dicTotalDiscount.Count + "人";
    //        }
    //        reportSummary.TotalDiscount = strTotalDiscount;
    //        //代理总额＝代理工资 +代理分红
    //        //当前盈利=投注－中奖－返点－活动－其他优惠－代理分红－日工资+拒绝出款+行政提出
    //        decimal fOut_BettingAccount = GetMoney(reportSummary.Out_BettingAccount);
    //        decimal fNowProfit = 0;
    //        if (!string.IsNullOrWhiteSpace(reportSummary.NowProfit))
    //        {
    //            fNowProfit = decimal.Parse(reportSummary.NowProfit);
    //        }
    //        fNowProfit = fOut_BettingAccount;
    //        fNowProfit -= GetMoney(reportSummary.Out_RebateAccount);
    //        fNowProfit -= GetMoney(reportSummary.Out_WinningAccount);
    //        fNowProfit -= GetMoney(reportSummary.Out_ActivityDiscountAccount);
    //        fNowProfit -= GetMoney(reportSummary.Out_OtherDiscountAccount);
    //        fNowProfit -= GetMoney(reportSummary.Bonus);
    //        fNowProfit -= GetMoney(reportSummary.Wages);
    //        fNowProfit += GetMoney(reportSummary.Out_RefuseAccount);
    //        fNowProfit += GetMoney(reportSummary.Out_AdministrationAccount);
    //        reportSummary.NowProfit = fNowProfit.ToString("0.00");
    //        //当前盈率＝当前盈利/投注*100、
    //        decimal fNowProfitRate = 0;
    //        if (fOut_BettingAccount != 0)
    //        {
    //            fNowProfitRate = fNowProfit / fOut_BettingAccount * 100;
    //        }
    //        reportSummary.NowProfitRate = fNowProfitRate.ToString("0.00");
    //        //本月盈利
    //        reportSummary.MonthProfitLoss = GetMoney(reportSummary.MonthProfitLoss).ToString("0.00");
    //        //上月盈利
    //        reportSummary.LastMonthProfitLoss = GetMoney(reportSummary.LastMonthProfitLoss).ToString("0.00");
    //        //当日损益
    //        decimal fCurrentProfitAndLoss = GetMoney(reportSummary.Out_BettingAccount) - GetMoney(reportSummary.Out_WinningAccount);
    //        reportSummary.CurrentProfitAndLoss = fCurrentProfitAndLoss.ToString("0.00");
    //        //本月损益
    //        decimal fMonthProfitAndLoss = GetMoney(reportSummary.MonthOut_BettingAccount) - GetMoney(reportSummary.MonthOut_WinningAccount);
    //        reportSummary.MonthProfitAndLoss = fMonthProfitAndLoss.ToString("0.00");
    //        //上月损益
    //        decimal fLastMonthProfitAndLoss = GetMoney(reportSummary.LastMonthOut_BettingAccount) - GetMoney(reportSummary.LastMonthOut_WinningAccount);
    //        reportSummary.LastMonthProfitAndLoss = fLastMonthProfitAndLoss.ToString("0.00");
    //        //平台抽成
    //        decimal fTakePercentage = ComputeTakePercentage(fLastMonthProfitAndLoss);
    //        reportSummary.TakePercentage = fTakePercentage.ToString("0.00");
    //        DataTableHelper dataTableHelper = new DataTableHelper();
    //        DataTable dataTable = dataTableHelper.ConvertListToDataTable(listReportSummary);
    //        dataSet.Tables.Add(dataTable);
    //        Log.Info("查询综合报表结束时间：" + DateTime.Now.ToString());
    //        return dataSet;
    //    }
    //    catch (Exception ex)
    //    {
    //        Log.Error(ex);
    //        return null;
    //    }
    //}
    /// <summary>
    /// 获取代理报表查询条件语句
    /// </summary>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strUserName">代理名称</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">结束日期</param>
    /// <returns></returns>
    private string GetReportAgentWhereSql(string strIdentityId, string strUserName, string strStartDate, string strEndDate)
    {
        StringBuilder strTemp = new StringBuilder();
        strUserName = strUserName.Replace("'", "");
        strStartDate = strStartDate.Replace("'", "");
        strEndDate = strEndDate.Replace("'", "");
        strTemp.Append(" identityid='" + strIdentityId + "' and is_agent=1 and flog=0");

        if (!string.IsNullOrEmpty(strUserName))
        {
            strTemp.Append(" and id in (select id from dt_users  where user_name='" + strUserName + "') and flog=0");
        }
        else
        {
            strTemp.Append(" and agent_id=-1 and flog=0");
        }
        return strTemp.ToString();
    }
    /// <summary>
    /// 获取条件
    /// </summary>
    /// <param name="reportInOutAccount"></param>
    /// <param name="listUserId">会员Id的条件集合</param>
    /// <returns></returns>
    private bool GetReportAgentCondition(dt_report_inoutaccount reportInOutAccount, string strIdentityId, string strStartDate, string strEndDate)
    {
        bool boolResult = true;
        //站长ID
        boolResult &= reportInOutAccount.identityid == strIdentityId;
        //日期范围查询
        if (!string.IsNullOrWhiteSpace(strStartDate) && !string.IsNullOrWhiteSpace(strEndDate))
        {
            boolResult &= reportInOutAccount.AddTime >= DateTime.Parse(strStartDate) && reportInOutAccount.AddTime <= DateTime.Parse(strEndDate);
        }
        return boolResult;
    }
    /// <summary>
    /// 获取条件
    /// </summary>
    /// <param name="reportInOutAccount"></param>
    /// <param name="listUserId">会员Id的条件集合</param>
    /// <returns></returns>
    private bool GetReportAgentNewRepaidCondition(dt_report_newprepaid reportNewRepaid, string strIdentityId, string strStartDate, string strEndDate)
    {
        bool boolResult = true;
        //站长ID
        boolResult &= reportNewRepaid.identityid == strIdentityId;
        //日期范围查询
        if (!string.IsNullOrWhiteSpace(strStartDate) && !string.IsNullOrWhiteSpace(strEndDate))
        {
            boolResult &= reportNewRepaid.AddTime >= DateTime.Parse(strStartDate) && reportNewRepaid.AddTime <= DateTime.Parse(strEndDate);
        }
        return boolResult;
    }
    /// <summary>
    /// 获取条件
    /// </summary>
    /// <param name="reportInOutAccount"></param>
    /// <param name="listUserId">会员Id的条件集合</param>
    /// <returns></returns>
    private bool GetReportAgentAgentCondition(dt_report_agent reportAgent, string strIdentityId, string strStartDate, string strEndDate)
    {
        bool boolResult = true;
        //站长ID
        boolResult &= reportAgent.identityid == strIdentityId;
        //日期范围查询
        if (!string.IsNullOrWhiteSpace(strStartDate) && !string.IsNullOrWhiteSpace(strEndDate))
        {
            boolResult &= reportAgent.AddTime >= DateTime.Parse(strStartDate) && reportAgent.AddTime <= DateTime.Parse(strEndDate);
        }
        return boolResult;
    }
    /// <summary>
    /// 获取条件
    /// </summary>
    /// <param name="reportInOutAccount"></param>
    /// <param name="listUserId">会员Id的条件集合</param>
    /// <returns></returns>
    private bool GetReportAgentReportNumCondition(dt_report_num reportNum, string strIdentityId, string strStartDate, string strEndDate)
    {
        bool boolResult = true;
        //站长ID
        boolResult &= reportNum.identityid == strIdentityId;
        boolResult &= reportNum.Out_BettingAccount_Num >= 1;
        //日期范围查询
        if (!string.IsNullOrWhiteSpace(strStartDate) && !string.IsNullOrWhiteSpace(strEndDate))
        {
            boolResult &= reportNum.AddTime >= DateTime.Parse(strStartDate) && reportNum.AddTime <= DateTime.Parse(strEndDate);
        }
        return boolResult;
    }
    /// <summary>
    /// 获取会员的上级代理
    /// </summary>
    /// <returns></returns>
    private Dictionary<string, string> GetAgentUserData(DataTable userClassDataTable)
    {
        string strUserIds = string.Empty;
        //保存会员的所有上级代理
        Dictionary<string, string> dicAgentUserData = new Dictionary<string, string>();
        //临时保存上级代理
        Dictionary<string, string> dicTempAgent = new Dictionary<string, string>();
        for (var i = 0; i < userClassDataTable.Rows.Count; i++)
        {
            string strUserId = userClassDataTable.Rows[i]["user_id"].ToString();
            string strFatherId = userClassDataTable.Rows[i]["father_id"].ToString();
            if (!dicAgentUserData.ContainsKey(strUserId))
            {
                dicAgentUserData.Add(strUserId, strFatherId + ",");
                continue;
            }
            dicAgentUserData[strUserId] += strFatherId + ",";
        }
        return dicAgentUserData;
    }
    /// <summary>
    /// 统计数量
    /// </summary>
    /// <param name="dicNum"></param>
    /// <param name="strUserId"></param>
    /// <param name="usersDataTable"></param>
    public void ComputeNum(Dictionary<string, int> dicNum, string strUserId, Dictionary<string, string> dicAgentData)
    {
        if (!dicNum.ContainsKey(strUserId))
        {
            dicNum.Add(strUserId, 1);
        }
        else
        {
            dicNum[strUserId]++;
        }
        if (dicAgentData.ContainsKey(strUserId))
        {
            string[] strFatherIds = dicAgentData[strUserId].Split(',');
            for (int i = 0; i < strFatherIds.Length; i++)
            {
                string strFatherId = strFatherIds[i];
                if (!dicNum.ContainsKey(strFatherId))
                {
                    dicNum.Add(strFatherId, 1);
                }
                else
                {
                    dicNum[strFatherId]++;
                }
            }
        }

    }
    /// <summary>
    /// 统计数量
    /// </summary>
    /// <param name="dicNum"></param>
    /// <param name="strUserId"></param>
    /// <param name="usersDataTable"></param>
    public void ComputeNum(Dictionary<string, int> dicNum, string strUserId, DataTable usersDataTable)
    {
        //本人
        if (!dicNum.ContainsKey(strUserId))
        {
            dicNum.Add(strUserId, 1);
        }
        //所有上级代理加1
        DataRow[] dataRows = usersDataTable.Select("user_id=" + strUserId);
        for (int i = 0; i < dataRows.Length; i++)
        {
            string fatherId = dataRows[i]["father_id"].ToString();
            if (fatherId == "-1")
                continue;

            if (!dicNum.ContainsKey(fatherId))
            {
                dicNum.Add(fatherId, 1);
            }
            else
            {
                dicNum[fatherId]++;
            }
        }
    }
    /// <summary>
    /// 获取代理报表数据
    /// </summary>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strUserName">代理ID</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">截止日期</param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public DataSet GetReportAgentData(string strIdentityId, string strUserName, string strStartDate, string strEndDate, int pageIndex, int pageSize)
    {
        DataSet dataSet = new DataSet();
        try
        {
            //验证是否有权访问  
            //string strMsg = ValidateAuthority();
            //if (!string.IsNullOrWhiteSpace(strMsg))
            //{
            //    return dataSet;
            //}
            var redisClient = GlobalVariable.redisClient;
            var redisClientKey = GlobalVariable.redisClientKey;
            //1、获取代理档案
            string strWhereSql = GetReportAgentWhereSql(strIdentityId, strUserName, strStartDate, strEndDate);
            DataTable listUsers = DBcon.DbHelperSQL.Query("Select id as UserId,user_name as UserName from dt_users where " + strWhereSql).Tables[0];
            //2、获取站长添加所关心的代理的数据
            string strSql = "select UserId,UserName from dt_agent_show where identityid='" + strIdentityId + "' ";
            if (!string.IsNullOrWhiteSpace(strUserName))
            {
                strSql += " and UserId in (select id from dt_users  where user_name='" + strUserName.Trim() + "')";
            }
            DataTable agentUser = DBcon.DbHelperSQL.Query(strSql).Tables[0];
            if (listUsers.Rows.Count > 0)
            {
                agentUser.Merge(listUsers.Copy());
            }
            string userIds = string.Empty;
            Dictionary<string, string> dicAgent = new Dictionary<string, string>();
            List<ReportAgent> listAgentReport = new List<ReportAgent>();
            Dictionary<string, ReportAgent> dicReportAgent = new Dictionary<string, ReportAgent>();
            for (int i = 0; i < agentUser.Rows.Count; i++)
            {
                string strUserId = agentUser.Rows[i]["UserId"].ToString();
                userIds += strUserId + ",";
                if (dicAgent.ContainsKey(strUserId))
                    continue;

                dicAgent.Add(strUserId, agentUser.Rows[i]["UserName"].ToString());
                ReportAgent agentReport = new ReportAgent();
                agentReport.UserId = strUserId;
                if (!dicReportAgent.ContainsKey(strUserId))
                {
                    dicReportAgent.Add(strUserId, agentReport);
                    listAgentReport.Add(agentReport);
                }
            }
            //3、获取代理统计表
            DateTime dtStartTime = DateTime.Parse(strStartDate);
            DateTime dtEndTime = DateTime.Parse(strEndDate);
            //Expression<Func<dt_report_agent, bool>> reportAgentExpression = n => GetReportAgentAgentCondition(n, strIdentityId, strStartDate, strEndDate);
            //var reportAgentList = redisClient.GetAll<dt_report_agent>().Where(reportAgentExpression.Compile()).ToList();
            List<dt_report_agent> reportAgentList = redisHelper.Search<dt_report_agent>(redisClient, redisClientKey, dtStartTime, dtEndTime, strIdentityId).ToList();
            for (int i = 0; i < reportAgentList.Count; i++)
            {
                string strAgentId = reportAgentList[i].user_id.ToString();
                if (!dicAgent.ContainsKey(strAgentId))
                    continue;

                ReportAgent agentReport = new ReportAgent();
                if (!dicReportAgent.ContainsKey(strAgentId))
                {
                    dicReportAgent.Add(strAgentId, agentReport);
                    listAgentReport.Add(agentReport);
                }
                agentReport = dicReportAgent[strAgentId];
                //投注金额
                agentReport.TotalBettingAccount += reportAgentList[i].Out_BettingAccount;
                agentReport.TotalBettingAccount += reportAgentList[i].Lower_Out_BettingAccount;
                //中奖金额
                agentReport.TotalWinningAccount += reportAgentList[i].Out_WinningAccount;
                agentReport.TotalWinningAccount += reportAgentList[i].Lower_Out_WinningAccount;
                //充值金额
                agentReport.TotalInMoney += reportAgentList[i].InMoney;
                agentReport.TotalInMoney += reportAgentList[i].Lower_InMoney;
                //提现金额
                agentReport.TotalOutMoney += reportAgentList[i].Out_Account;
                agentReport.TotalOutMoney += reportAgentList[i].Lower_Out_Account;
                //活动优惠
                agentReport.TotalDiscountAccount += reportAgentList[i].ActivityDiscountMoney;
                agentReport.TotalDiscountAccount += reportAgentList[i].Lower_ActivityDiscountMoney;
                agentReport.TotalDiscountAccount += reportAgentList[i].OtherDiscountMoney;
                agentReport.TotalDiscountAccount += reportAgentList[i].Lower_OtherDiscountMoney;
                //团队返点
                agentReport.TotalRebateAccount += reportAgentList[i].Out_RebateAccount;
                agentReport.TotalRebateAccount += reportAgentList[i].Lower_Out_RebateAccount;
                //自身返点
                agentReport.RebateAccount += reportAgentList[i].Out_RebateAccount;
                //代理工资
                agentReport.TotalWages += reportAgentList[i].Wages;
                //代理分红
                agentReport.TotalWages += reportAgentList[i].Bonus;
            }
            userIds = userIds.Substring(0, userIds.Length - 1);
            //5、获取代理下的所有会员数据
            DataTable usersDataTable = DBcon.DbHelperSQL.Query("select user_id,father_id from dt_users_class where identityid='" + strIdentityId + "' and (father_id in(" + userIds + ") or user_id in (" + userIds + "))").Tables[0];
            Dictionary<string, string> dicAgentData = GetAgentUserData(usersDataTable);
            Dictionary<string, string> dicUsers = new Dictionary<string, string>();
            //6、获取注册人数
            DataTable regDataTable = DBcon.DbHelperSQL.Query("select UserId as user_id  from dt_users_register where identityid='" + strIdentityId + "' and Tester=0 and AddTime between '" + strStartDate + "' and '" + strEndDate
                + "'").Tables[0];
            Dictionary<string, int> dicRegNum = new Dictionary<string, int>();
            //7、获取汇总统计表数据
            //多条件组合
            //首充人数统计表
            Expression<Func<dt_report_newprepaid, bool>> newRepaidExpression = n => GetReportAgentNewRepaidCondition(n, strIdentityId, strStartDate, strEndDate);
            //var newPrepaidList = redisClient.GetAll<dt_report_newprepaid>().Where(newRepaidExpression.Compile()).ToList();
            List<dt_report_newprepaid> newPrepaidList = redisHelper.SearchByUserId<dt_report_newprepaid>(redisClient, redisClientKey, "").Where(newRepaidExpression.Compile()).ToList();
            //出入款次数统计
            //Expression<Func<dt_report_num, bool>> reportNumExpression = n => GetReportAgentReportNumCondition(n, strIdentityId, strStartDate, strEndDate);
            //var reportNumList = redisClient.GetAll<dt_report_num>().Where(reportNumExpression.Compile()).GroupBy(m => m.user_id).ToList();
            var reportNumList = redisHelper.Search<dt_report_num>(redisClient, redisClientKey, dtStartTime, dtEndTime, strIdentityId).Where(m => m.Out_BettingAccount_Num >= 1).GroupBy(m => m.user_id).ToList();
            redisClient.Quit();
            redisClient.Dispose();
            redisClientKey.Quit();
            redisClientKey.Dispose();
            //首充人数
            Dictionary<string, int> dicNewPrepaid = new Dictionary<string, int>();
            //投注人数
            Dictionary<string, int> dicBetNum = new Dictionary<string, int>();
            //注册人数
            for (int i = 0; i < regDataTable.Rows.Count; i++)
            {
                string strUserId = regDataTable.Rows[i]["user_id"].ToString();
                ComputeNum(dicRegNum, strUserId, dicAgentData);
            }
            //首充人数
            for (int i = 0; i < newPrepaidList.Count; i++)
            {
                string strUserId = newPrepaidList[i].user_id.ToString();
                ComputeNum(dicNewPrepaid, strUserId, dicAgentData);
            }
            //投注人数
            for (int i = 0; i < reportNumList.Count; i++)
            {
                string strUserId = reportNumList[i].Key.ToString();
                ComputeNum(dicBetNum, strUserId, dicAgentData);
            }
            //默认排序投注递减
            listAgentReport = listAgentReport.OrderByDescending(m => m.TotalBettingAccount).ToList();
            int totalCount = listAgentReport.Count;
            //保存记录总数量的表
            DataTable totalDataTable = new DataTable();
            DataColumn dataColumn = new DataColumn();
            dataColumn.ColumnName = "totalCount";
            dataColumn.DataType = typeof(Int64);
            totalDataTable.Columns.Add(dataColumn);
            DataRow newDataRow = totalDataTable.NewRow();
            newDataRow["totalCount"] = totalCount;
            totalDataTable.Rows.Add(newDataRow);
            dataSet.Tables.Add(totalDataTable);
            listAgentReport = listAgentReport.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
            for (int i = 0; i < listAgentReport.Count; i++)
            {
                string strAgentId = listAgentReport[i].UserId;
                //总代名称
                if (dicAgent.ContainsKey(strAgentId))
                {
                    listAgentReport[i].UserName = dicAgent[strAgentId];
                }
                //投注人数
                if (dicBetNum.ContainsKey(strAgentId))
                {
                    listAgentReport[i].TotalBettingNum = dicBetNum[strAgentId];
                }
                //首充人数
                if (dicNewPrepaid.ContainsKey(strAgentId))
                {
                    listAgentReport[i].NewPrepaidNum = dicNewPrepaid[strAgentId];
                }
                //注册人数
                if (dicRegNum.ContainsKey(strAgentId))
                {
                    listAgentReport[i].RegNum = dicRegNum[strAgentId];
                }
                //盈利=中奖-投注+团队返点+团队活动优惠
                listAgentReport[i].ProfitLoss = listAgentReport[i].TotalWinningAccount - listAgentReport[i].TotalBettingAccount + listAgentReport[i].TotalDiscountAccount + listAgentReport[i].TotalRebateAccount;
            }
            DataTableHelper dataTableHelper = new DataTableHelper();
            DataTable dataTable = dataTableHelper.ConvertListToDataTable(listAgentReport);
            dataSet.Tables.Add(dataTable);
            return dataSet;
        }
        catch (Exception ex)
        {
            Log.Error(ex);
            return null;
        }
    }
    /// <summary>
    /// 获取等级报表数据
    /// </summary>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strUserName">代理ID</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">截止日期</param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public DataSet GetReportGroupData(string strIdentityId, string strStartDate, string strEndDate, int pageIndex, int pageSize)
    {
        DataSet dataSet = new DataSet();
        try
        {
            //验证是否有权访问  
            string strMsg = ValidateAuthority();
            if (!string.IsNullOrWhiteSpace(strMsg))
            {
                return dataSet;
            }
            var redisClient = GlobalVariable.redisClient;
            var redisClientKey = GlobalVariable.redisClientKey;
            //获取所有等级（黑名单、VIP1到VIP9）
            Dictionary<string, ReportGroup> dicReportGroup = new Dictionary<string, ReportGroup>();
            List<ReportGroup> listReportGroup = new List<ReportGroup>();
            DataTable groupDataTable = DBcon.DbHelperSQL.Query("Select grade,title from dt_user_groups where identityid='" + strIdentityId + "' and grade!=-1 order by grade").Tables[0];
            for (int i = 0; i < groupDataTable.Rows.Count; i++)
            {
                string strTitle = groupDataTable.Rows[i]["title"].ToString();
                ReportGroup reportGroup = new ReportGroup();
                if (dicReportGroup.ContainsKey(strTitle))
                    continue;

                reportGroup.group_name = strTitle;
                dicReportGroup.Add(strTitle, reportGroup);
                listReportGroup.Add(reportGroup);
            }
            //1、获取首充记录表数据
            List<dt_report_inoutaccount> listReportInOutAccount = new List<dt_report_inoutaccount>();
            //保存记录总数量的表
            DataTable totalDataTable = new DataTable();
            DataColumn dataColumn = new DataColumn();
            dataColumn.ColumnName = "totalCount";
            dataColumn.DataType = typeof(Int64);
            totalDataTable.Columns.Add(dataColumn);
            //出入账日报统计表
            //listReportInOutAccount = redisClient.GetAll<dt_report_inoutaccount>().Where(m => m.identityid == strIdentityId && m.AddTime >= DateTime.Parse(strStartDate) && m.AddTime <= DateTime.Parse(strEndDate)).ToList();
            DateTime dtStartTime = DateTime.Parse(strStartDate);
            DateTime dtEndTime = DateTime.Parse(strEndDate);
            listReportInOutAccount = redisHelper.Search<dt_report_inoutaccount>(redisClient, redisClientKey, dtStartTime, dtEndTime, strIdentityId).ToList();
            int totalCount = listReportInOutAccount.Count;
            DataTableHelper dataTableHelper = new DataTableHelper();
            DataTable dataTable = null;
            DataRow newDataRow = totalDataTable.NewRow();
            newDataRow["totalCount"] = totalCount;
            totalDataTable.Rows.Add(newDataRow);
            dataSet.Tables.Add(totalDataTable);
            if (listReportInOutAccount.Count < 1)
            {
                dataTable = dataTableHelper.ConvertListToDataTable(listReportInOutAccount);
                dataSet.Tables.Add(dataTable);
                return dataSet;
            }
            //次数日报统计表
            //List<dt_report_num> listReportNum = redisClient.GetAll<dt_report_num>().Where(m => m.identityid == strIdentityId && m.AddTime >= DateTime.Parse(strStartDate) && m.AddTime <= DateTime.Parse(strEndDate)).ToList();
            List<dt_report_num> listReportNum = redisHelper.Search<dt_report_num>(redisClient, redisClientKey, dtStartTime, dtEndTime, strIdentityId).ToList();
            redisClient.Quit();
            redisClient.Dispose();
            redisClientKey.Quit();
            redisClientKey.Dispose();
            //用来校验统计投注人数
            Dictionary<string, string> dicBetUserGroup = new Dictionary<string, string>();
            //用来校验统计充值人数
            Dictionary<string, string> dicPrepaidUserGroup = new Dictionary<string, string>();
            for (int i = 0; i < listReportInOutAccount.Count; i++)
            {
                //等级名称
                string strGroupName = listReportInOutAccount[i].group_name;
                ReportGroup reportGroup = new ReportGroup();
                if (!dicReportGroup.ContainsKey(strGroupName))
                {
                    dicReportGroup.Add(strGroupName, reportGroup);
                    listReportGroup.Add(reportGroup);
                }
                reportGroup = dicReportGroup[strGroupName];
                //等级名称
                reportGroup.group_name = strGroupName;
                //会员Id
                string strUserId = listReportInOutAccount[i].user_id.ToString();
                string strKey = strGroupName + "_" + strUserId;
                //投注金额
                decimal fBetMoney = listReportInOutAccount[i].Out_BettingAccount;
                if (fBetMoney > 0)
                {
                    if (!dicBetUserGroup.ContainsKey(strKey))
                    {
                        dicBetUserGroup.Add(strKey, strKey);
                        //投注人数
                        reportGroup.BetNum++;
                    }

                    //投注金额
                    reportGroup.BetMoney += fBetMoney;
                }
                //快捷充值
                decimal fFastPrepaid = listReportInOutAccount[i].In_FastPrepaid;
                //银行转账
                decimal fBankPrepaid = listReportInOutAccount[i].In_BankPrepaid;
                //支付宝充值
                decimal fAlipayPrepaid = listReportInOutAccount[i].In_AlipayPrepaid;
                //微信充值
                decimal fWeChatPrepaid = listReportInOutAccount[i].In_WeChatPrepaid;
                //人工存款
                decimal fArtificialDeposit = listReportInOutAccount[i].In_ArtificialDeposit;
                if (fFastPrepaid > 0 || fBankPrepaid > 0 || fAlipayPrepaid > 0 || fWeChatPrepaid > 0 || fArtificialDeposit > 0)
                {
                    //充值金额
                    reportGroup.PrepaidMoney += fFastPrepaid;
                    reportGroup.PrepaidMoney += fBankPrepaid;
                    reportGroup.PrepaidMoney += fAlipayPrepaid;
                    reportGroup.PrepaidMoney += fWeChatPrepaid;
                    reportGroup.PrepaidMoney += fArtificialDeposit;
                    if (!dicPrepaidUserGroup.ContainsKey(strKey))
                    {
                        dicPrepaidUserGroup.Add(strKey, strKey);
                        //充值人数
                        reportGroup.PrepaidNum++;
                    }
                }
                //提现金额
                reportGroup.OutMoney += listReportInOutAccount[i].Out_Account;
                //中奖金额
                reportGroup.WinMoney += listReportInOutAccount[i].Out_WinningAccount;
                //返点金额
                reportGroup.Out_RebateAccount += listReportInOutAccount[i].Out_RebateAccount;
                //活动礼金
                reportGroup.ActivityDiscountAccountMoney += listReportInOutAccount[i].Out_ActivityDiscountAccount;
                //盈利＝投注-中奖-返点-活动-其他优惠-代理工资-代理分红+拒绝总额+行政提出
                reportGroup.ProfitLoss += listReportInOutAccount[i].Out_BettingAccount;
                reportGroup.ProfitLoss -= listReportInOutAccount[i].Out_WinningAccount;
                reportGroup.ProfitLoss -= listReportInOutAccount[i].Out_RebateAccount;
                reportGroup.ProfitLoss -= listReportInOutAccount[i].Out_ActivityDiscountAccount;
                reportGroup.ProfitLoss -= listReportInOutAccount[i].Out_OtherDiscountAccount;
                reportGroup.ProfitLoss -= listReportInOutAccount[i].Wages;
                reportGroup.ProfitLoss -= listReportInOutAccount[i].Bonus;
                reportGroup.ProfitLoss += listReportInOutAccount[i].Out_RefuseAccount;
                reportGroup.ProfitLoss += listReportInOutAccount[i].Out_AdministrationAccount;
                //盈率：盈率=盈利/投注*100
                if (reportGroup.BetMoney != 0)
                {
                    reportGroup.WinRate = reportGroup.ProfitLoss / reportGroup.BetMoney * 100;
                }
            }
            totalCount = listReportGroup.Count;
            totalDataTable.Rows[0]["totalCount"] = totalCount;
            listReportGroup = listReportGroup.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
            for (int i = 0; i < listReportGroup.Count; i++)
            {
                string strGroupName = listReportGroup[i].group_name;
                //投注单量
                listReportGroup[i].BetOrderNum = listReportNum.Where(m => m.group_name == strGroupName).Sum(m => m.Out_BettingAccount_Num);
            }
            dataTable = dataTableHelper.ConvertListToDataTable(listReportGroup);
            dataSet.Tables.Add(dataTable);
            return dataSet;
        }
        catch (Exception ex)
        {
            Log.Error(ex);
            return null;
        }
    }
    /// <summary>
    /// 彩种报表获取条件
    /// </summary>
    /// <param name="reportLottery"></param>
    /// <param name="listUserId">会员Id的条件集合</param>
    /// <returns></returns>
    private bool GetReportLotteryCondition(dt_report_lottery reportLottery, string strIdentityId, string strStartDate, string strEndDate, string strLotteryCode)
    {
        bool boolResult = true;
        //站长ID
        boolResult &= reportLottery.identityid == strIdentityId;
        //日期范围查询
        if (!string.IsNullOrWhiteSpace(strStartDate) && !string.IsNullOrWhiteSpace(strEndDate))
        {
            boolResult &= reportLottery.AddTime >= DateTime.Parse(strStartDate) && reportLottery.AddTime <= DateTime.Parse(strEndDate);
        }
        //彩种编码
        if (!string.IsNullOrWhiteSpace(strLotteryCode))
        {
            boolResult &= reportLottery.LotteryCode == strLotteryCode;
        }
        return boolResult;
    }
    /// <summary>
    /// 获取彩种报表查询条件SQL语句
    /// </summary>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strLotteryCode">彩种编码</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">截止日期</param>
    /// <returns></returns>
    private string GetReportLotteryWhereSql(string strIdentityId, string strLotteryCode, string strStartDate, string strEndDate)
    {
        StringBuilder strTemp = new StringBuilder();
        //商户Id
        strTemp.Append("identityid='" + strIdentityId + "'");
        if (!string.IsNullOrEmpty(strLotteryCode) && strLotteryCode != "0000")
        {
            strTemp.Append(" and lottery_code = '" + strLotteryCode + "'");
        }
        if (!string.IsNullOrEmpty(strStartDate) && !string.IsNullOrEmpty(strEndDate))
        {
            strTemp.Append(" and add_time between '" + strStartDate + "' and '" + strEndDate + "'");
        }
        else
        {
            strStartDate = DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00";
            strEndDate = DateTime.Now.ToString("yyyy-MM-dd") + " 23:59:59";
            strTemp.Append(" and add_time between '" + strStartDate + "' and '" + strEndDate + "'");
        }
        return strTemp.ToString();
    }
    /// <summary>
    /// 获取彩种报表数据
    /// </summary>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strLotteryCode">彩种编码</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">截止日期</param>
    /// <param name="pageIndex">当前页号</param>
    /// <param name="pageSize">每一页多少条记录</param>
    /// <param name="strOrderBy">排序</param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public DataSet GetReportLotteryData(string strIdentityId, string strLotteryCode, string strStartDate, string strEndDate, int pageIndex, int pageSize, string strOrderBy)
    {
        DataSet dataSet = new DataSet();
        try
        {
            //验证是否有权访问  
            string strMsg = ValidateAuthority();
            if (!string.IsNullOrWhiteSpace(strMsg))
            {
                return dataSet;
            }
            //获取汇总数据
            var redisClient = GlobalVariable.redisClient;
            var redisClientKey = GlobalVariable.redisClientKey;
            //Expression<Func<dt_report_lottery, bool>> expression = n => GetReportLotteryCondition(n, strIdentityId, strStartDate, strEndDate, strLotteryCode);
            //var lotteryList = redisClient.GetAll<dt_report_lottery>().Where(expression.Compile()).ToList();
            DateTime dtStartTime = DateTime.Parse(strStartDate);
            DateTime dtEndTime = DateTime.Parse(strEndDate);
            List<dt_report_lottery> lotteryList = redisHelper.Search<dt_report_lottery>(redisClient, redisClientKey, dtStartTime, dtEndTime, strIdentityId).ToList();
            string strWhereSql = GetReportLotteryWhereSql(strIdentityId, strLotteryCode, strStartDate, strEndDate);
            DataTable lotteryDataTable = DBcon.DbHelperSQL.Query("select a.lottery_code,a.user_id from dt_lottery_orders a join dt_users b on a.user_id=b.id where a." + strWhereSql + " and b.flog=0 and a.openState in (1,2) group by a.lottery_code,a.user_id").Tables[0];
            redisClient.Quit();
            redisClient.Dispose();
            redisClientKey.Quit();
            redisClientKey.Dispose();
            int lotteryListCount = lotteryList.Count;
            Dictionary<string, ReportLottery> dicLottery = new Dictionary<string, ReportLottery>();
            List<ReportLottery> listLottery = new List<ReportLottery>();
            for (var i = 0; i < lotteryListCount; i++)
            {
                string tempLotteryCode = lotteryList[i].LotteryCode;
                ReportLottery reportLottery = new ReportLottery();
                if (!dicLottery.ContainsKey(tempLotteryCode))
                {
                    dicLottery.Add(tempLotteryCode, reportLottery);
                    listLottery.Add(reportLottery);
                    //投注名称
                    reportLottery.LotteryName = lotteryList[i].LotteryName;
                    //投注人数
                    DataRow[] lotteryDataRows = lotteryDataTable.Select("lottery_code='" + tempLotteryCode + "'");
                    if (lotteryDataRows != null)
                    {
                        reportLottery.BetNum = lotteryDataRows.Length;
                    }
                }
                reportLottery = dicLottery[tempLotteryCode];
                //投注总额
                reportLottery.BetMoney += lotteryList[i].BetMoney;
                //中奖总额
                reportLottery.WinMoney += lotteryList[i].WinMoney;
                //撤单总额
                reportLottery.CancelMoney += lotteryList[i].CancelMoney;
                //返点总额
                reportLottery.RebateMoney += lotteryList[i].RebateMoney;
                //盈利总额＝投注-中奖－返点
                reportLottery.ProfitMoney += lotteryList[i].BetMoney;
                reportLottery.ProfitMoney -= lotteryList[i].WinMoney;
                reportLottery.ProfitMoney -= lotteryList[i].RebateMoney;
                //盈率=盈利总额/投注总额*100
                if (reportLottery.BetMoney != 0)
                {
                    reportLottery.ProfitRate = reportLottery.ProfitMoney / reportLottery.BetMoney * 100;
                }
            }
            int totalCount = listLottery.Count;
            DataTableHelper dataTableHelper = new DataTableHelper();
            DataTable totalDataTable = new DataTable();
            DataColumn dataColumn = new DataColumn();
            dataColumn.ColumnName = "totalCount";
            dataColumn.DataType = typeof(Int64);
            totalDataTable.Columns.Add(dataColumn);
            DataRow newDataRow = totalDataTable.NewRow();
            newDataRow["totalCount"] = totalCount;
            totalDataTable.Rows.Add(newDataRow);
            dataSet.Tables.Add(totalDataTable);
            if (string.IsNullOrWhiteSpace(strOrderBy))
            {
                strOrderBy = "betting_money desc";
            }
            //投注递减
            if (strOrderBy == "betting_money desc")
            {
                listLottery = listLottery.OrderByDescending(m => m.BetMoney).ToList();
            }
            //盈利递减
            if (strOrderBy == "profits_money desc")
            {
                listLottery = listLottery.OrderByDescending(m => m.ProfitMoney).ToList();
            }
            //盈利递增
            if (strOrderBy == "profits_money asc")
            {
                listLottery = listLottery.OrderBy(m => m.ProfitMoney).ToList();
            }
            //盈率递减
            if (strOrderBy == "profits_rates desc")
            {
                listLottery = listLottery.OrderByDescending(m => m.ProfitRate).ToList();
            }
            //盈率递增
            if (strOrderBy == "profits_rates asc")
            {
                listLottery = listLottery.OrderBy(m => m.ProfitRate).ToList();
            }
            listLottery = listLottery.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
            DataTable dataTable = dataTableHelper.ConvertListToDataTable(listLottery);
            dataSet.Tables.Add(dataTable);
            return dataSet;
        }
        catch (Exception ex)
        {
            Log.Error(ex);
            return null;
        }
    }
    /// <summary>
    /// 会员报表获取条件
    /// </summary>
    /// <param name="reportInOutAccount"></param>
    /// <param name="listUserId">会员Id的条件集合</param>
    /// <returns></returns>
    private bool GetReportMemberCondition(dt_report_inoutaccount reportInOutAccount, string strIdentityId, string strStartDate, string strEndDate, string strUserId)
    {
        bool boolResult = true;
        //站长ID
        boolResult &= reportInOutAccount.identityid == strIdentityId;
        //日期范围查询
        if (!string.IsNullOrWhiteSpace(strStartDate) && !string.IsNullOrWhiteSpace(strEndDate))
        {
            boolResult &= reportInOutAccount.AddTime >= DateTime.Parse(strStartDate) && reportInOutAccount.AddTime <= DateTime.Parse(strEndDate);
        }
        //会员
        if (!string.IsNullOrWhiteSpace(strUserId))
        {
            if (strUserId == "-1")
            {
                boolResult &= false;
            }
            else
            {
                boolResult &= reportInOutAccount.user_id == int.Parse(strUserId);
            }
        }
        return boolResult;
    }
    /// <summary>
    /// 获取会员报表数据
    /// </summary>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strUserName">会员名称</param>
    /// <param name="strAgentName">代理名称</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">截止日期</param>
    /// <param name="pageIndex">当前页号</param>
    /// <param name="pageSize">每一页多少条记录</param>
    /// <param name="strOrderBy">排序</param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public DataSet GetReportMemberData(string strIdentityId, string strUserName, string strAgentName, string strStartDate, string strEndDate, int pageIndex, int pageSize, string strOrderBy)
    {
        DataSet dataSet = new DataSet();
        try
        {
            //验证是否有权访问  
            string strMsg = ValidateAuthority();
            if (!string.IsNullOrWhiteSpace(strMsg))
            {
                return dataSet;
            }
            //获取汇总数据
            var redisClient = GlobalVariable.redisClient;
            var redisClientKey = GlobalVariable.redisClientKey;
            //多条件组合
            //会员Id
            string strUserId = string.Empty;
            if (!string.IsNullOrWhiteSpace(strUserName))
            {
                DataTable userData = DBcon.DbHelperSQL.Query("select id from dt_users where identityid='" + strIdentityId + "' and user_name='" + strUserName + "'").Tables[0];
                if (userData != null && userData.Rows.Count > 0)
                {
                    strUserId = userData.Rows[0]["id"].ToString();
                }
                else
                {
                    strUserId = "-1";
                }
            }
            //上级代理Id
            string strAgentId = string.Empty;
            DataTable agentDataTable = null;
            if (!string.IsNullOrWhiteSpace(strAgentName))
            {
                agentDataTable = DBcon.DbHelperSQL.Query(@"select father_id,user_id from dt_users_class where identityid='" + strIdentityId +
                                                         "' and father_id in (select id from dt_users where user_name='" + strAgentName + "')").Tables[0];
            }
            //Expression<Func<dt_report_inoutaccount, bool>> expression = n => GetReportMemberCondition(n, strIdentityId, strStartDate, strEndDate, strUserId);
            //var inOutAccountList = redisClient.GetAll<dt_report_inoutaccount>().Where(expression.Compile()).ToList();
            DateTime dtStartTime = DateTime.Parse(strStartDate);
            DateTime dtEndTime = DateTime.Parse(strEndDate);
            List<dt_report_inoutaccount> inOutAccountList = null;
            if (!string.IsNullOrWhiteSpace(strUserId))
            {
                inOutAccountList = redisHelper.Search<dt_report_inoutaccount>(redisClient, redisClientKey, dtStartTime, dtEndTime, strUserId,"").Where(m => m.identityid == strIdentityId).ToList();
            }
            else
            {
                inOutAccountList = redisHelper.Search<dt_report_inoutaccount>(redisClient, redisClientKey, dtStartTime, dtEndTime, strIdentityId).ToList();
            }
            redisClient.Quit();
            redisClient.Dispose();
            redisClientKey.Quit();
            redisClientKey.Dispose();
            int inOutAccountCount = inOutAccountList.Count;
            Dictionary<string, ReportMember> dicReportMember = new Dictionary<string, ReportMember>();
            Dictionary<string, string> dicUsers = new Dictionary<string, string>();
            List<ReportMember> listReportMember = new List<ReportMember>();
            for (int i = 0; i < inOutAccountCount; i++)
            {
                strUserId = inOutAccountList[i].user_id.ToString();
                strAgentId = inOutAccountList[i].agent_id.ToString();
                if (agentDataTable != null)
                {
                    DataRow[] dataRows = agentDataTable.Select("user_id=" + strUserId);
                    if (dataRows == null || dataRows.Length < 1)
                        continue;
                }
                ReportMember reportMember = new ReportMember();
                if (!dicReportMember.ContainsKey(strUserId))
                {
                    dicReportMember.Add(strUserId, reportMember);
                    listReportMember.Add(reportMember);
                    dicUsers.Add(strUserId, strAgentId);
                }
                reportMember = dicReportMember[strUserId];
                //会员Id
                reportMember.UserId = strUserId;
                //代理ID
                reportMember.AgentId = strAgentId;
                //等级名称
                reportMember.GroupName = inOutAccountList[i].group_name;
                //入款总额
                reportMember.InMoney += inOutAccountList[i].In_FastPrepaid;
                reportMember.InMoney += inOutAccountList[i].In_AlipayPrepaid;
                reportMember.InMoney += inOutAccountList[i].In_ArtificialDeposit;
                reportMember.InMoney += inOutAccountList[i].In_BankPrepaid;
                reportMember.InMoney += inOutAccountList[i].In_WeChatPrepaid;
                //出款总额
                reportMember.OutMoney += inOutAccountList[i].Out_Account;
                //投注总额
                reportMember.BetMoney += inOutAccountList[i].Out_BettingAccount;
                //中奖总额
                reportMember.WinMoney += inOutAccountList[i].Out_WinningAccount;
                //返点总额
                reportMember.RebateMoney += inOutAccountList[i].Out_RebateAccount;
                //优惠总额
                reportMember.DiscountMoney += inOutAccountList[i].Out_ActivityDiscountAccount;
                reportMember.DiscountMoney += inOutAccountList[i].Out_OtherDiscountAccount;
                //盈利总额=中奖-投注+返点+活动+其他优惠+代理工资+代理分红-拒绝总额-行政提出
                reportMember.ProfitLoss += inOutAccountList[i].Out_WinningAccount;
                reportMember.ProfitLoss -= inOutAccountList[i].Out_BettingAccount;
                reportMember.ProfitLoss += inOutAccountList[i].Out_RebateAccount;
                reportMember.ProfitLoss += inOutAccountList[i].Out_ActivityDiscountAccount;
                reportMember.ProfitLoss += inOutAccountList[i].Out_OtherDiscountAccount;
                reportMember.ProfitLoss += inOutAccountList[i].Wages;
                reportMember.ProfitLoss += inOutAccountList[i].Bonus;
                reportMember.ProfitLoss -= inOutAccountList[i].Out_RefuseAccount;
                reportMember.ProfitLoss -= inOutAccountList[i].Out_AdministrationAccount;
                //盈率
                decimal fProfitRate = 0;
                if (reportMember.BetMoney != 0)
                {
                    fProfitRate = reportMember.ProfitLoss / reportMember.BetMoney * 100;
                }
                reportMember.ProfitRate = fProfitRate;
            }
            //获取会员信息表
            string strUserIdWhereSql = string.Empty;
            string strUserCapitalWhereSql = string.Empty;
            Dictionary<string, string> dicTempUsers = new Dictionary<string, string>();
            foreach (KeyValuePair<string, string> user in dicUsers)
            {
                if (!dicTempUsers.ContainsKey(user.Key))
                {
                    dicTempUsers.Add(user.Key, user.Key);
                    strUserIdWhereSql += user.Key + ",";
                }
                if (!dicTempUsers.ContainsKey(user.Value))
                {
                    dicTempUsers.Add(user.Value, user.Value);
                    strUserIdWhereSql += user.Value + ",";
                }
            }
            if (!string.IsNullOrWhiteSpace(strUserIdWhereSql))
            {
                strUserIdWhereSql = strUserIdWhereSql.Substring(0, strUserIdWhereSql.Length - 1);
                strUserCapitalWhereSql = " user_id in(" + strUserIdWhereSql + ")";
                strUserIdWhereSql = " id in(" + strUserIdWhereSql + ")";
            }
            else
            {
                strUserIdWhereSql = "1=2";
                strUserCapitalWhereSql = "1=2";
            }
            DataTable userDataTable = DBcon.DbHelperSQL.Query("select id,user_name from dt_users where identityid='" + strIdentityId + "' and " + strUserIdWhereSql).Tables[0];
            //3、获取会员的余额
            DataTable userCapitalDataTable = DBcon.DbHelperSQL.Query(string.Format(@"select user_id,use_money from dt_user_capital where " + strUserCapitalWhereSql)).Tables[0];
            for (int i = 0; i < listReportMember.Count; i++)
            {
                strUserId = listReportMember[i].UserId;
                strAgentId = listReportMember[i].AgentId;
                //会员名称
                DataRow[] userDataRow = userDataTable.Select("id=" + strUserId);
                if (userDataRow != null && userDataRow.Length > 0)
                {
                    listReportMember[i].UserName = userDataRow[0]["user_name"].ToString();
                }
                //代理名称
                DataRow[] agentDataRow = userDataTable.Select("id=" + strAgentId);
                if (agentDataRow != null && agentDataRow.Length > 0)
                {
                    listReportMember[i].AgentName = agentDataRow[0]["user_name"].ToString();
                }
                //会员余额
                DataRow[] userCapitalDataRow = userCapitalDataTable.Select("user_id=" + strUserId);
                if (userCapitalDataRow != null && userCapitalDataRow.Length > 0)
                {
                    object bjBalance = userCapitalDataRow[0]["use_money"];
                    if (bjBalance != null && !string.IsNullOrWhiteSpace(bjBalance.ToString()))
                    {
                        listReportMember[i].Balance = decimal.Parse(bjBalance.ToString());
                    }
                }
            }
            //排序
            //投注递减
            if (strOrderBy == "BetMoney desc" || string.IsNullOrWhiteSpace(strOrderBy))
            {
                listReportMember = listReportMember.OrderByDescending(m => m.BetMoney).ToList();
            }
            //盈利递减
            if (strOrderBy == "ProfitLoss desc")
            {
                listReportMember = listReportMember.OrderByDescending(m => m.ProfitLoss).ToList();
            }
            //盈利递增
            if (strOrderBy == "ProfitLoss asc")
            {
                listReportMember = listReportMember.OrderBy(m => m.ProfitLoss).ToList();
            }
            //盈率递减
            if (strOrderBy == "ProfitRate desc")
            {
                listReportMember = listReportMember.OrderByDescending(m => m.ProfitRate).ToList();
            }
            //盈率递增
            if (strOrderBy == "ProfitRate asc")
            {
                listReportMember = listReportMember.OrderBy(m => m.ProfitRate).ToList();
            }
            //入款递减
            if (strOrderBy == "InMoney desc")
            {
                listReportMember = listReportMember.OrderByDescending(m => m.InMoney).ToList();
            }
            //出款递减
            if (strOrderBy == "OutMoney desc")
            {
                listReportMember = listReportMember.OrderByDescending(m => m.OutMoney).ToList();
            }
            //返点递减
            if (strOrderBy == "RebateMoney desc")
            {
                listReportMember = listReportMember.OrderByDescending(m => m.RebateMoney).ToList();
            }
            //活动礼金递减
            if (strOrderBy == "DiscountMoney desc")
            {
                listReportMember = listReportMember.OrderByDescending(m => m.DiscountMoney).ToList();
            }
            int totalCount = listReportMember.Count;
            DataTableHelper dataTableHelper = new DataTableHelper();
            DataTable totalDataTable = new DataTable();
            DataColumn dataColumn = new DataColumn();
            dataColumn.ColumnName = "totalCount";
            dataColumn.DataType = typeof(Int64);
            totalDataTable.Columns.Add(dataColumn);
            DataRow newDataRow = totalDataTable.NewRow();
            newDataRow["totalCount"] = totalCount;
            totalDataTable.Rows.Add(newDataRow);
            dataSet.Tables.Add(totalDataTable);
            listReportMember = listReportMember.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
            DataTable dataTable = dataTableHelper.ConvertListToDataTable(listReportMember);
            dataSet.Tables.Add(dataTable);
            return dataSet;
        }
        catch (Exception ex)
        {
            Log.Error(ex);
            return null;
        }
    }
    /// <summary>
    /// 获取条件
    /// </summary>
    /// <param name="reportInOutAccount"></param>
    /// <param name="listUserId">会员Id的条件集合</param>
    /// <returns></returns>
    private bool GetReportNewPrepaidCondition(dt_report_newprepaid reportInOutAccount, string strIdentityId, string strStartDate, string strEndDate, string strUserId, string strAgentId)
    {
        bool boolResult = true;
        //站长ID
        boolResult &= reportInOutAccount.identityid == strIdentityId;
        //日期范围查询
        if (!string.IsNullOrWhiteSpace(strStartDate) && !string.IsNullOrWhiteSpace(strEndDate))
        {
            boolResult &= reportInOutAccount.AddTime >= DateTime.Parse(strStartDate) && reportInOutAccount.AddTime <= DateTime.Parse(strEndDate);
        }
        //会员Id
        if (!string.IsNullOrWhiteSpace(strUserId))
        {
            boolResult &= reportInOutAccount.user_id == int.Parse(strUserId);
        }
        //代理ID
        if (!string.IsNullOrWhiteSpace(strAgentId))
        {
            boolResult &= reportInOutAccount.agent_id == int.Parse(strAgentId);
        }
        return boolResult;
    }
    /// <summary>
    /// 获取新充报表数据
    /// </summary>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strUserName">会员名称</param>
    /// <param name="strAgentName">代理名称</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">截止日期</param>
    /// <param name="pageIndex">当前页号</param>
    /// <param name="pageSize">每一页多少条记录</param>
    /// <param name="strOrderBy">排序</param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public DataSet GetReportNewPrepaidData(string strIdentityId, string strUserName, string strAgentName, string strStartDate, string strEndDate, int pageIndex, int pageSize, string strOrderBy)
    {
        DataSet dataSet = new DataSet();
        try
        {
            //验证是否有权访问  
            string strMsg = ValidateAuthority();
            if (!string.IsNullOrWhiteSpace(strMsg))
            {
                return dataSet;
            }
            //会员账号
            string strUserId = string.Empty;
            if (!string.IsNullOrWhiteSpace(strUserName))
            {
                DataTable userDataTable = DBcon.DbHelperSQL.Query("select id from dt_users where user_name='" + strUserName + "'").Tables[0];
                if (userDataTable != null && userDataTable.Rows.Count > 0)
                {
                    strUserId = userDataTable.Rows[0]["id"].ToString();
                }
                else
                {
                    strUserId = "-1";
                }
            }
            //代理名称
            string strAgentId = string.Empty;
            if (!string.IsNullOrWhiteSpace(strAgentName))
            {
                DataTable userDataTable = DBcon.DbHelperSQL.Query("select id from dt_users where user_name='" + strAgentName + "'").Tables[0];
                if (userDataTable != null && userDataTable.Rows.Count > 0)
                {
                    strAgentId = userDataTable.Rows[0]["id"].ToString();
                }
                else
                {
                    strAgentId = "-1";
                }
            }
            if (string.IsNullOrWhiteSpace(strOrderBy))
            {
                strOrderBy = "add_time desc";
            }
            //1、获取首充记录表数据
            var redisClient = GlobalVariable.redisClient;
            var redisClientKey = GlobalVariable.redisClientKey;
            //多条件组合
            Expression<Func<dt_report_newprepaid, bool>> expression = n => GetReportNewPrepaidCondition(n, strIdentityId, strStartDate, strEndDate, strUserId, strAgentId);
            //List<dt_report_newprepaid> listReportNewPrepaid = redisClient.GetAll<dt_report_newprepaid>().Where(expression.Compile()).ToList();
            DateTime dtStartTime = DateTime.Parse(strStartDate);
            DateTime dtEndTime = DateTime.Parse(strEndDate);
            List<dt_report_newprepaid> listReportNewPrepaid = redisHelper.SearchByUserId<dt_report_newprepaid>(redisClient, redisClientKey, "").Where(expression.Compile()).ToList();
            redisClient.Quit();
            redisClient.Dispose();
            redisClientKey.Quit();
            redisClientKey.Dispose();
            //默认按时间排序
            if (strOrderBy == "add_time desc")
            {
                listReportNewPrepaid = listReportNewPrepaid.OrderByDescending(m => m.AddTime).ToList();
            }
            if (strOrderBy == "money desc")
            {
                listReportNewPrepaid = listReportNewPrepaid.OrderByDescending(m => m.PrepaidMoney).ToList();
            }
            int totalCount = listReportNewPrepaid.Count;
            DataTable totalDataTable = new DataTable();
            DataColumn dataColumn = new DataColumn();
            dataColumn.ColumnName = "totalCount";
            dataColumn.DataType = typeof(Int64);
            totalDataTable.Columns.Add(dataColumn);
            DataRow newDataRow = totalDataTable.NewRow();
            newDataRow["totalCount"] = totalCount;
            totalDataTable.Rows.Add(newDataRow);
            dataSet.Tables.Add(totalDataTable);
            listReportNewPrepaid = listReportNewPrepaid.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
            //2、获取会员Id
            string strInAccountWhereSql = string.Empty;
            string strUserWhereSql = string.Empty;
            Dictionary<int, string> dicUser = new Dictionary<int, string>();
            for (int i = 0; i < listReportNewPrepaid.Count; i++)
            {
                //获取入账表的过滤条件
                strInAccountWhereSql += listReportNewPrepaid[i].InAccountId + ",";
                //获取会员档案的过滤条件
                if (!dicUser.ContainsKey(listReportNewPrepaid[i].user_id))
                {
                    dicUser.Add(listReportNewPrepaid[i].user_id, listReportNewPrepaid[i].group_name);
                    strUserWhereSql += listReportNewPrepaid[i].user_id + ",";
                }
                if (!dicUser.ContainsKey(listReportNewPrepaid[i].agent_id))
                {
                    dicUser.Add(listReportNewPrepaid[i].agent_id, listReportNewPrepaid[i].group_name);
                    strUserWhereSql += listReportNewPrepaid[i].agent_id + ",";
                }
            }
            //3、获取会员档案
            DataTable useDataTable;
            if (!string.IsNullOrWhiteSpace(strUserWhereSql))
            {
                strUserWhereSql = strUserWhereSql.Substring(0, strUserWhereSql.Length - 1);
                strUserWhereSql = "identityid='" + strIdentityId + "' and id in(" + strUserWhereSql + ")";
                useDataTable = DBcon.DbHelperSQL.Query("select id,user_name,reg_time from dt_users where " + strUserWhereSql).Tables[0];
            }
            else
            {
                useDataTable = DBcon.DbHelperSQL.Query("select user_name,reg_time from dt_users where 1=2 ").Tables[0];
            }
            //4、获取入账数据
            if (!string.IsNullOrWhiteSpace(strInAccountWhereSql))
            {
                strInAccountWhereSql = strInAccountWhereSql.Substring(0, strInAccountWhereSql.Length - 1);
                strInAccountWhereSql = "a.id in (" + strInAccountWhereSql + ")";
                strInAccountWhereSql = "a.identityid='" + strIdentityId + "' and " + strInAccountWhereSql;
            }
            else
            {
                strInAccountWhereSql = "1=2";
            }
            DataTable reportNewPrepaidDataTable = DBcon.DbHelperSQL.Query(@"select a.user_id,'' as user_name,''as title,'' as agent_name,b.use_money,a.money,case when a.type=20 then '人工存款' else a.commt end as commt,a.SourceName,a.cztime,'' as RegTime
                                                                             from dt_user_in_account a join dt_user_capital b on a.user_id=b.user_id  where " + strInAccountWhereSql + " order by " + strOrderBy).Tables[0];
            //Log.Info("起始日期：" + strStartDate + ",截止日期:" + strEndDate + ",redis数量：" + listReportNewPrepaid.Count + ",实体库数量:" + reportNewPrepaidDataTable.Rows.Count + ",查询条件:" + strInAccountWhereSql);
            for (var i = 0; i < reportNewPrepaidDataTable.Rows.Count; i++)
            {
                strUserId = reportNewPrepaidDataTable.Rows[i]["user_id"].ToString();
                //会员账号称
                DataRow[] dataRow = useDataTable.Select("id=" + strUserId);
                if (dataRow != null && dataRow.Length > 0)
                {
                    reportNewPrepaidDataTable.Rows[i]["user_name"] = dataRow[0]["user_name"];
                    reportNewPrepaidDataTable.Rows[i]["RegTime"] = dataRow[0]["reg_time"];
                }
                //上级代理名称
                List<dt_report_newprepaid> tempReportNewRepaid = listReportNewPrepaid.Where(m => m.user_id == int.Parse(strUserId)).ToList();

                if (tempReportNewRepaid != null && tempReportNewRepaid.Count > 0)
                {
                    strAgentId = tempReportNewRepaid[0].agent_id.ToString();
                    DataRow[] agentDataRow = useDataTable.Select("id=" + strAgentId);
                    if (agentDataRow != null && agentDataRow.Length > 0)
                    {
                        reportNewPrepaidDataTable.Rows[i]["agent_name"] = agentDataRow[0]["user_name"];
                    }
                    //等级名称
                    reportNewPrepaidDataTable.Rows[i]["title"] = tempReportNewRepaid[0].group_name;
                }
            }
            dataSet.Tables.Add(reportNewPrepaidDataTable.Copy());
            return dataSet;
        }
        catch (Exception ex)
        {
            Log.Error(ex);
            return null;
        }
    }
    /// <summary>
    /// 获取终端报表数据
    /// </summary>
    /// <param name="strIdentityId">站长ID</param>
    /// <param name="strStartDate">起始日期</param>
    /// <param name="strEndDate">截止日期</param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public DataSet GetReportSourceData(string strIdentityId, string strStartDate, string strEndDate, int pageIndex, int pageSize)
    {
        DataSet dataSet = new DataSet();
        try
        {
            //验证是否有权访问  
            string strMsg = ValidateAuthority();
            if (!string.IsNullOrWhiteSpace(strMsg))
            {
                return dataSet;
            }
            //1、获取首充记录表数据
            var redisClient = GlobalVariable.redisClient;
            var redisClientKey = GlobalVariable.redisClientKey;
            //List<dt_report_inoutaccount> listReportInOutAccount = new List<dt_report_inoutaccount>();
            ////出入账日报统计表
            //listReportInOutAccount = redisClient.GetAll<dt_report_inoutaccount>().Where(m => m.identityid == strIdentityId &&
            //                          int.Parse(m.AddTime.ToString("yyyyMMdd")) >= int.Parse(DateTime.Parse(strStartDate).ToString("yyyyMMdd")) &&
            //                          int.Parse(m.AddTime.ToString("yyyyMMdd")) <= int.Parse(DateTime.Parse(strEndDate).ToString("yyyyMMdd"))).ToList();
            DateTime dtStartTime = DateTime.Parse(strStartDate);
            DateTime dtEndTime = DateTime.Parse(strEndDate);
            List<dt_report_inoutaccount> listReportInOutAccount = redisHelper.Search<dt_report_inoutaccount>(redisClient, redisClientKey, dtStartTime, dtEndTime,strIdentityId).ToList();
            List<ReportSource> listReportSource = new List<ReportSource>();
            int totalCount = listReportInOutAccount.Count;
            //保存记录总数量的表
            DataTable totalDataTable = new DataTable();
            DataColumn dataColumn = new DataColumn();
            dataColumn.ColumnName = "totalCount";
            dataColumn.DataType = typeof(Int64);
            totalDataTable.Columns.Add(dataColumn);
            DataTableHelper dataTableHelper = new DataTableHelper();
            DataTable dataTable = null;
            DataRow newDataRow = totalDataTable.NewRow();
            newDataRow["totalCount"] = totalCount;
            totalDataTable.Rows.Add(newDataRow);
            dataSet.Tables.Add(totalDataTable);
            if (listReportInOutAccount == null || listReportInOutAccount.Count < 1)
            {
                dataTable = dataTableHelper.ConvertListToDataTable(listReportInOutAccount);
                dataSet.Tables.Add(dataTable);
                return dataSet;
            }
            //次数日报统计表
            //List<dt_report_num> listReportNum = redisClient.GetAll<dt_report_num>().Where(m => m.identityid == strIdentityId &&
            //                            int.Parse(m.AddTime.ToString("yyyyMMdd")) >= int.Parse(DateTime.Parse(strStartDate).ToString("yyyyMMdd")) &&
            //                            int.Parse(m.AddTime.ToString("yyyyMMdd")) <= int.Parse(DateTime.Parse(strEndDate).ToString("yyyyMMdd"))).ToList();
            List<dt_report_num> listReportNum = redisHelper.Search<dt_report_num>(redisClient, redisClientKey, dtStartTime, dtEndTime, strIdentityId).ToList();
            //首充日报统计表
            //List<dt_report_newprepaid> listReportNewPrepaid = redisClient.GetAll<dt_report_newprepaid>().Where(m => m.identityid == strIdentityId &&
            //                            int.Parse(m.AddTime.ToString("yyyyMMdd")) >= int.Parse(DateTime.Parse(strStartDate).ToString("yyyyMMdd")) &&
            //                            int.Parse(m.AddTime.ToString("yyyyMMdd")) <= int.Parse(DateTime.Parse(strEndDate).ToString("yyyyMMdd"))).ToList();
            List<dt_report_newprepaid> listReportNewPrepaid = redisHelper.Search<dt_report_newprepaid>(redisClient, redisClientKey, dtStartTime,dtEndTime,strIdentityId).ToList();
            redisClient.Quit();
            redisClient.Dispose();
            redisClientKey.Quit();
            redisClientKey.Dispose();
            //注册记录表
            DataTable registerDataTable = DBcon.DbHelperSQL.Query("select UserId,SourceName from dt_users_register where identityid='" + strIdentityId + "' and AddTime between '" + strStartDate + "' and '" + strEndDate + "'").Tables[0];
            Dictionary<string, ReportSource> dicReportSource = new Dictionary<string, ReportSource>();

            //用来校验统计投注人数
            Dictionary<string, string> dicBetUserGroup = new Dictionary<string, string>();
            //用来校验统计充值人数
            Dictionary<string, string> dicPrepaidUserGroup = new Dictionary<string, string>();
            for (int i = 0; i < listReportInOutAccount.Count; i++)
            {
                //等级名称
                string strSourceName = listReportInOutAccount[i].SourceName;
                ReportSource reportSource = new ReportSource();
                if (!dicReportSource.ContainsKey(strSourceName))
                {
                    dicReportSource.Add(strSourceName, reportSource);
                    listReportSource.Add(reportSource);
                    //首充人数
                    int iNewPrepaidNum = listReportNewPrepaid.Where(m => m.SourceName == strSourceName).GroupBy(m => m.user_id).ToList().Count;
                    reportSource.NewPrepaidNum = iNewPrepaidNum;
                }
                reportSource = dicReportSource[strSourceName];
                //等级
                reportSource.group_name = strSourceName;
                //注册人数
                DataRow[] dataRows = registerDataTable.Select("SourceName='" + strSourceName + "'");
                reportSource.RegNum = dataRows.Length;
                //会员Id
                string strUserId = listReportInOutAccount[i].user_id.ToString();
                string strKey = strSourceName + "_" + strUserId;
                //投注金额
                decimal fBetMoney = listReportInOutAccount[i].Out_BettingAccount;
                if (fBetMoney > 0)
                {
                    if (!dicBetUserGroup.ContainsKey(strKey))
                    {
                        dicBetUserGroup.Add(strKey, strKey);
                        //投注人数
                        reportSource.BetNum++;
                    }

                    //投注金额
                    reportSource.BetMoney += fBetMoney;
                }
                //快捷充值
                decimal fFastPrepaid = listReportInOutAccount[i].In_FastPrepaid;
                //银行转账
                decimal fBankPrepaid = listReportInOutAccount[i].In_BankPrepaid;
                //支付宝充值
                decimal fAlipayPrepaid = listReportInOutAccount[i].In_AlipayPrepaid;
                //微信充值
                decimal fWeChatPrepaid = listReportInOutAccount[i].In_WeChatPrepaid;
                //人工存款
                decimal fArtificialDeposit = listReportInOutAccount[i].In_ArtificialDeposit;
                if (fFastPrepaid > 0 || fBankPrepaid > 0 || fAlipayPrepaid > 0 || fWeChatPrepaid > 0 || fArtificialDeposit > 0)
                {
                    //充值金额
                    reportSource.PrepaidMoney += fFastPrepaid;
                    reportSource.PrepaidMoney += fBankPrepaid;
                    reportSource.PrepaidMoney += fAlipayPrepaid;
                    reportSource.PrepaidMoney += fWeChatPrepaid;
                    reportSource.PrepaidMoney += fArtificialDeposit;
                    if (!dicPrepaidUserGroup.ContainsKey(strKey))
                    {
                        dicPrepaidUserGroup.Add(strKey, strKey);
                        //充值人数
                        reportSource.PrepaidNum++;
                    }
                }
                //提现金额
                reportSource.OutMoney += listReportInOutAccount[i].Out_Account;
                //中奖金额
                reportSource.WinMoney += listReportInOutAccount[i].Out_WinningAccount;
                //返点金额
                reportSource.Out_RebateAccount += listReportInOutAccount[i].Out_RebateAccount;
                //活动礼金
                reportSource.ActivityDiscountAccountMoney += listReportInOutAccount[i].Out_ActivityDiscountAccount;
                reportSource.ActivityDiscountAccountMoney += listReportInOutAccount[i].Out_OtherDiscountAccount;
                //盈利＝投注-中奖-返点-活动-其他优惠-代理工资-代理分红+拒绝总额+行政提出
                reportSource.ProfitLoss += listReportInOutAccount[i].Out_BettingAccount;
                reportSource.ProfitLoss -= listReportInOutAccount[i].Out_WinningAccount;
                reportSource.ProfitLoss -= listReportInOutAccount[i].Out_RebateAccount;
                reportSource.ProfitLoss -= listReportInOutAccount[i].Out_ActivityDiscountAccount;
                reportSource.ProfitLoss -= listReportInOutAccount[i].Out_OtherDiscountAccount;
                reportSource.ProfitLoss -= listReportInOutAccount[i].Wages;
                reportSource.ProfitLoss -= listReportInOutAccount[i].Bonus;
                reportSource.ProfitLoss += listReportInOutAccount[i].Out_RefuseAccount;
                reportSource.ProfitLoss += listReportInOutAccount[i].Out_AdministrationAccount;
                //盈率：盈率=盈利/投注*100
                if (reportSource.BetMoney != 0)
                {
                    reportSource.WinRate = reportSource.ProfitLoss / reportSource.BetMoney * 100;
                }
            }
            for (int i = 0; i < listReportSource.Count; i++)
            {
                string strSourceName = listReportSource[i].group_name;
                listReportSource[i].BetOrderNum = listReportNum.Where(m => m.SourceName == strSourceName).Sum(m => m.Out_BettingAccount_Num);
            }
            totalCount = listReportSource.Count;
            dataSet.Tables[0].Rows[0]["totalCount"] = totalCount;
            listReportSource = listReportSource.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
            dataTable = dataTableHelper.ConvertListToDataTable(listReportSource);
            dataSet.Tables.Add(dataTable);
            return dataSet;
        }
        catch (Exception ex)
        {
            Log.Error(ex);
            return null;
        }
    }
    /// <summary>
    /// 获取终端报表数据
    /// </summary>
    /// <param name="strIdentityId">站长ID</param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public DataSet GetGroupLayerData(string strIdentityId, string strIsFirst, string strUserName, int pageIndex, int pageSize)
    {
        DataSet dataSet = new DataSet();
        try
        {
            //验证是否有权访问  
            string strMsg = ValidateAuthority();
            if (!string.IsNullOrWhiteSpace(strMsg))
            {
                return dataSet;
            }
            //通过历史查询默认列表为空
            int totalCount = 0;
            DataTable totalDataTable = new DataTable();
            DataColumn dataColumn = new DataColumn();
            dataColumn.ColumnName = "totalCount";
            dataColumn.DataType = typeof(Int64);
            totalDataTable.Columns.Add(dataColumn);
            DataRow newDataRow = totalDataTable.NewRow();
            newDataRow["totalCount"] = totalCount;
            totalDataTable.Rows.Add(newDataRow);
            dataSet.Tables.Add(totalDataTable);
            DataTableHelper dataTableHelper = new DataTableHelper();
            List<GroupLayer> listGroupLayer = new List<GroupLayer>();
            DataTable dataTable = null;
            if (strIsFirst == "1")
            {
                dataTable = dataTableHelper.ConvertListToDataTable(listGroupLayer);
                dataSet.Tables.Add(dataTable);
                return dataSet;
            }
            //1、获取会员档案数据
            string[] strUserNames = strUserName.Split(",");
            string strWhereSql = string.Empty;
            Dictionary<string, GroupLayer> dicInOutAccount = new Dictionary<string, GroupLayer>();
            for (int i = 0; i < strUserNames.Length; i++)
            {
                strWhereSql += "'" + strUserNames[i] + "',";
            }
            if (string.IsNullOrWhiteSpace(strWhereSql))
            {
                dataTable = dataTableHelper.ConvertListToDataTable(listGroupLayer);
                dataSet.Tables.Add(dataTable);
                return dataSet;
            }
            strWhereSql = strWhereSql.Substring(0, strWhereSql.Length - 1);
            DataTable userDataTable = DBcon.DbHelperSQL.Query("select id,user_name,group_id,reg_time from dt_users where IdentityId='" + strIdentityId + "' and  user_name in (" + strWhereSql + ")  ").Tables[0];
            if (userDataTable != null && userDataTable.Rows.Count < 1)
            {
                dataTable = dataTableHelper.ConvertListToDataTable(listGroupLayer);
                dataSet.Tables.Add(dataTable);
                return dataSet;
            }
            for (int i = 0; i < strUserNames.Length; i++)
            {
                DataRow[] dataRows = userDataTable.Select("user_name='" + strUserNames[i] + "'");
                if (dataRows != null && dataRows.Length < 1)
                    continue;

                GroupLayer groupLayer = new GroupLayer();
                string strUserId = dataRows[0]["id"].ToString();
                if (dicInOutAccount.ContainsKey(strUserId))
                    continue;

                dicInOutAccount.Add(strUserId, groupLayer);
                listGroupLayer.Add(groupLayer);
                //会员Id
                groupLayer.User_id = int.Parse(strUserId);
                //会员账号称
                object bjUserName = dataRows[0]["user_name"];
                if (bjUserName != null)
                {
                    groupLayer.User_name = bjUserName.ToString();
                }
                //会员注册时间
                object bjRegTime = dataRows[0]["reg_time"];
                if (bjRegTime != null)
                {
                    groupLayer.zctime = DateTime.Parse(bjRegTime.ToString());
                }
                //会员等级
                object bjGroup = dataRows[0]["group_id"];
                if (bjGroup != null && !string.IsNullOrWhiteSpace(bjGroup.ToString()))
                {
                    groupLayer.groups = bjGroup.ToString();
                }
            }
            //2、获取会员盈利报表数据
            //RedisClientConfig redisClientConfig = new RedisClientConfig();
            //redisClientConfig.ServerIP = DBcon.DBcontring.redisServerIP;
            var redisClient = GlobalVariable.redisClient;
            // List<dt_report_inoutaccount> tempInOutAccountList = redisClient.GetAll<dt_report_inoutaccount>().ToList();
            List<dt_report_inoutaccount> tempInOutAccountList = redisHelper.Get<dt_report_inoutaccount>(redisClient).ToList();
            //List<dt_report_num> tempReportNumList = redisClient.GetAll<dt_report_num>().ToList();
            List<dt_report_num> tempReportNumList = redisHelper.Get<dt_report_num>(redisClient).ToList();
            redisClient.Quit();
            int inOutAccountListCount = tempInOutAccountList.Count;
            for (int i = 0; i < inOutAccountListCount; i++)
            {
                string strUserId = tempInOutAccountList[i].user_id.ToString();
                int iUserId = int.Parse(strUserId);
                DataRow[] dataRows = userDataTable.Select("id=" + strUserId);
                if (dataRows != null && dataRows.Length < 1)
                    continue;

                GroupLayer groupLayer = new GroupLayer();
                if (!dicInOutAccount.ContainsKey(strUserId))
                {
                    dicInOutAccount.Add(strUserId, groupLayer);
                    listGroupLayer.Add(groupLayer);
                }
                groupLayer = dicInOutAccount[strUserId];
                //存款总额
                groupLayer.totalruk += tempInOutAccountList[i].In_AlipayPrepaid;
                groupLayer.totalruk += tempInOutAccountList[i].In_ArtificialDeposit;
                groupLayer.totalruk += tempInOutAccountList[i].In_BankPrepaid;
                groupLayer.totalruk += tempInOutAccountList[i].In_FastPrepaid;
                groupLayer.totalruk += tempInOutAccountList[i].In_WeChatPrepaid;
                //取款总额
                groupLayer.totalchuk += tempInOutAccountList[i].Out_Account;
                //投注总额
                groupLayer.totalplay += tempInOutAccountList[i].Out_BettingAccount;
                //中奖总额
                groupLayer.totalwin += tempInOutAccountList[i].Out_WinningAccount;
                //返点总额
                groupLayer.totalfd += tempInOutAccountList[i].Out_RebateAccount;
                //优惠总额
                groupLayer.totalyh += tempInOutAccountList[i].Out_ActivityDiscountAccount;
                //groupLayer.totalyh += tempInOutAccountList[i].Out_DiscountAccount;
                //盈利总额=中奖-投注+返点+活动+其他优惠+代理工资+代理分红-拒绝总额-行政提出
                groupLayer.Deficit += tempInOutAccountList[i].Out_WinningAccount;
                groupLayer.Deficit -= tempInOutAccountList[i].Out_BettingAccount;
                groupLayer.Deficit += tempInOutAccountList[i].Out_RebateAccount;
                groupLayer.Deficit += tempInOutAccountList[i].Out_ActivityDiscountAccount;
                groupLayer.Deficit += tempInOutAccountList[i].Out_OtherDiscountAccount;
                groupLayer.Deficit += tempInOutAccountList[i].Wages;
                groupLayer.Deficit += tempInOutAccountList[i].Bonus;
                groupLayer.Deficit -= tempInOutAccountList[i].Out_RefuseAccount;
                groupLayer.Deficit -= tempInOutAccountList[i].Out_AdministrationAccount;
            }
            for (int i = 0; i < listGroupLayer.Count; i++)
            {
                int iUserId = listGroupLayer[i].User_id;
                //入款次数＝快捷充值次数+银行充值次数+支付宝充值次数+微信充值次数+人工存款次数
                listGroupLayer[i].ruknum += tempReportNumList.Where(m => m.user_id == iUserId).Sum(m => m.In_AlipayPrepaid_Num);
                listGroupLayer[i].ruknum += tempReportNumList.Where(m => m.user_id == iUserId).Sum(m => m.In_ArtificialDeposit_Num);
                listGroupLayer[i].ruknum += tempReportNumList.Where(m => m.user_id == iUserId).Sum(m => m.In_BankPrepaid_Num);
                listGroupLayer[i].ruknum += tempReportNumList.Where(m => m.user_id == iUserId).Sum(m => m.In_FastPrepaid_Num);
                listGroupLayer[i].ruknum += tempReportNumList.Where(m => m.user_id == iUserId).Sum(m => m.In_WeChatPrepaid_Num);
                //出款次数
                listGroupLayer[i].chuknum += tempReportNumList.Where(m => m.user_id == iUserId).Sum(m => m.Out_Account_Num);
            }
            dataSet.Tables[0].Rows[0]["totalCount"] = listGroupLayer.Count;
            dataTable = dataTableHelper.ConvertListToDataTable(listGroupLayer);
            dataSet.Tables.Add(dataTable);
            return dataSet;
        }
        catch (Exception ex)
        {
            Log.Error(ex);
            return null;
        }
    }
    /// <summary>
    /// 获取会员取款盈利
    /// </summary>
    /// <param name="UserId"></param>
    /// <param name="IdentityId"></param>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public string GetUserProfit(int UserId, string strIdentityId, string strOrderTime)
    {
        decimal ProfitLoss = 0m;
        string ProfitRebate = "0.00%";
        string strResult = string.Empty;
        try
        {
            var redisClient = GlobalVariable.redisClient;
            var redisClientKey = GlobalVariable.redisClientKey;
            DateTime dtStartTime=Convert.ToDateTime(strOrderTime + " 00:00:00");
            DateTime dtEndTime=Convert.ToDateTime(strOrderTime + " 23:59:59");
            var inOutAccountList = redisHelper.Search<dt_report_inoutaccount>(redisClient, redisClientKey, dtStartTime, dtEndTime, UserId.ToString(), "").Where(m => m.identityid == strIdentityId).ToList();
            decimal BetMoney = 0m;
            decimal WinMoney = 0m;
            decimal RebateMoney = 0m;
            decimal DiscountMoney = 0m;
            for (int i = 0; i < inOutAccountList.Count; i++)
            {
                //投注总额
                BetMoney += inOutAccountList[i].Out_BettingAccount;
                //中奖总额
                WinMoney += inOutAccountList[i].Out_WinningAccount;
                //返点总额
                RebateMoney += inOutAccountList[i].Out_RebateAccount;
                //优惠总额
                DiscountMoney += inOutAccountList[i].Out_ActivityDiscountAccount;
                DiscountMoney += inOutAccountList[i].Out_OtherDiscountAccount;
                //盈利总额=中奖-投注+返点+活动+其他优惠+代理工资+代理分红-拒绝总额-行政提出
                ProfitLoss += inOutAccountList[i].Out_WinningAccount;
                ProfitLoss -= inOutAccountList[i].Out_BettingAccount;
                ProfitLoss += inOutAccountList[i].Out_RebateAccount;
                ProfitLoss += inOutAccountList[i].Out_ActivityDiscountAccount;
                ProfitLoss += inOutAccountList[i].Out_OtherDiscountAccount;
                ProfitLoss += inOutAccountList[i].Wages;
                ProfitLoss += inOutAccountList[i].Bonus;
                ProfitLoss -= inOutAccountList[i].Out_RefuseAccount;
                ProfitLoss -= inOutAccountList[i].Out_AdministrationAccount;
            }
            //盈率
            decimal fProfitRate = 0;
            if (BetMoney != 0)
            {
                fProfitRate = ProfitLoss / BetMoney * 100;
            }
            ProfitRebate = fProfitRate.ToString("0.00") + "%";
            redisClient.Quit();
            redisClient.Dispose();
            redisClientKey.Quit();
            redisClientKey.Dispose();
            strResult = ProfitLoss.ToString("#0.00") + "_" + ProfitRebate;
            return strResult;
        }
        catch (Exception ex)
        {
            strResult="0.00_0.00%";
            EasyFrame.NewCommon.Log.Error(ex);
        }
        return strResult;
    }
}
