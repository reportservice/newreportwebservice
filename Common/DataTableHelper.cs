﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Reflection;
using EasyFrame.Model;
using System.Collections;
using EasyFrame.NewCommon.Common;
/**************************************************************************

作者: zengzh

日期:2016-07-26

说明:DataTable帮助类

**************************************************************************/
/// <summary>
/// DataTable帮助类
/// </summary>
public class DataTableHelper
{
    /// <summary>
    /// 枚举转换成DataTable
    /// </summary>
    /// <param name="enumName">枚举对象</param>
    ///枚举格式如下：
    /// [Description("投注递减,betting_money desc")]
    /// betting_money_desc = 1,
    /// <returns></returns>
    public DataTable ConvertEnumToDataTable(object enumName)
    {
        DataTable dataTable = new DataTable();
        //Title列
        DataColumn dataColumn = new DataColumn();
        dataColumn.ColumnName = "Title";
        dataTable.Columns.Add(dataColumn);
        //Value列
        dataColumn = new DataColumn();
        dataColumn.ColumnName = "Value";
        dataTable.Columns.Add(dataColumn);
        //获取枚举所有属性
        foreach (var propertyName in Enum.GetNames(enumName.GetType()))
        {
            //获取枚举的Description属性
            string strDescription = EnumHelp.GetEnumDesc(enumName, propertyName);
            int index = strDescription.IndexOf(",");
            if (index == -1)
                continue;

            DataRow dataRow = dataTable.NewRow();
            string strTitle = strDescription.Substring(0, index);
            dataRow["Title"] = strTitle;
            string strValue = strDescription.Substring(index + 1);
            dataRow["Value"] = strValue;
            dataTable.Rows.Add(dataRow);
        }
        return dataTable;
    }
    /// <summary>
    /// 实体类转换成DataTable
    /// </summary>
    /// <param name="model">实体类对象</param>
    /// <returns></returns>
    public DataTable ConvertModelToDataTable(object model)
    {
        DataTable dataTable = new DataTable();
        //获取实体类的公共属性
        PropertyInfo[] propertyInfos = model.GetType().GetProperties();
        foreach (PropertyInfo propertyInfo in propertyInfos)
        {
            DataColumn dataColumn = new DataColumn();
            dataColumn.ColumnName = propertyInfo.Name;
            if (propertyInfo.PropertyType.Name == "String")
            {
                dataColumn.DataType = typeof(String);
            }
            if (propertyInfo.PropertyType.Name == "Double")
            {
                dataColumn.DataType = typeof(Double);
            }
            dataTable.Columns.Add(dataColumn);
        }
        return dataTable;
    }
    /// <summary>
    /// DataRow转换成实体类
    /// </summary>
    /// <param name="dataRow">DataRow</param>
    /// <param name="Model">实体类</param>
    /// <returns></returns>
    public void ConvertDataRowToModel(DataRow dataRow, object Model)
    {
        Dictionary<string, PropertyInfo> dicPropertyInfo = new Dictionary<string, PropertyInfo>();
        PropertyInfo[] propertyInfos = Model.GetType().GetProperties();
        foreach (PropertyInfo propertyInfo in propertyInfos)
        {
            if (!dicPropertyInfo.ContainsKey(propertyInfo.Name))
            {
                dicPropertyInfo.Add(propertyInfo.Name, propertyInfo);
            }
        }
        for (int j = 0; j < dataRow.Table.Columns.Count; j++)
        {
            string strColumnName = dataRow.Table.Columns[j].ColumnName;
            if (!dicPropertyInfo.ContainsKey(strColumnName))
                continue;

            object bjValue = dataRow[strColumnName];
            if (bjValue == null || bjValue is DBNull)
                continue;

            dicPropertyInfo[strColumnName].SetValue(Model, bjValue, null);
        }
    }
    /// <summary>
    /// 获取分页数据
    /// </summary>
    /// <param name="PageCurrent">当前页号</param>
    /// <param name="PageSize">每页多少条记录</param>
    /// <param name="FieldOrder">排序字段</param>
    /// <param name="Sql">SQL语句</param>
    /// <param name="totalRows">总数量</param>
    /// <returns></returns>
    public DataTable GetPaginationData(int PageCurrent, int PageSize, string FieldOrder, string Sql, out int totalRows)
    {
        totalRows = 0;
        //分页
        if (PageCurrent <= 0 && PageSize <= 0)
            return null;

        //起始行号
        int startNums = (PageSize * (PageCurrent - 1)) + 1;
        //结束行号
        int endNums = (PageSize * PageCurrent);
        string tmpSql = "with t_rowtable as (Select *,row_number() over( order by " + FieldOrder + ") as row_number from (" + Sql + ") aaa) Select count(*) from t_rowtable ";
        DataTable dataTable = DBcon.DbHelperSQL.Query(tmpSql).Tables[0];

        string strSql = "with t_rowtable as (Select *,row_number() over( order by " + FieldOrder + ") as row_number from (" + Sql + ") aaa) Select * from t_rowtable  ";
        totalRows = int.Parse(dataTable.Rows[0][0].ToString());
        strSql += "where row_number between " + startNums.ToString() + " and " + endNums.ToString();
        return DBcon.DbHelperSQL.Query(strSql).Tables[0];
    }
    /// <summary>
    /// DataTable转换成字典
    /// </summary>
    /// <param name="dicData">字典数据</param>
    /// <param name="dataTable">DataTable数据</param>
    /// <param name="fieldName">字段</param>
    public void ConvertDataTableToDictionary(Dictionary<string, DataTable> dicData, DataTable dataTable, string fieldName)
    {
        int count = dataTable.Rows.Count;
        for (int i = 0; i < count; i++)
        {
            string strKey = dataTable.Rows[i][fieldName].ToString();
            DataRow[] dataRows = { dataTable.Rows[i] };
            if (dicData.ContainsKey(strKey))
            {
                DataTable data = dicData[strKey];
                data.Merge(dataRows.CopyToDataTable());
            }
            else
            {
                dicData.Add(strKey, dataRows.CopyToDataTable());
            }
        }
    }
    /// <summary>
    /// DataTable转换成字典
    /// </summary>
    /// <param name="dicData">字典数据</param>
    /// <param name="dataTable">DataTable数据</param>
    /// <param name="keyFieldName">作为Key的字段名</param>
    /// <param name="valueFieldName">作为Value的字段名</param>
    public void ConvertDataTableToDictionary(Dictionary<string, string> dicData, DataTable dataTable, string keyFieldName, string valueFieldName)
    {
        int count = dataTable.Rows.Count;
        for (int i = 0; i < count; i++)
        {
            string strKey = dataTable.Rows[i][keyFieldName].ToString();
            string strValue = dataTable.Rows[i][valueFieldName].ToString();
            if (dicData.ContainsKey(strKey))
                continue;

            dicData.Add(strKey, strValue);
        }
    }
    /// <summary>
    /// DataTable转换成字典
    /// </summary>
    /// <param name="listData">字典数据</param>
    /// <param name="dataTable">DataTable数据</param>
    /// <param name="keyFieldName">作为Key的字段名</param>
    /// <param name="valueFieldName">作为Value的字段名</param>
    public void ConvertDataTableDictionary(Dictionary<string, string> dicData, DataTable dataTable, string keyFieldName, string valueFieldName)
    {
        int count = dataTable.Rows.Count;
        for (int i = 0; i < count; i++)
        {
            string strKey = dataTable.Rows[i][keyFieldName].ToString();
            string strValue = dataTable.Rows[i][valueFieldName].ToString();
            if (!dicData.ContainsKey(strKey))
            {
                dicData.Add(strKey, strValue);
            }
            if (!dicData.ContainsKey(strValue))
            {
                dicData.Add(strValue, strValue);
            }
        }
    }
    /// <summary>
    /// 将泛型集合类转换成DataTable
    /// </summary>
    /// <typeparam name="T">集合项类型</typeparam>
    /// <param name="list">集合</param>
    /// <param name="propertyName">需要返回的列的列名</param>
    /// <returns>数据集(表)</returns>
    public DataTable ConvertListToDataTable<T>(IList<T> list, params string[] propertyName)
    {
        List<string> propertyNameList = new List<string>();
        if (propertyName != null)
            propertyNameList.AddRange(propertyName);

        DataTable result = new DataTable();

        PropertyInfo[] propertys = list.GetType().GetGenericArguments()[0].GetProperties();
        foreach (PropertyInfo pi in propertys)
        {
            Type type = pi.PropertyType;

            if (propertyNameList.Count == 0)
            {
                result.Columns.Add(pi.Name, type);
            }
            else
            {
                if (propertyNameList.Contains(pi.Name))
                    result.Columns.Add(pi.Name, type);
            }
        }
        for (int i = 0; i < list.Count; i++)
        {
            ArrayList tempList = new ArrayList();
            foreach (PropertyInfo pi in propertys)
            {
                if (pi.PropertyType.Name.ToLower() == "object")
                    continue;

                if (propertyNameList.Count == 0)
                {
                    object obj = pi.GetValue(list[i], null);
                    tempList.Add(obj);
                }
                else
                {
                    if (propertyNameList.Contains(pi.Name))
                    {
                        object obj = pi.GetValue(list[i], null);
                        tempList.Add(obj);
                    }
                }
            }
            object[] array = tempList.ToArray();
            result.LoadDataRow(array, true);
        }
        return result;
    }
    /// <summary>
    /// 将泛型集合类转换成DataTable
    /// </summary>
    /// <typeparam name="T">集合项类型</typeparam>
    /// <param name="list">集合</param>
    /// <returns>数据集(表)</returns>
    public DataTable ConvertListToDataTable<T>(IList<T> list)
    {
        return ConvertListToDataTable<T>(list, null);
    }
    /// <summary>
    /// DataTable分页
    /// </summary>
    /// <param name="dataTable">记录集 DataTable</param>
    /// <param name="PageIndex">当前页</param>
    /// <param name="pagesize">一页的记录数</param>
    /// <returns></returns>
    public DataTable SetDataTablePagination(DataTable dataTable, int PageIndex, int PageSize)
    {
        if (PageIndex == 0)
        {
            return dataTable;
        }
        //复制一份新的记录集
        DataTable newDataTable = dataTable.Clone();
        int rowBegin = (PageIndex - 1) * PageSize;
        int rowEnd = PageIndex * PageSize;
        if (rowBegin >= dataTable.Rows.Count)
        {
            return newDataTable;
        }

        if (rowEnd > dataTable.Rows.Count)
        {
            rowEnd = dataTable.Rows.Count;
        }
        for (int i = rowBegin; i <= rowEnd - 1; i++)
        {
            DataRow newDataRow = newDataTable.NewRow();
            DataRow dataRow = dataTable.Rows[i];
            foreach (DataColumn column in dataTable.Columns)
            {
                newDataRow[column.ColumnName] = dataRow[column.ColumnName];
            }
            newDataTable.Rows.Add(newDataRow);
        }

        return newDataTable;
    }
}