﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;


public class SqlHelper
{
    //主庫
    public static string localconnnStr = DBcon.DBcontring.connectionString_Write;
    //分庫                                     
    public static string localconnnStr2 = DBcon.DBcontring.connectionString_switch;

    /// <summary>
    /// 用来查詢數據
    /// </summary>
    /// <param name="strsql"></param>
    /// <returns></returns>
    public static DataTable GetQueryFromLocalData(string strsql, int DBtype, SqlParameter[] cmdParms)
    {
        string connnStr = "";
        switch (DBtype)
        {
            case 1:
                connnStr = localconnnStr;
                break;
            case 2:
                connnStr = localconnnStr2;
                break;
        }

        using (SqlConnection localconn = new SqlConnection(connnStr))
        {
            DataSet ds1 = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand();
                PrepareCommand(cmd, localconn, null, strsql, cmdParms);
                using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                {
                    sda.Fill(ds1);
                    cmd.Parameters.Clear();
                }

            }
            catch (Exception e)
            {

            }
            finally
            {
                if (localconn.State == ConnectionState.Open)
                {
                    localconn.Dispose();
                    localconn.Close();
                }
            }
            if (ds1.Tables.Count > 0)
            {
                return ds1.Tables[0];
            }
            else
            {
                return null;
            }

        }

    }
    /// <summary>
    /// 切換DBtype
    /// </summary>
    /// <returns></returns>
    public static int SwitchDBtype(string identityid)
    {
        int DBtype = 0;
        identityid = identityid.ToLower();
        if(Flash_Dic.dic_tenant.ContainsKey(identityid))
            DBtype = Flash_Dic.dic_tenant[identityid];
        else
        {
            EasyFrame.Web.Global.AddTenant();
            DBtype = Flash_Dic.dic_tenant[identityid];
        }
        return DBtype;
    }

    /// <summary>
    /// 分庫通時執行
    /// </summary>
    /// <param name="strsql"></param>
    /// <returns></returns>
    public static int QueryFromLocalSwitch(string strsql, SqlParameter[] cmdParms)
    {
        int row = 0;
        using (SqlConnection localconn = new SqlConnection(localconnnStr))
        {
            DataSet ds1 = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand();
                PrepareCommand(cmd, localconn, null, strsql, cmdParms);
                row = cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();


            }
            catch
            {

            }
            finally
            {
                if (localconn.State == ConnectionState.Open)
                {
                    localconn.Dispose();
                    localconn.Close();
                }
            }


        }
        return row;

    }
    /// <summary>
    /// 並行分庫主庫執行語句
    /// </summary>
    /// <param name="strsql"></param>
    /// <returns></returns>
    public static int QueryFromSwitch(string strsql, SqlParameter[] cmdParms)
    {
        int row = 0;
        int diff_tenant = Flash_Dic.dic_tenant.Values.Distinct().Count();
        //for (int i = 0; i < diff_tenant; i++)
        foreach (var dic in Flash_Dic.dic_tenant.Values.Distinct())
        {
            string connnStr = "";
            switch (dic)
            {
                case 1:
                    connnStr = localconnnStr;
                    //connnStr = localconnnStr2;
                    break;
                case 2:
                    connnStr = localconnnStr2;
                    break;
            }


            using (SqlConnection conn = new SqlConnection(connnStr))
            {
                DataSet ds1 = new DataSet();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    PrepareCommand(cmd, conn, null, strsql, cmdParms);
                    row = cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();

                }
                catch (Exception e)
                {

                }
                finally
                {
                    if (conn.State == ConnectionState.Open)
                    {
                        conn.Dispose();
                        conn.Close();
                    }
                }


            }
        }

        return row;

    }
    /// <summary>
    /// 用来執行命令
    /// </summary>
    /// <param name="strsql"></param>
    /// <returns></returns>
    public static int QueryFromLocal(string strsql, int DBtype, SqlParameter[] cmdParms)
    {
        int row = 0;
        string connStr = "";
        switch (DBtype)
        {
            case 1:
                connStr = localconnnStr;
                break;
            case 2:
                connStr = localconnnStr2;
                break;
        }
        using (SqlConnection localconn = new SqlConnection(connStr))
        {
            DataSet ds1 = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand();
                PrepareCommand(cmd, localconn, null, strsql, cmdParms);
                row = cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();


            }
            catch (Exception e)
            {

            }
            finally
            {
                if (localconn.State == ConnectionState.Open)
                {
                    localconn.Dispose();
                    localconn.Close();
                }
            }


        }
        return row;

    }
    /// <summary>
    /// 分庫執行預存
    /// </summary>
    /// <param name="DBtype"></param>
    /// <param name="record_code"></param>
    /// <param name="user_id"></param>
    /// <param name="identityid"></param>
    /// <param name="ProName"></param>
    /// <returns></returns>
    public static int RunInsert(int DBtype, string record_code, string user_id, string identityid, string ProName)
    {
        string connStr = "";
        switch (DBtype)
        {
            case 1:
                connStr = localconnnStr;
                break;
            case 2:
                connStr = localconnnStr2;
                break;
        }

        SqlConnection conn = new SqlConnection(connStr);
        int resulte = 0;
        try
        {
            //dt.Columns.Remove("lockid");
            conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = ProName;
            cmd.CommandTimeout = 500000;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@record_code", SqlDbType.VarChar);
            cmd.Parameters.Add("@user_id", SqlDbType.Int);
            cmd.Parameters.Add("@ruczname", SqlDbType.VarChar);
            cmd.Parameters.Add("@czname", SqlDbType.VarChar);
            cmd.Parameters.Add("@cztime", SqlDbType.DateTime);
            cmd.Parameters.Add("@result", SqlDbType.Int);
            cmd.Parameters.Add("@identityid", SqlDbType.VarChar);
            cmd.Parameters[0].Value = record_code;
            cmd.Parameters[1].Value = user_id;
            cmd.Parameters[2].Value = "0";
            cmd.Parameters[3].Value = "0";
            cmd.Parameters[4].Value = "2016-01-01";
            cmd.Parameters[5].Value = resulte;
            cmd.Parameters[6].Value = identityid;
            cmd.Parameters["@result"].Direction = ParameterDirection.ReturnValue;
            cmd.ExecuteNonQuery();
            resulte = (int)cmd.Parameters["@result"].Value;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (conn.State == ConnectionState.Open)
            {
                conn.Dispose();
                conn.Close();
            }
        }
        return resulte;
    }

    private static void PrepareCommand(SqlCommand cmd, SqlConnection conn, SqlTransaction trans, string cmdText, SqlParameter[] cmdParms)
    {
        if (conn.State != ConnectionState.Open)
            conn.Open();
        cmd.Connection = conn;
        cmd.CommandText = cmdText;
        if (trans != null)
            cmd.Transaction = trans;
        cmd.CommandType = CommandType.Text;//cmdType;
        if (cmdParms != null)
        {
            foreach (SqlParameter parm in cmdParms)
                cmd.Parameters.Add(parm);
        }
    }

}
