﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
/// <summary>
/// 获取中奖总额接口实体类
/// </summary>
public class AwardMoneyData
{
    /// <summary>
    /// 中奖总额
    /// </summary>
    public decimal AwardMoney { get; set; }
}