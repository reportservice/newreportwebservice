﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// 月报表
/// </summary>
public class dt_month_record
{
    public decimal ytzmoney { get; set; }//月投注额
    public decimal ytznumber { get; set; }//月投人數
    public decimal yzjmoney { get; set; }//月中奖额
    public decimal yzjnumber { get; set; }//月中人數
    public decimal bysymoney { get; set; }//本月损益
    public decimal sysymoney { get; set; }//上月损益
    public decimal byylmoney { get; set; }//本月盈利
    public decimal byylrate { get; set; }//本月赢率
    public decimal syylmoney { get; set; }//上月盈利
    public decimal syylrate { get; set; }//上月赢率
    public decimal dlylmoney { get; set; }//当日盈利
    public decimal dlylrate { get; set; }//当日盈率
    public decimal ptccmoney { get; set; }//平台抽成
}