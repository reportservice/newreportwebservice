﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
///代理报表
/// </summary>
public class dt_users
{
    public virtual int transfer_superior { get; set; }
   
    public virtual string telphone { get; set; }
  
    public virtual string address { get; set; }
   
    public virtual DateTime reg_time { get; set; }
  
    public virtual string reg_ip { get; set; }
   
    public virtual DateTime last_login_time { get; set; }
   
    public virtual int login_num { get; set; }
   
    public virtual int transfer_lower { get; set; }
   
    public virtual string login_IP { get; set; }
   
    public virtual decimal use_money { get; set; }
   
    public virtual decimal lock_money { get; set; }
   
    public virtual int point { get; set; }
   
    public virtual int exp { get; set; }
    
    public virtual int open_account_limit { get; set; }
   
    public virtual int lock_bankcord { get; set; }
    
    public virtual DateTime birthday { get; set; }
    
    public virtual int with_draw_count { get; set; }
   
    public virtual string sex { get; set; }
   
    public virtual string PlayLottery { get; set; }
   
    public virtual int id { get; set; }
   
    public virtual string identityid { get; set; }
   
    public virtual int group_id { get; set; }
   
    public virtual int agent_id { get; set; }
    
    public virtual bool is_agent { get; set; }
   
    public virtual string nick_name { get; set; }
    
    public virtual string user_name { get; set; }
    
    public virtual string verification_info { get; set; }
    
    public virtual string password { get; set; }
    
    public virtual string salt { get; set; }
   
    public virtual string avatar { get; set; }
    
    public virtual string mobile { get; set; }
    
    public virtual string email { get; set; }
   
    public virtual string qq { get; set; }
    
    public virtual int status { get; set; }
   
    public virtual int flog { get; set; }
    
    public virtual string safety_password { get; set; }
}