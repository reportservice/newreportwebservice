﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
///代理报表
/// </summary>
public class dt_user_out_sum
{
    public decimal txmony { get; set; }//提现金额
    public int txnumber { get; set; } //提现人数
    public decimal rgtcmoney { get; set; }//人工提出
    public int rgtcnumber { get; set; } //人工提出人数
    public decimal wctcmony { get; set; }//误存提出
    public int wctcnumber { get; set; } //误存提出人数
    public decimal xztcmony { get; set; }//行政提出
    public int xztcnumber { get; set; } //行政提出人数
    public decimal jjmony { get; set; }//拒绝金额
    public int jjnumber { get; set; } //拒绝金额人数
    public decimal opearttime { get; set; } //提现操作
    public int txcount { get; set; } //提现笔数
}