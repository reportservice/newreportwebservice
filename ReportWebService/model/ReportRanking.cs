﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
/// <summary>
/// 今日排行实体类
/// </summary>
public class ReportRanking
{
    /// <summary>
    /// 呢称
    /// </summary>
    public string NickName { get; set; }
    /// <summary>
    /// 金额
    /// </summary>
    public decimal RechargeMoney { get; set; }
    /// <summary>
    /// 用户头像
    /// </summary>
    public string UserPhoto { get; set; }
}