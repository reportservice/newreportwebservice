﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Reflection;
//using EasyFrame.Model;
using System.Collections;
using EasyFrame.NewCommon.Common;
using EasyFrame.NewCommon.Model;
using System.Data.SqlClient;
/**************************************************************************

作者: zengzh

日期:2016-07-26

说明:DataTable帮助类

**************************************************************************/
/// <summary>
/// DataTable帮助类
/// </summary>
public class ReportHelper
{
    public static string Report_Share = DBcon.DBcontring.Report_Share;
    public static string Report_In_Conn = DBcon.DBcontring.Report_In_Conn;
    public static string Report_Out_Conn = DBcon.DBcontring.Report_Out_Conn;
    public static string Report_Order_Conn = DBcon.DBcontring.Report_Order_Conn;
    public static string Agent_In_Conn = DBcon.DBcontring.Agent_In_Conn;
    public static string Agent_Out_Conn = DBcon.DBcontring.Agent_Out_Conn;
    public static string Agent_Order_Conn = DBcon.DBcontring.Agent_Order_Conn;
    public static string congkucon = DBcon.DBcontring.connectionString;
    /// <summary>
    /// 枚举转换成DataTable
    /// </summary>
    /// <param name="enumName">枚举对象</param>
    ///枚举格式如下：
    /// [Description("投注递减,betting_money desc")]
    /// betting_money_desc = 1,
    /// <returns></returns>
    public object ConvertToMode(DataTable table, object Model)
    {
        try
        {

            Dictionary<string, PropertyInfo> dicPropertyInfo = new Dictionary<string, PropertyInfo>();
            PropertyInfo[] propertyInfos = Model.GetType().GetProperties();
            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                if (!dicPropertyInfo.ContainsKey(propertyInfo.Name))
                {
                    dicPropertyInfo.Add(propertyInfo.Name, propertyInfo);
                }
            }
            if (table.Rows.Count > 0)
            {
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    int count = table.Columns.Count;
                    for (int j = 0; j < table.Columns.Count; j++)
                    {
                
                        string strColumnName = table.Columns[j].ColumnName;
                        if (!dicPropertyInfo.ContainsKey(strColumnName))
                            continue;

                        object bjValue = table.Rows[i][strColumnName];
                        if (bjValue == null || bjValue is DBNull || table.Rows[i][strColumnName] == null)
                            continue;

                        if (dicPropertyInfo[strColumnName].PropertyType.Name.ToLower() == "decimal")
                        {
                            object bjValue2 = Convert.ToDecimal(table.Rows[i][strColumnName]);
                            dicPropertyInfo[strColumnName].SetValue(Model, bjValue2, null);
                        }
                        else if (dicPropertyInfo[strColumnName].PropertyType.Name.ToLower() == "datetime")
                        {

                            DateTime bjValue2 = Convert.ToDateTime(table.Rows[i][strColumnName]);
                            dicPropertyInfo[strColumnName].SetValue(Model, bjValue2, null);
                        }
                        else if (dicPropertyInfo[strColumnName].PropertyType.Name.ToLower() == "double")
                        {
                            double bjValue2 = Convert.ToDouble(table.Rows[i][strColumnName]);
                            dicPropertyInfo[strColumnName].SetValue(Model, bjValue2, null);
                        }
                        else if (dicPropertyInfo[strColumnName].PropertyType.Name.ToLower() == "int32")
                        {
                            int bjValue2 = Convert.ToInt32(table.Rows[i][strColumnName]);
                            dicPropertyInfo[strColumnName].SetValue(Model, bjValue2, null);
                        }
                        else
                        {
                            object bjValue2 = table.Rows[i][strColumnName];
                            dicPropertyInfo[strColumnName].SetValue(Model, bjValue2, null);
                        }


                    }
                }

            }
            return Model;
        }
        catch (Exception e)
        {
            return Model;
        }


    }
    public object ConvertToMode(DataRow row, object Model)
    {
        try
        {

            Dictionary<string, PropertyInfo> dicPropertyInfo = new Dictionary<string, PropertyInfo>();
            PropertyInfo[] propertyInfos = Model.GetType().GetProperties();
            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                if (!dicPropertyInfo.ContainsKey(propertyInfo.Name))
                {
                    dicPropertyInfo.Add(propertyInfo.Name, propertyInfo);
                }
            }
            if (row != null)
            {

                int count = row.Table.Columns.Count;
                for (int j = 0; j < count; j++)
                {

                    string strColumnName = row.Table.Columns[j].ColumnName;
                    if (!dicPropertyInfo.ContainsKey(strColumnName))
                        continue;

                    object bjValue = row[strColumnName];
                    if (bjValue == null || bjValue is DBNull || row[strColumnName] == null)
                        continue;

                    if (dicPropertyInfo[strColumnName].PropertyType.Name.ToLower() == "decimal")
                    {
                        object bjValue2 = Convert.ToDecimal(row[strColumnName]);
                        dicPropertyInfo[strColumnName].SetValue(Model, bjValue2, null);
                    }
                    else if (dicPropertyInfo[strColumnName].PropertyType.Name.ToLower() == "datetime")
                    {

                        DateTime bjValue2 = Convert.ToDateTime(row[strColumnName]);
                        dicPropertyInfo[strColumnName].SetValue(Model, bjValue2, null);
                    }
                    else if (dicPropertyInfo[strColumnName].PropertyType.Name.ToLower() == "double")
                    {
                        double bjValue2 = Convert.ToDouble(row[strColumnName]);
                        dicPropertyInfo[strColumnName].SetValue(Model, bjValue2, null);
                    }
                    else if (dicPropertyInfo[strColumnName].PropertyType.Name.ToLower() == "int32")
                    {
                        int bjValue2 = Convert.ToInt32(row[strColumnName]);
                        dicPropertyInfo[strColumnName].SetValue(Model, bjValue2, null);
                    }
                    else
                    {
                        object bjValue2 = row[strColumnName];
                        dicPropertyInfo[strColumnName].SetValue(Model, bjValue2, null);
                    }



                }

            }
            return Model;
        }
        catch (Exception e)
        {
            return Model;
        }


    }
    //datatable to list<model>
    public List<T> ConvertToMode<T>(DataTable table, T Model, List<T> ListModel) where T : new()
    {
        try
        {

            Dictionary<string, PropertyInfo> dicPropertyInfo = new Dictionary<string, PropertyInfo>();
            PropertyInfo[] propertyInfos = Model.GetType().GetProperties();
            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                if (!dicPropertyInfo.ContainsKey(propertyInfo.Name))
                {
                    dicPropertyInfo.Add(propertyInfo.Name, propertyInfo);
                }
            }
            if (table.Rows.Count > 0)
            {
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    var MM = new T();
                    int count = table.Columns.Count;
                    for (int j = 0; j < table.Columns.Count; j++)
                    {

                        string strColumnName = table.Columns[j].ColumnName;
                        if (!dicPropertyInfo.ContainsKey(strColumnName))
                            continue;

                        object bjValue = table.Rows[i][strColumnName];
                        if (bjValue == null || bjValue is DBNull || table.Rows[i][strColumnName] == null)
                            continue;

                        if (dicPropertyInfo[strColumnName].PropertyType.Name.ToLower() == "decimal")
                        {
                            object bjValue2 = Convert.ToDecimal(table.Rows[i][strColumnName]);
                            dicPropertyInfo[strColumnName].SetValue(MM, bjValue2, null);
                        }
                        else if (dicPropertyInfo[strColumnName].PropertyType.Name.ToLower() == "datetime")
                        {

                            DateTime bjValue2 = Convert.ToDateTime(table.Rows[i][strColumnName]);
                            dicPropertyInfo[strColumnName].SetValue(MM, bjValue2, null);
                        }
                        else if (dicPropertyInfo[strColumnName].PropertyType.Name.ToLower() == "double")
                        {
                            double bjValue2 = Convert.ToDouble(table.Rows[i][strColumnName]);
                            dicPropertyInfo[strColumnName].SetValue(MM, bjValue2, null);
                        }
                        else if (dicPropertyInfo[strColumnName].PropertyType.Name.ToLower() == "int32")
                        {
                            int bjValue2 = Convert.ToInt32(table.Rows[i][strColumnName]);
                            dicPropertyInfo[strColumnName].SetValue(MM, bjValue2, null);
                        }
                        else
                        {
                            object bjValue2 = table.Rows[i][strColumnName];
                            dicPropertyInfo[strColumnName].SetValue(MM, bjValue2, null);
                        }

                    }
                    ListModel.Add(MM);
                }

            }
            return ListModel;
        }
        catch (Exception e)
        {
            return ListModel;
        }


    }
    //datatable to list<model>
    public List<T> ConvertToMode<T>(DataRow row, T Model, List<T> ListModel) where T : new()
    {
        try
        {

            Dictionary<string, PropertyInfo> dicPropertyInfo = new Dictionary<string, PropertyInfo>();
            PropertyInfo[] propertyInfos = Model.GetType().GetProperties();
            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                if (!dicPropertyInfo.ContainsKey(propertyInfo.Name))
                {
                    dicPropertyInfo.Add(propertyInfo.Name, propertyInfo);
                }
            }

            var MM = new T();
            //int count = row.Table.Columns.Count;
            for (int j = 0; j < row.Table.Columns.Count; j++)
            {

                string strColumnName = row.Table.Columns[j].ColumnName;
                if (!dicPropertyInfo.ContainsKey(strColumnName))
                    continue;

                object bjValue = row[strColumnName];
                if (bjValue == null || bjValue is DBNull || row[strColumnName] == null)
                    continue;

                if (dicPropertyInfo[strColumnName].PropertyType.Name.ToLower() == "decimal")
                {
                    object bjValue2 = Convert.ToDecimal(row[strColumnName]);

                    dicPropertyInfo[strColumnName].SetValue(MM, bjValue2, null);
                }
                else if (dicPropertyInfo[strColumnName].PropertyType.Name.ToLower() == "datetime")
                {

                    DateTime bjValue2 = Convert.ToDateTime(row[strColumnName]);
                    dicPropertyInfo[strColumnName].SetValue(MM, bjValue2, null);
                }
                else if (dicPropertyInfo[strColumnName].PropertyType.Name.ToLower() == "double")
                {
                    double bjValue2 = Convert.ToDouble(row[strColumnName]);
                    dicPropertyInfo[strColumnName].SetValue(MM, bjValue2, null);
                }
                else if (dicPropertyInfo[strColumnName].PropertyType.Name.ToLower() == "int32")
                {
                    int bjValue2 = Convert.ToInt32(row[strColumnName]);
                    dicPropertyInfo[strColumnName].SetValue(MM, bjValue2, null);
                }
                else
                {
                    object bjValue2 = row[strColumnName];
                    dicPropertyInfo[strColumnName].SetValue(MM, bjValue2, null);
                }

            }
            ListModel.Add(MM);



            return ListModel;
        }
        catch (Exception e)
        {
            return ListModel;
        }


    }



    public object ConvertToInMode(DataTable table, object Model)
    {
        try
        {

            Dictionary<string, PropertyInfo> dicPropertyInfo = new Dictionary<string, PropertyInfo>();
            PropertyInfo[] propertyInfos = Model.GetType().GetProperties();
            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                if (!dicPropertyInfo.ContainsKey(propertyInfo.Name))
                {
                    dicPropertyInfo.Add(propertyInfo.Name, propertyInfo);
                }
            }

            string report_in_conn = ReportHelper.Report_In_Conn;
            string fastpay = "select fastpay from dt_diction_quickpay where fastpay > 0";
            Dictionary<string, string> dic_fastpay = GetPayDic(fastpay, report_in_conn);
            string alipay = "select alipay from dt_diction_quickpay where alipay > 0";
            Dictionary<string, string> dic_alipay = GetPayDic(alipay, report_in_conn);
            string wechat = "select wechat from dt_diction_quickpay where Wechat > 0";
            Dictionary<string, string> dic_wechat = GetPayDic(wechat, report_in_conn);
            string qqpay = "select QQ from dt_diction_quickpay where QQ > 0";
            Dictionary<string, string> dic_qq = GetPayDic(qqpay, report_in_conn);
            string unionpay = "select unionpay from dt_diction_quickpay where unionpay > 0";
            Dictionary<string, string> dic_union = GetPayDic(unionpay, report_in_conn);

            if (table.Rows.Count > 0)
            {
                // int count = table.Rows.Count;
                //for (int j = 0; j < table.Rows.Count; j++)
                //{
                //string strColumnName = table.Columns[j].ColumnName;

                int fast_count = table.Select("type2 in (" + dic_fastpay["fastpay"] + ") and state = 1").Count();
                int alipay_count = table.Select("type2 in (" + dic_alipay["alipay"] + ",22) and state = 1").Count();
                int wechat_count = table.Select("type2 in (" + dic_wechat["wechat"] + ",23) and state = 1").Count();
                int QQ_count = table.Select("type2 in (" + dic_qq["QQ"] + ",201) and state = 1").Count();
                int fourth_count = table.Select("type2 =268 and state = 1").Count();
                int unionpay_count = table.Select("type2 in (" + dic_union["unionpay"] + ",291) and state = 1").Count();
                int bank_count = table.Select("type2 =21 and state = 1").Count();
                int rgck_count = table.Select("type2 in (11,20) and state = 1").Count();
                int qthd_count = table.Select("type2=12  and state=1").Count();
                int cdn_count = table.Select("type2=17 and state=1").Count();
                int dlgz_count = table.Select("type2=14  and state=1").Count();
                int dlfh_count = table.Select("type2=13  and state=1").Count();
                int xthd_count = table.Select("type2=7  and state=1").Count();

                // 快捷
                for (int f = 0; f < fast_count; f++)
                {
                    object bjValue2 = Convert.ToDecimal(table.Select("type2 in (" + dic_fastpay["fastpay"] + ") and state = 1")[f]["money"]) + Convert.ToDecimal(dicPropertyInfo["fastpaymoney"].GetValue(Model, null));
                    dicPropertyInfo["fastpaymoney"].SetValue(Model, bjValue2, null);
                }
                //支付寶
                for (int f = 0; f < alipay_count; f++)
                {
                    object bjValue2 = Convert.ToDecimal(table.Select("type2 in (" + dic_alipay["alipay"] + ",22) and state = 1")[f]["money"]) + Convert.ToDecimal(dicPropertyInfo["aliypaymony"].GetValue(Model, null));
                    dicPropertyInfo["aliypaymony"].SetValue(Model, bjValue2, null);
                }
                //微信
                for (int f = 0; f < wechat_count; f++)
                {
                    object bjValue2 = Convert.ToDecimal(table.Select("type2 in (" + dic_wechat["wechat"] + ",23) and state = 1")[f]["money"]) + Convert.ToDecimal(dicPropertyInfo["wechatmony"].GetValue(Model, null));
                    dicPropertyInfo["wechatmony"].SetValue(Model, bjValue2, null);
                }
                //QQ
                for (int f = 0; f < QQ_count; f++)
                {
                    object bjValue2 = Convert.ToDecimal(table.Select("type2 in (" + dic_qq["QQ"] + ",201) and state = 1")[f]["money"]) + Convert.ToDecimal(dicPropertyInfo["qqmoney"].GetValue(Model, null));
                    dicPropertyInfo["qqmoney"].SetValue(Model, bjValue2, null);
                }
                //第四方
                for (int f = 0; f < fourth_count; f++)
                {
                    object bjValue2 = Convert.ToDecimal(table.Select("type2 =268 and state = 1")[f]["money"]) + Convert.ToDecimal(dicPropertyInfo["fourthmoney"].GetValue(Model, null));
                    dicPropertyInfo["fourthmoney"].SetValue(Model, bjValue2, null);
                }
                //銀聯
                for (int f = 0; f < unionpay_count; f++)
                {
                    object bjValue2 = Convert.ToDecimal(table.Select("type2 in (" + dic_union["unionpay"] + ",291) and state = 1")[f]["money"]) + Convert.ToDecimal(dicPropertyInfo["unionpaymoney"].GetValue(Model, null));
                    dicPropertyInfo["unionpaymoney"].SetValue(Model, bjValue2, null);
                }
                //銀行
                for (int f = 0; f < bank_count; f++)
                {
                    object bjValue2 = Convert.ToDecimal(table.Select("type2 =21 and state = 1")[f]["money"]) + Convert.ToDecimal(dicPropertyInfo["bankmony"].GetValue(Model, null));
                    dicPropertyInfo["bankmony"].SetValue(Model, bjValue2, null);
                }
                //人工
                for (int f = 0; f < rgck_count; f++)
                {
                    object bjValue2 = Convert.ToDecimal(table.Select("type2 in (11,20) and state = 1")[f]["money"]) + Convert.ToDecimal(dicPropertyInfo["rgckmony"].GetValue(Model, null));
                    dicPropertyInfo["rgckmony"].SetValue(Model, bjValue2, null);
                }
                //其他
                for (int f = 0; f < qthd_count; f++)
                {
                    object bjValue2 = Convert.ToDecimal(table.Select("type2=12  and state=1")[f]["money"]) + Convert.ToDecimal(dicPropertyInfo["qthdmony"].GetValue(Model, null));
                    dicPropertyInfo["qthdmony"].SetValue(Model, bjValue2, null);
                }
                //撤單
                for (int f = 0; f < cdn_count; f++)
                {
                    object bjValue2 = Convert.ToDecimal(table.Select("type2=17 and state=1")[f]["money"]) + Convert.ToDecimal(dicPropertyInfo["cdmony"].GetValue(Model, null));
                    dicPropertyInfo["cdmony"].SetValue(Model, bjValue2, null);
                }
                //代理工資
                for (int f = 0; f < dlgz_count; f++)
                {
                    object bjValue2 = Convert.ToDecimal(table.Select("type2=14 and state=1")[f]["money"]) + Convert.ToDecimal(dicPropertyInfo["dlgzmony"].GetValue(Model, null));
                    dicPropertyInfo["dlgzmony"].SetValue(Model, bjValue2, null);
                }
                //代理分紅
                for (int f = 0; f < dlfh_count; f++)
                {
                    object bjValue2 = Convert.ToDecimal(table.Select("type2=13 and state=1")[f]["money"]) + Convert.ToDecimal(dicPropertyInfo["dlfhmony"].GetValue(Model, null));
                    dicPropertyInfo["dlfhmony"].SetValue(Model, bjValue2, null);
                }
                //系統活動
                for (int f = 0; f < xthd_count; f++)
                {
                    object bjValue2 = Convert.ToDecimal(table.Select("type2=7 and state=1")[f]["money"]) + Convert.ToDecimal(dicPropertyInfo["xthdmony"].GetValue(Model, null));
                    dicPropertyInfo["xthdmony"].SetValue(Model, bjValue2, null);
                }


            }
            return Model;
        }
        catch (Exception e)
        {
            return Model;
        }


    }
    public object ConvertToOutMode(DataTable table, object Model)
    {
        try
        {

            Dictionary<string, PropertyInfo> dicPropertyInfo = new Dictionary<string, PropertyInfo>();
            PropertyInfo[] propertyInfos = Model.GetType().GetProperties();
            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                if (!dicPropertyInfo.ContainsKey(propertyInfo.Name))
                {
                    dicPropertyInfo.Add(propertyInfo.Name, propertyInfo);
                }
            }

            if (table.Rows.Count > 0)
            {
                // int count = table.Rows.Count;
                //for (int j = 0; j < table.Rows.Count; j++)
                //{
                //string strColumnName = table.Columns[j].ColumnName;

                int rgtc_count = table.Select("type2 =3 and state = 1").Count();
                int wctc_count = table.Select("type2 =9 and state = 1").Count();
                int xztc_count = table.Select("type2 =10 and state = 1").Count();
                int jj_count = table.Select("type2 =3 and state = 3").Count();


                // 人工提出
                for (int f = 0; f < rgtc_count; f++)
                {
                    object bjValue2 = Convert.ToDecimal(table.Select("type2 =3 and state = 1")[f]["money"]) + Convert.ToDecimal(dicPropertyInfo["rgtcmoney"].GetValue(Model, null));
                    dicPropertyInfo["rgtcmoney"].SetValue(Model, bjValue2, null);
                }
                //誤存提出
                for (int f = 0; f < wctc_count; f++)
                {
                    object bjValue2 = Convert.ToDecimal(table.Select("type2 =9 and state = 1) and state = 1")[f]["money"]) + Convert.ToDecimal(dicPropertyInfo["wctcmony"].GetValue(Model, null));
                    dicPropertyInfo["wctcmony"].SetValue(Model, bjValue2, null);
                }
                //行政提出
                for (int f = 0; f < xztc_count; f++)
                {
                    object bjValue2 = Convert.ToDecimal(table.Select("type2 =10 and state = 1")[f]["money"]) + Convert.ToDecimal(dicPropertyInfo["xztcmony"].GetValue(Model, null));
                    dicPropertyInfo["xztcmony"].SetValue(Model, bjValue2, null);
                }
                //拒絕
                for (int f = 0; f < jj_count; f++)
                {
                    object bjValue2 = Convert.ToDecimal(table.Select("type2 =3 and state = 3")[f]["money"]) + Convert.ToDecimal(dicPropertyInfo["jjmony"].GetValue(Model, null));
                    dicPropertyInfo["jjmony"].SetValue(Model, bjValue2, null);
                }








                //  }
            }
            return Model;
        }
        catch (Exception e)
        {
            return Model;
        }


    }
    /// <summary>
    /// 实体类转换成DataTable
    /// </summary>
    /// <param name="model">实体类对象</param>
    /// <returns></returns>
    public void ConvertReportModelToModel(ReportSummary model_report, object mode_source)
    {
        Dictionary<string, PropertyInfo> dicPropertyInfo = new Dictionary<string, PropertyInfo>();
        PropertyInfo[] propertyInfos = mode_source.GetType().GetProperties();
        foreach (PropertyInfo propertyInfo in propertyInfos)
        {
            if (!dicPropertyInfo.ContainsKey(propertyInfo.Name))
            {
                dicPropertyInfo.Add(propertyInfo.Name, propertyInfo);
            }
        }
        //获取实体类的公共属性
        PropertyInfo[] report_propertyInfos = model_report.GetType().GetProperties();
        foreach (PropertyInfo propertyInfo in report_propertyInfos)
        {
            switch (propertyInfo.Name)
            {
                case "TotalInMoney":
                    model_report.TotalInMoney = Convert.ToDecimal(dicPropertyInfo["czmony"].GetValue(mode_source, null)).ToString("0.00") + "/" + dicPropertyInfo["cznumber"].GetValue(mode_source, null) + "人";
                    break;
                case "InMoneyNum":
                    model_report.InMoneyNum = Convert.ToDecimal(dicPropertyInfo["czcount"].GetValue(mode_source, null)).ToString("0.0") + "/" + dicPropertyInfo["cznumber"].GetValue(mode_source, null) + "人";
                    break;
                case "In_FastPrepaid":
                    model_report.In_FastPrepaid = Convert.ToDecimal(dicPropertyInfo["fastpaymoney"].GetValue(mode_source, null)).ToString("0.0") + "/" + dicPropertyInfo["fastnumber"].GetValue(mode_source, null) + "人";
                    break;
                case "In_BankPrepaid":
                    model_report.In_BankPrepaid = Convert.ToDecimal(dicPropertyInfo["bankmony"].GetValue(mode_source, null)).ToString("0.0") + "/" + dicPropertyInfo["banknumber"].GetValue(mode_source, null) + "人";
                    break;
                case "In_AlipayPrepaid":
                    model_report.In_AlipayPrepaid = Convert.ToDecimal(dicPropertyInfo["aliypaymony"].GetValue(mode_source, null)).ToString("0.0") + "/" + dicPropertyInfo["aliypaynumber"].GetValue(mode_source, null) + "人";
                    break;
                case "In_WeChatPrepaid":
                    model_report.In_WeChatPrepaid = Convert.ToDecimal(dicPropertyInfo["wechatmony"].GetValue(mode_source, null)).ToString("0.0") + "/" + dicPropertyInfo["wechatnumber"].GetValue(mode_source, null) + "人";
                    break;
                case "In_QQ":
                    model_report.In_QQ = Convert.ToDecimal(dicPropertyInfo["qqmoney"].GetValue(mode_source, null)).ToString("0.0") + "/" + dicPropertyInfo["qqnumber"].GetValue(mode_source, null) + "人";
                    model_report.In_QQPrepaid = Convert.ToDecimal(dicPropertyInfo["qqmoney"].GetValue(mode_source, null)).ToString("0.0") + "/" + dicPropertyInfo["qqnumber"].GetValue(mode_source, null) + "人";
                    break;
                case "In_FourthPrepaid":
                    model_report.In_FourthPrepaid = Convert.ToDecimal(dicPropertyInfo["fourthmoney"].GetValue(mode_source, null)).ToString("0.0") + "/" + dicPropertyInfo["fourthnumber"].GetValue(mode_source, null) + "人";
                    break;
                case "In_UnionpayPrepaid":
                    model_report.In_UnionpayPrepaid = Convert.ToDecimal(dicPropertyInfo["unionpaymoney"].GetValue(mode_source, null)).ToString("0.0") + "/" + dicPropertyInfo["unionpaynumber"].GetValue(mode_source, null) + "人";
                    break;
                case "In_ArtificialDeposit":
                    model_report.In_ArtificialDeposit = Convert.ToDecimal(dicPropertyInfo["rgckmony"].GetValue(mode_source, null)).ToString("0.0") + "/" + dicPropertyInfo["rgcknumber"].GetValue(mode_source, null) + "人";
                    break;
                case "Out_RebateAccount":
                    model_report.Out_RebateAccount = Convert.ToDecimal(dicPropertyInfo["rebatemoney"].GetValue(mode_source, null)).ToString("0.0") + "/" + dicPropertyInfo["rebatenumber"].GetValue(mode_source, null) + "人";
                    break;
                case "TotalDiscount":
                    model_report.TotalDiscount = Convert.ToDecimal(dicPropertyInfo["hdljmony"].GetValue(mode_source, null)).ToString("0.0") + "/" + dicPropertyInfo["hdljnumber"].GetValue(mode_source, null) + "人";
                    break;
                case "Out_ActivityDiscountAccount":
                    model_report.Out_ActivityDiscountAccount = Convert.ToDecimal(dicPropertyInfo["xthdmony"].GetValue(mode_source, null)).ToString("0.0") + "/" + dicPropertyInfo["xthdnumber"].GetValue(mode_source, null) + "人";
                    break;
                case "Out_OtherDiscountAccount":
                    model_report.Out_OtherDiscountAccount = Convert.ToDecimal(dicPropertyInfo["qthdmony"].GetValue(mode_source, null)).ToString("0.0") + "/" + dicPropertyInfo["qthdnumber"].GetValue(mode_source, null) + "人";
                    break;
                case "Out_WinningAccount":
                    model_report.Out_WinningAccount = Convert.ToDecimal(dicPropertyInfo["zjmoney"].GetValue(mode_source, null)).ToString("0.0") + "/" + dicPropertyInfo["zjnumber"].GetValue(mode_source, null) + "人";
                    break;
                case "Out_CancelAccount":
                    model_report.Out_CancelAccount = Convert.ToDecimal(dicPropertyInfo["cdmony"].GetValue(mode_source, null)).ToString("0.0") + "/" + dicPropertyInfo["cdnumber"].GetValue(mode_source, null) + "人";
                    break;
                case "Bonus":
                    model_report.Bonus = Convert.ToDecimal(dicPropertyInfo["dlgzmony"].GetValue(mode_source, null)).ToString("0.0") + "/" + dicPropertyInfo["dlgznumber"].GetValue(mode_source, null) + "人";
                    break;
                case "Wages":
                    model_report.Wages = Convert.ToDecimal(dicPropertyInfo["dlfhmony"].GetValue(mode_source, null)).ToString("0.0") + "/" + dicPropertyInfo["dlfhnumber"].GetValue(mode_source, null) + "人";
                    break;
                case "Out_Account":
                    model_report.Out_Account = Convert.ToDecimal(dicPropertyInfo["txmony"].GetValue(mode_source, null)).ToString("0.0") + "/" + dicPropertyInfo["txnumber"].GetValue(mode_source, null) + "人";
                    break;
                case "TotalArtificialOut":
                    model_report.TotalArtificialOut = Convert.ToDecimal(dicPropertyInfo["rgtcmoney"].GetValue(mode_source, null)).ToString("0.0") + "/" + dicPropertyInfo["rgtcnumber"].GetValue(mode_source, null) + "人";
                    break;
                case "Out_MistakeAccount":
                    model_report.Out_MistakeAccount = Convert.ToDecimal(dicPropertyInfo["wctcmony"].GetValue(mode_source, null)).ToString("0.0") + "/" + dicPropertyInfo["wctcnumber"].GetValue(mode_source, null) + "人";
                    break;
                case "Out_AdministrationAccount":
                    model_report.Out_AdministrationAccount = Convert.ToDecimal(dicPropertyInfo["xztcmony"].GetValue(mode_source, null)).ToString("0.0") + "/" + dicPropertyInfo["xztcnumber"].GetValue(mode_source, null) + "人";
                    break;
                case "Out_RefuseAccount":
                    model_report.Out_RefuseAccount = Convert.ToDecimal(dicPropertyInfo["jjmony"].GetValue(mode_source, null)).ToString("0.0") + "/" + dicPropertyInfo["jjnumber"].GetValue(mode_source, null) + "人";
                    break;
                case "Out_Account_Num":
                    model_report.Out_Account_Num = Convert.ToDecimal(dicPropertyInfo["txcount"].GetValue(mode_source, null)).ToString("0.0") + "/" + dicPropertyInfo["txnumber"].GetValue(mode_source, null) + "人";
                    break;
                case "Out_BettingAccount":
                    model_report.Out_BettingAccount = Convert.ToDecimal(dicPropertyInfo["tzmony"].GetValue(mode_source, null)).ToString("0.0") + "/" + dicPropertyInfo["tznumber"].GetValue(mode_source, null) + "人";
                    break;
                case "Out_BettingAccount_Num":
                    model_report.Out_BettingAccount_Num = Convert.ToDecimal(dicPropertyInfo["tzcount"].GetValue(mode_source, null)).ToString("0.0") + "/" + dicPropertyInfo["tznumber"].GetValue(mode_source, null) + "人";
                    break;
                case "InMoneyTime":
                    model_report.InMoneyTime = Convert.ToDecimal(dicPropertyInfo["operattime"].GetValue(mode_source, null)).ToString("0.0") + "/" + Convert.ToDecimal(dicPropertyInfo["czcount"].GetValue(mode_source, null)).ToString("0");
                    break;
                case "OutMoneyTime":
                    model_report.OutMoneyTime = dicPropertyInfo["opearttime"].GetValue(mode_source, null).ToString() + "/" + Convert.ToDecimal(dicPropertyInfo["txcount"].GetValue(mode_source, null)).ToString("0");
                    break;
                case "RewardMoney":
                    model_report.RewardMoney = Convert.ToDecimal(dicPropertyInfo["RewardMoney"].GetValue(mode_source, null)).ToString("0.0") + "/" + dicPropertyInfo["RewardNum"].GetValue(mode_source, null) + "人";
                    break;
                case "RewardCount":
                    model_report.RewardCount = Convert.ToDecimal(dicPropertyInfo["RewardCount"].GetValue(mode_source, null)).ToString("0.0") + "/" + dicPropertyInfo["RewardNum"].GetValue(mode_source, null) + "人";
                    break;

            }

        }

    }
    //datatable分頁
    public DataTable GetPagedTable(DataTable dt, int PageIndex, int PageSize)
    {
        if (PageIndex == 0)
        {
            return dt;
        }
        DataTable NewDt = dt.Copy();
        NewDt.Clear();
        //起始列
        int rowbegin = (PageIndex - 1) * PageSize;
        //結束列
        int rowend = PageIndex * PageSize;
        if (rowbegin >= dt.Rows.Count)
        {
            return NewDt;
        }

        if (rowend > dt.Rows.Count)
        {
            rowend = dt.Rows.Count;
        }
        //產生新的DataTable
        for (int i = rowbegin; i <= rowend - 1; i++)
        {
            DataRow newdr = NewDt.NewRow();
            DataRow dr = dt.Rows[i];
            foreach (DataColumn column in dt.Columns)
            {
                newdr[column.ColumnName] = dr[column.ColumnName];
            }
            NewDt.Rows.Add(newdr);
        }
        return NewDt;
    }


    public Dictionary<string, string> GetPayDic(string sql, string reportcon)
    {
        Dictionary<string, string> paydic = new Dictionary<string, string>();
        DataTable dt = GetQueryFromReport(sql, reportcon);
        string value = "";
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            value += dt.Rows[i][0].ToString() + ",";

        }
        if (value.Length > 0)
        {
            value = value.Substring(0, value.Length - 1);
            paydic.Add(dt.Columns[0].ColumnName, value);
        }
        else
        {
            paydic.Add(dt.Columns[0].ColumnName, "0");
        }



        return paydic;

    }

    public static DataTable GetQueryFromReport(string sql, string reportcon)
    {

        System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(reportcon);
        DataSet ds1 = new DataSet();
        try
        {
            conn.Open();
            System.Data.SqlClient.SqlDataAdapter sda = new System.Data.SqlClient.SqlDataAdapter(sql, conn);
            sda.Fill(ds1);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (conn.State == ConnectionState.Open)
            {
                conn.Dispose();
                conn.Close();
            }
        }
        return ds1.Tables[0];
    }

    public void ConvertInoutAccountModelToModel(dt_report_inoutaccount_new model_report, object mode_source)
    {
        Dictionary<string, PropertyInfo> dicPropertyInfo = new Dictionary<string, PropertyInfo>();
        PropertyInfo[] propertyInfos = mode_source.GetType().GetProperties();
        foreach (PropertyInfo propertyInfo in propertyInfos)
        {
            if (!dicPropertyInfo.ContainsKey(propertyInfo.Name))
            {
                dicPropertyInfo.Add(propertyInfo.Name, propertyInfo);
            }
        }
        //获取实体类的公共属性
        PropertyInfo[] report_propertyInfos = model_report.GetType().GetProperties();
        foreach (PropertyInfo propertyInfo in report_propertyInfos)
        {
            switch (propertyInfo.Name)
            {
                case "OutMoneyNum":
                    model_report.OutMoneyNum = Convert.ToDouble(dicPropertyInfo["txcount"].GetValue(mode_source, null));
                    break;
                case "OutMoneyTime":
                    model_report.OutMoneyTime = Convert.ToDouble(dicPropertyInfo["opearttime"].GetValue(mode_source, null));
                    break;
                case "InMoneyNum":
                    model_report.InMoneyNum = Convert.ToDouble(dicPropertyInfo["czcount"].GetValue(mode_source, null));
                    break;
                case "InMoneyTime":
                    model_report.InMoneyTime = Convert.ToDouble(dicPropertyInfo["operattime"].GetValue(mode_source, null));
                    break;
                case "Wages":
                    model_report.Wages = Convert.ToDecimal(dicPropertyInfo["dlfhmony"].GetValue(mode_source, null));
                    break;
                case "Bonus":
                    model_report.Bonus = Convert.ToDecimal(dicPropertyInfo["dlgzmony"].GetValue(mode_source, null));
                    break;
                // case "WinRate":
                //     model_report.WinRate = Convert.ToDecimal(dicPropertyInfo["wechatmony"].GetValue(mode_source, null)).ToString("0.0") + "/" + dicPropertyInfo["wechatnumber"].GetValue(mode_source, null) + "人";
                //      break;
                //case "user_id":
                //    model_report.user_id = Convert.ToInt32(dicPropertyInfo["user_id"].GetValue(mode_source, null));
                //    break;
                //case "SourceName":
                //    model_report.SourceName = Convert.ToDecimal(dicPropertyInfo["rebatemoney"].GetValue(mode_source, null)).ToString("0.0") + "/" + dicPropertyInfo["rebatenumber"].GetValue(mode_source, null) + "人";
                //    break;
                //case "ProfitLoss":
                //    model_report.ProfitLoss = Convert.ToDecimal(dicPropertyInfo["hdljmony"].GetValue(mode_source, null)).ToString("0.0") + "/" + dicPropertyInfo["hdljnumber"].GetValue(mode_source, null) + "人";
                //    break;
                case "Out_WinningAccount":
                    model_report.Out_WinningAccount = Convert.ToDecimal(dicPropertyInfo["zjmoney"].GetValue(mode_source, null));
                    break;
                case "Out_RefuseAccount":
                    model_report.Out_RefuseAccount = Convert.ToDecimal(dicPropertyInfo["jjmony"].GetValue(mode_source, null));
                    break;
                case "Out_RebateAccount":
                    model_report.Out_RebateAccount = Convert.ToDecimal(dicPropertyInfo["rebatemoney"].GetValue(mode_source, null));
                    break;
                case "Out_OtherDiscountAccount":
                    model_report.Out_OtherDiscountAccount = Convert.ToDecimal(dicPropertyInfo["qthdmony"].GetValue(mode_source, null));
                    break;
                case "Out_MistakeAccount":
                    model_report.Out_MistakeAccount = Convert.ToDecimal(dicPropertyInfo["wctcmony"].GetValue(mode_source, null));
                    break;
                case "Out_CancelAccount":
                    model_report.Out_CancelAccount = Convert.ToDecimal(dicPropertyInfo["cdmony"].GetValue(mode_source, null));
                    break;
                case "Out_BettingAccount":
                    model_report.Out_BettingAccount = Convert.ToDecimal(dicPropertyInfo["tzmony"].GetValue(mode_source, null));
                    break;
                case "Out_AdministrationAccount":
                    model_report.Out_AdministrationAccount = Convert.ToDecimal(dicPropertyInfo["xztcmony"].GetValue(mode_source, null));
                    break;
                case "Out_ActivityDiscountAccount":
                    model_report.Out_ActivityDiscountAccount = Convert.ToDecimal(dicPropertyInfo["xthdmony"].GetValue(mode_source, null));
                    break;
                case "Out_Account":
                    model_report.Out_Account = Convert.ToDecimal(dicPropertyInfo["txmony"].GetValue(mode_source, null));
                    break;
                case "In_WeChatPrepaid":
                    model_report.In_WeChatPrepaid = Convert.ToDecimal(dicPropertyInfo["wechatmony"].GetValue(mode_source, null));
                    break;
                case "In_QQ":
                    model_report.In_QQ = Convert.ToDecimal(dicPropertyInfo["qqmoney"].GetValue(mode_source, null));
                    break;
                case "In_FourthPrepaid":
                    model_report.In_FourthPrepaid = Convert.ToDecimal(dicPropertyInfo["fourthmoney"].GetValue(mode_source, null));
                    break;
                case "In_UnionpayPrepaid":
                    model_report.In_UnionpayPrepaid = Convert.ToDecimal(dicPropertyInfo["unionpaymoney"].GetValue(mode_source, null));
                    break;
                case "In_FastPrepaid":
                    model_report.In_FastPrepaid = Convert.ToDecimal(dicPropertyInfo["fastpaymoney"].GetValue(mode_source, null));
                    break;
                case "In_BankPrepaid":
                    model_report.In_BankPrepaid = Convert.ToDecimal(dicPropertyInfo["bankmony"].GetValue(mode_source, null));
                    break;
                case "In_ArtificialDeposit":
                    model_report.In_ArtificialDeposit = Convert.ToDecimal(dicPropertyInfo["rgckmony"].GetValue(mode_source, null));
                    break;
                case "In_AlipayPrepaid":
                    model_report.In_AlipayPrepaid = Convert.ToDecimal(dicPropertyInfo["aliypaymony"].GetValue(mode_source, null));
                    break;

                //case "identityid":
                //    model_report.identityid = dicPropertyInfo["identityid"].GetValue(mode_source, null).ToString() ;
                //    break;
                //case "id":
                //    model_report.id = Convert.ToInt32(dicPropertyInfo["id"].GetValue(mode_source, null));
                //    break;
                //case "group_name":
                //    model_report.group_name = dicPropertyInfo["opearttime"].GetValue(mode_source, null).ToString() + "/" + Convert.ToDecimal(dicPropertyInfo["txcount"].GetValue(mode_source, null)).ToString("0");
                //    break;
                //case "agent_id":
                //    model_report.agent_id = dicPropertyInfo["opearttime"].GetValue(mode_source, null).ToString() + "/" + Convert.ToDecimal(dicPropertyInfo["txcount"].GetValue(mode_source, null)).ToString("0");
                //    break;
                case "AddTime":
                    model_report.AddTime = System.DateTime.Now.Date;
                    break;
                case "UpdateTime":
                    model_report.UpdateTime = System.DateTime.Now.Date;
                    break;

            }

        }

    }
    /// <summary>
    /// DataRow转换成实体类
    /// </summary>
    /// <param name="dataRow">DataRow</param>
    /// <param name="Model">实体类</param>
    /// <returns></returns>
    public void ConvertDataRowToModel(DataRow dataRow, object Model)
    {
        Dictionary<string, PropertyInfo> dicPropertyInfo = new Dictionary<string, PropertyInfo>();
        PropertyInfo[] propertyInfos = Model.GetType().GetProperties();
        foreach (PropertyInfo propertyInfo in propertyInfos)
        {
            if (!dicPropertyInfo.ContainsKey(propertyInfo.Name))
            {
                dicPropertyInfo.Add(propertyInfo.Name, propertyInfo);
            }
        }
        for (int j = 0; j < dataRow.Table.Columns.Count; j++)
        {
            string strColumnName = dataRow.Table.Columns[j].ColumnName;
            if (!dicPropertyInfo.ContainsKey(strColumnName))
                continue;

            object bjValue = dataRow[strColumnName];
            if (bjValue == null || bjValue is DBNull)
                continue;
            if (dicPropertyInfo[strColumnName].PropertyType.Name.ToLower() == "int32")
            {
                int bjValue2 = Convert.ToInt32(dataRow[strColumnName]);
                dicPropertyInfo[strColumnName].SetValue(Model, bjValue2, null);
            }

            dicPropertyInfo[strColumnName].SetValue(Model, bjValue, null);
        }
    }
    public void ConvertDataRowToModel_sql2(object report_inoutaccount, object Model)
    {
        Dictionary<string, PropertyInfo> dicPropertyInfo = new Dictionary<string, PropertyInfo>();
        PropertyInfo[] propertyInfos = Model.GetType().GetProperties();
        foreach (PropertyInfo propertyInfo in propertyInfos)
        {
            if (!dicPropertyInfo.ContainsKey(propertyInfo.Name))
            {
                dicPropertyInfo.Add(propertyInfo.Name, propertyInfo);
            }
        }
        for (int j = 0; j < report_inoutaccount.GetType().GetProperties().Count(); j++)
        {

            string strColumnName = report_inoutaccount.GetType().GetProperties()[j].Name;
            if (!dicPropertyInfo.ContainsKey(strColumnName))
                continue;

            object bjValue = report_inoutaccount.GetType().GetProperty(strColumnName).GetValue(report_inoutaccount, null);
            if (bjValue == null || bjValue is DBNull)
                continue;

            if (dicPropertyInfo[strColumnName].PropertyType.Name.ToLower() == "decimal")
            {
                decimal ovalue = Convert.ToDecimal(Model.GetType().GetProperty(strColumnName).GetValue(Model, null));
                decimal bjValue2 = Convert.ToDecimal(report_inoutaccount.GetType().GetProperty(strColumnName).GetValue(report_inoutaccount, null)) + ovalue;
                dicPropertyInfo[strColumnName].SetValue(Model, bjValue2, null);
            }
            else if (dicPropertyInfo[strColumnName].PropertyType.Name.ToLower() == "datetime")
            {

                DateTime bjValue2 = Convert.ToDateTime(report_inoutaccount.GetType().GetProperty(strColumnName).GetValue(report_inoutaccount, null));
                dicPropertyInfo[strColumnName].SetValue(Model, bjValue2, null);
            }
            else if (dicPropertyInfo[strColumnName].PropertyType.Name.ToLower() == "double")
            {
                double ovalue = Convert.ToDouble(Model.GetType().GetProperty(strColumnName).GetValue(Model, null));
                double bjValue2 = Convert.ToDouble(report_inoutaccount.GetType().GetProperty(strColumnName).GetValue(report_inoutaccount, null)) + ovalue;
                dicPropertyInfo[strColumnName].SetValue(Model, bjValue2, null);
            }
            else if (dicPropertyInfo[strColumnName].PropertyType.Name.ToLower() == "int32")
            {
                int bjValue2 = Convert.ToInt32(report_inoutaccount.GetType().GetProperty(strColumnName).GetValue(report_inoutaccount, null));
                dicPropertyInfo[strColumnName].SetValue(Model, bjValue2, null);
            }
            else
            {
                object bjValue2 = report_inoutaccount.GetType().GetProperty(strColumnName).GetValue(report_inoutaccount, null);
                dicPropertyInfo[strColumnName].SetValue(Model, bjValue2, null);
            }


        }
    }
    public void ConvertDataRowToModel_sql2(DataRow dataRow, object Model)
    {
        Dictionary<string, PropertyInfo> dicPropertyInfo = new Dictionary<string, PropertyInfo>();
        PropertyInfo[] propertyInfos = Model.GetType().GetProperties();
        foreach (PropertyInfo propertyInfo in propertyInfos)
        {
            if (!dicPropertyInfo.ContainsKey(propertyInfo.Name))
            {
                dicPropertyInfo.Add(propertyInfo.Name, propertyInfo);
            }
        }
        for (int j = 0; j < dataRow.Table.Columns.Count; j++)
        {

            string strColumnName = dataRow.Table.Columns[j].ColumnName;
            if (!dicPropertyInfo.ContainsKey(strColumnName))
                continue;

            object bjValue = dataRow[strColumnName];
            if (bjValue == null || bjValue is DBNull || dataRow[strColumnName] == null)
                continue;

            if (dicPropertyInfo[strColumnName].PropertyType.Name.ToLower() == "decimal")
            {
                decimal ovalue = Convert.ToDecimal(Model.GetType().GetProperty(strColumnName).GetValue(Model, null));
                decimal bjValue2 = Convert.ToDecimal(dataRow[strColumnName]) + ovalue;
                dicPropertyInfo[strColumnName].SetValue(Model, bjValue2, null);
            }
            else if (dicPropertyInfo[strColumnName].PropertyType.Name.ToLower() == "datetime")
            {

                DateTime bjValue2 = Convert.ToDateTime(dataRow[strColumnName]);
                dicPropertyInfo[strColumnName].SetValue(Model, bjValue2, null);
            }
            else if (dicPropertyInfo[strColumnName].PropertyType.Name.ToLower() == "double")
            {
                double ovalue = Convert.ToDouble(Model.GetType().GetProperty(strColumnName).GetValue(Model, null));
                double bjValue2 = Convert.ToDouble(dataRow[strColumnName]) + ovalue;
                dicPropertyInfo[strColumnName].SetValue(Model, bjValue2, null);
            }
            else if (dicPropertyInfo[strColumnName].PropertyType.Name.ToLower() == "int32")
            {
                int bjValue2 = Convert.ToInt32(dataRow[strColumnName]);
                dicPropertyInfo[strColumnName].SetValue(Model, bjValue2, null);
            }
            else
            {
                object bjValue2 = dataRow[strColumnName];
                dicPropertyInfo[strColumnName].SetValue(Model, bjValue2, null);
            }


        }
    }

    public void ConvertDataRowToModel_sql(DataRow dataRow, object Model)
    {
        Dictionary<string, PropertyInfo> dicPropertyInfo = new Dictionary<string, PropertyInfo>();
        PropertyInfo[] propertyInfos = Model.GetType().GetProperties();
        foreach (PropertyInfo propertyInfo in propertyInfos)
        {
            if (!dicPropertyInfo.ContainsKey(propertyInfo.Name))
            {
                dicPropertyInfo.Add(propertyInfo.Name, propertyInfo);
            }
        }
        for (int j = 0; j < dataRow.Table.Columns.Count; j++)
        {

            string strColumnName = dataRow.Table.Columns[j].ColumnName;
            if (!dicPropertyInfo.ContainsKey(strColumnName))
                continue;

            object bjValue = dataRow[strColumnName];
            if (bjValue == null || bjValue is DBNull || dataRow[strColumnName] == null)
                continue;

            if (dicPropertyInfo[strColumnName].PropertyType.Name.ToLower() == "decimal")
            {
                decimal bjValue2 = Convert.ToDecimal(dataRow[strColumnName]);
                dicPropertyInfo[strColumnName].SetValue(Model, bjValue2, null);
            }
            else if (dicPropertyInfo[strColumnName].PropertyType.Name.ToLower() == "datetime")
            {

                DateTime bjValue2 = Convert.ToDateTime(dataRow[strColumnName]);
                dicPropertyInfo[strColumnName].SetValue(Model, bjValue2, null);
            }
            else if (dicPropertyInfo[strColumnName].PropertyType.Name.ToLower() == "double")
            {
                double bjValue2 = Convert.ToDouble(dataRow[strColumnName]);
                dicPropertyInfo[strColumnName].SetValue(Model, bjValue2, null);
            }
            else if (dicPropertyInfo[strColumnName].PropertyType.Name.ToLower() == "int32")
            {
                int bjValue2 = Convert.ToInt32(dataRow[strColumnName]);
                dicPropertyInfo[strColumnName].SetValue(Model, bjValue2, null);
            }
            else if (dicPropertyInfo[strColumnName].PropertyType.Name.ToLower() == "string")
            {
                string bjValue2 = dataRow[strColumnName].ToString();
                dicPropertyInfo[strColumnName].SetValue(Model, bjValue2, null);
            }
            else
            {
                object bjValue2 = dataRow[strColumnName];
                dicPropertyInfo[strColumnName].SetValue(Model, bjValue2, null);
            }


        }
    }
    //將table轉換成物件
    public void GetReport(DataTable dt, EasyFrame.NewCommon.Model.dt_report_inoutaccount_sql Model, List<EasyFrame.NewCommon.Model.dt_report_inoutaccount_sql> ListModel)
    {
        try
        {
            for (int r = 0; r < dt.Rows.Count; r++)
            {

                EasyFrame.NewCommon.Model.dt_report_inoutaccount_sql dt_report = new EasyFrame.NewCommon.Model.dt_report_inoutaccount_sql();
                ConvertDataRowToModel_sql(dt.Rows[r], dt_report);
                ListModel.Add(dt_report);

            }
        }
        catch (Exception ex)
        {

        }
    }
    //將table轉換成物件
    public void GetAgentReport(DataTable dt, Object Model, List<ReportAgent> ListModel)
    {
        try
        {
            for (int r = 0; r < dt.Rows.Count; r++)
            {
                ReportAgent dt_report = new ReportAgent();
                ConvertDataRowToModel_sql(dt.Rows[r], dt_report);
                ListModel.Add(dt_report);
            }
        }
        catch (Exception ex)
        {

        }
    }


    /// <summary>
    /// 获取分页数据
    /// </summary>
    /// <param name="PageCurrent">当前页号</param>
    /// <param name="PageSize">每页多少条记录</param>
    /// <param name="FieldOrder">排序字段</param>
    /// <param name="Sql">SQL语句</param>
    /// <param name="totalRows">总数量</param>
    /// <returns></returns>
    //public DataTable GetPaginationData(int PageCurrent, int PageSize, string FieldOrder, string Sql, out int totalRows)
    //{
    //    totalRows = 0;
    //    //分页
    //    if (PageCurrent <= 0 && PageSize <= 0)
    //        return null;

    //    //起始行号
    //    int startNums = (PageSize * (PageCurrent - 1)) + 1;
    //    //结束行号
    //    int endNums = (PageSize * PageCurrent);
    //    string tmpSql = "with t_rowtable as (Select *,row_number() over( order by " + FieldOrder + ") as row_number from (" + Sql + ") aaa) Select count(*) from t_rowtable ";
    //    DataTable dataTable = DBcon.DbHelperSQL.GetQueryFromCongku(tmpSql);

    //    string strSql = "with t_rowtable as (Select *,row_number() over( order by " + FieldOrder + ") as row_number from (" + Sql + ") aaa) Select * from t_rowtable  ";
    //    totalRows = int.Parse(dataTable.Rows[0][0].ToString());
    //    strSql += "where row_number between " + startNums.ToString() + " and " + endNums.ToString();
    //    return DBcon.DbHelperSQL.GetQueryFromCongku(strSql);
    //}
    /// <summary>
    /// DataTable转换成字典
    /// </summary>
    /// <param name="dicData">字典数据</param>
    /// <param name="dataTable">DataTable数据</param>
    /// <param name="fieldName">字段</param>
    public void ConvertDataTableToDictionary(Dictionary<string, DataTable> dicData, DataTable dataTable, string fieldName)
    {
        int count = dataTable.Rows.Count;
        for (int i = 0; i < count; i++)
        {
            string strKey = dataTable.Rows[i][fieldName].ToString();
            DataRow[] dataRows = { dataTable.Rows[i] };
            if (dicData.ContainsKey(strKey))
            {
                DataTable data = dicData[strKey];
                data.Merge(dataRows.CopyToDataTable());
            }
            else
            {
                dicData.Add(strKey, dataRows.CopyToDataTable());
            }
        }
    }
    /// <summary>
    /// DataTable转换成字典
    /// </summary>
    /// <param name="dicData">字典数据</param>
    /// <param name="dataTable">DataTable数据</param>
    /// <param name="keyFieldName">作为Key的字段名</param>
    /// <param name="valueFieldName">作为Value的字段名</param>
    public void ConvertDataTableToDictionary(Dictionary<string, string> dicData, DataTable dataTable, string keyFieldName, string valueFieldName)
    {
        int count = dataTable.Rows.Count;
        for (int i = 0; i < count; i++)
        {
            string strKey = dataTable.Rows[i][keyFieldName].ToString();
            string strValue = dataTable.Rows[i][valueFieldName].ToString();
            if (dicData.ContainsKey(strKey))
                continue;

            dicData.Add(strKey, strValue);
        }
    }
    /// <summary>
    /// DataTable转换成字典
    /// </summary>
    /// <param name="listData">字典数据</param>
    /// <param name="dataTable">DataTable数据</param>
    /// <param name="keyFieldName">作为Key的字段名</param>
    /// <param name="valueFieldName">作为Value的字段名</param>
    public void ConvertDataTableDictionary(Dictionary<string, string> dicData, DataTable dataTable, string keyFieldName, string valueFieldName)
    {
        int count = dataTable.Rows.Count;
        for (int i = 0; i < count; i++)
        {
            string strKey = dataTable.Rows[i][keyFieldName].ToString();
            string strValue = dataTable.Rows[i][valueFieldName].ToString();
            if (!dicData.ContainsKey(strKey))
            {
                dicData.Add(strKey, strValue);
            }
            if (!dicData.ContainsKey(strValue))
            {
                dicData.Add(strValue, strValue);
            }
        }
    }
    /// <summary>
    /// 将泛型集合类转换成DataTable
    /// </summary>
    /// <typeparam name="T">集合项类型</typeparam>
    /// <param name="list">集合</param>
    /// <param name="propertyName">需要返回的列的列名</param>
    /// <returns>数据集(表)</returns>
    public DataTable ConvertListToDataTable<T>(IList<T> list, params string[] propertyName)
    {
        List<string> propertyNameList = new List<string>();
        if (propertyName != null)
            propertyNameList.AddRange(propertyName);

        DataTable result = new DataTable();

        PropertyInfo[] propertys = list.GetType().GetGenericArguments()[0].GetProperties();
        foreach (PropertyInfo pi in propertys)
        {
            Type type = pi.PropertyType;

            if (propertyNameList.Count == 0)
            {
                result.Columns.Add(pi.Name, type);
            }
            else
            {
                if (propertyNameList.Contains(pi.Name))
                    result.Columns.Add(pi.Name, type);
            }
        }
        for (int i = 0; i < list.Count; i++)
        {
            ArrayList tempList = new ArrayList();
            foreach (PropertyInfo pi in propertys)
            {
                if (pi.PropertyType.Name.ToLower() == "object")
                    continue;

                if (propertyNameList.Count == 0)
                {
                    object obj = pi.GetValue(list[i], null);
                    tempList.Add(obj);
                }
                else
                {
                    if (propertyNameList.Contains(pi.Name))
                    {
                        object obj = pi.GetValue(list[i], null);
                        tempList.Add(obj);
                    }
                }
            }
            object[] array = tempList.ToArray();
            result.LoadDataRow(array, true);
        }
        return result;
    }
    /// <summary>
    /// 将泛型集合类转换成DataTable
    /// </summary>
    /// <typeparam name="T">集合项类型</typeparam>
    /// <param name="list">集合</param>
    /// <returns>数据集(表)</returns>
    public DataTable ConvertListToDataTable<T>(IList<T> list)
    {
        return ConvertListToDataTable<T>(list, null);
    }
    /// <summary>
    /// DataTable分页
    /// </summary>
    /// <param name="dataTable">记录集 DataTable</param>
    /// <param name="PageIndex">当前页</param>
    /// <param name="pagesize">一页的记录数</param>
    /// <returns></returns>
    public DataTable SetDataTablePagination(DataTable dataTable, int PageIndex, int PageSize)
    {
        if (PageIndex == 0)
        {
            return dataTable;
        }
        //复制一份新的记录集
        DataTable newDataTable = dataTable.Clone();
        int rowBegin = (PageIndex - 1) * PageSize;
        int rowEnd = PageIndex * PageSize;
        if (rowBegin >= dataTable.Rows.Count)
        {
            return newDataTable;
        }

        if (rowEnd > dataTable.Rows.Count)
        {
            rowEnd = dataTable.Rows.Count;
        }
        for (int i = rowBegin; i <= rowEnd - 1; i++)
        {
            DataRow newDataRow = newDataTable.NewRow();
            DataRow dataRow = dataTable.Rows[i];
            foreach (DataColumn column in dataTable.Columns)
            {
                newDataRow[column.ColumnName] = dataRow[column.ColumnName];
            }
            newDataTable.Rows.Add(newDataRow);
        }

        return newDataTable;
    }


    

}