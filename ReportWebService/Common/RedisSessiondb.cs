﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


    public class RedisSessiondb
    {
        private HttpContext context;

        /// <summary>
        /// RedisSession
        /// </summary>
        /// <param name="context"></param>
        /// <param name="isReadOnly">是否只读</param>
        /// <param name="sourceName">设备类型</param>
        /// <param name="timeout">单位：分钟</param>
        public RedisSessiondb( bool isReadOnly, string sourceName, int timeout = 0, string key_v = "")
        {
            //this.context = context;
            this.IsReadOnly = isReadOnly;
            if (!sourceName.IsNullOrEmpty() && sourceName.Contains("APP"))
            {
                if (timeout == 0)
                {
                    timeout = 720;
                }
            }
            else
            {
                if (timeout == 0)
                {
                    timeout = 720;
                }
            }
            this.Timeout = timeout;
            this.SourceName = sourceName;
            //更新缓存过期时间
            key = key_v;
           // RedisBasedb.Hash_Set<object>(key, name, value);
            RedisBasedb.Hash_SetExpire(key_v, DateTime.Now.AddMinutes(Timeout));
        }

        public void SetRedisTimeout(string key_v, int timeout = 0)
        {
            RedisBasedb.Hash_SetExpire(key_v, DateTime.Now.AddMinutes(timeout));
        }

        public string key;
        //public string key_v
        //{
        //    get
        //    {
        //        string sessionKey = SessionKey;
        //        string ivk = IVKey;
        //        return "Session_" + sessionKey;
        //    }
        //    set
        //    {
        //        return;
        //      // key = key_v;
        //    }
        //}
        public const string SessionName = "C_SessionId";

        public const string IVKeyName = "IVK";

        public string SessionId
        {
            get
            {
                string sessionKey = SessionKey;
                string ivk = IVKey;

                return "Session_" + sessionKey;
                //return key;
            }
        }
        public string SessionKey
        {
            get
            {
                string sessionKey = string.Empty;
                HttpCookie sessionCookie = context.Request.Cookies.Get(SessionName);
                if (sessionCookie != null)
                {
                    sessionKey = sessionCookie.Value;
                }
                if (string.IsNullOrEmpty(sessionKey))
                {
                    sessionKey = Guid.NewGuid().ToString();
                    sessionCookie = new HttpCookie(SessionName);
                    sessionCookie.Value = sessionKey;
                    sessionCookie.HttpOnly = IsReadOnly;
                    context.Response.Cookies.Add(sessionCookie);
                }
                return sessionKey;
                // return key;
            }
            set
            {
                HttpCookie cookie = new HttpCookie(SessionName);
                //cookie.Value = Guid.NewGuid().ToString();
                cookie.Value = value;
                cookie.HttpOnly = IsReadOnly;
                context.Response.Cookies.Add(cookie);
            }
        }

        public string IVKey
        {
            get
            {
                string ivk = string.Empty;
                HttpCookie ivKeyCookie = context.Request.Cookies.Get(IVKeyName);
                if (ivKeyCookie != null)
                {
                    ivk = ivKeyCookie.Value;
                }
                if (string.IsNullOrEmpty(ivk))
                {
                    ivk = "";//Common.Utils.GenIVKey(SessionKey);
                    ivKeyCookie = new HttpCookie(IVKeyName);
                    ivKeyCookie.Value = ivk;
                    ivKeyCookie.HttpOnly = false;
                    context.Response.Cookies.Add(ivKeyCookie);
                }
                return ivk;
            }
            set
            {
                HttpCookie cookie = new HttpCookie(IVKeyName);
                //cookie.Value = Common.Utils.GenIVKey(SessionKey);
                cookie.Value = value;
                cookie.HttpOnly = false;
                context.Response.Cookies.Add(cookie);
            }
        }

        //public int Count
        //{
        //    get
        //    {
        //        return RedisBasedb.Hash_GetCount(SessionId);
        //    }
        //}

        public bool IsReadOnly { get; set; }

        /// <summary>
        /// Device Type, ex: PC / MB / APP etc...
        /// </summary>
        public string SourceName { get; set; }

        public int Timeout { get; set; }

        //TTL不設=-1
        public object this[string name]
        {
            get
            {
                return RedisBasedb.Hash_GetObj<object>(key, name);
            }
            set
            {
                RedisBasedb.Hash_Set<object>(key, name, value);
            }
        }

        public bool IsExistKey(string name)
        {
            return RedisBasedb.Hash_Exist<object>(key, name);
        }
        public List<string> Hash_Value(string hash_key)
        {
            return RedisBasedb.Hash_GetAll<string>(hash_key).ToList();
        }

        //獲取hashkey全部datakey  轉 list
        public List<string> Hash_AllKey()
        {
        return RedisBasedb.Hash_GetAll<string>().ToList();
        }
      //獲取hashkey全部datakey  轉 list
        public List<string> Hash_Key(string key)
        {
        return RedisBasedb.Hash_GetAll<string>(key).ToList();
        }

        public void Add(string name, object value)
        {
            RedisBasedb.Hash_Set<object>(key, name, value, DateTime.Now.AddMinutes(Timeout));
        }

        public void Clear()
        {
            RedisBasedb.Hash_Remove(SessionId);

            string guid = Guid.NewGuid().ToString();
            SessionKey = guid;
           // IVKey = Common.Utils.GenIVKey(guid);
        }
        //指定redis key和 datakey
        public void Remove(string SessionId, string name)
        {
            RedisBasedb.Hash_Remove(SessionId, name);
        }
    }
