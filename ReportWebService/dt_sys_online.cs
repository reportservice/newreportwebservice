﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


    public class dt_sys_online
    {
        public int id
        {
            get;
            set;
        }

        public string identityid
        {
            get;
            set;
        }

        public bool is_unlok
        {
            get;
            set;
        }

        public string login_addr
        {
            get;
            set;
        }

        public string login_ip
        {
            get;
            set;
        }

        public DateTime login_time
        {
            get;
            set;
        }

        public string SourceName
        {
            get;
            set;
        }

        public int status
        {
            get;
            set;
        }

        public int user_id
        {
            get;
            set;
        }

        public string user_name
        {
            get;
            set;
        }

        public DateTime oper_time
        {
            get;
            set;
        }

        public string x_sec
        {
            get;
            set;
        }
    }
