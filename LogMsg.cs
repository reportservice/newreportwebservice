﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;


public class LogMsg
{

    public void MsgRun(string functionName, string IdentityId, string StartTime, string EndTime, string cs)
    {
        string logRoad = "C:\\LOGTXT\\" + System.DateTime.Now.ToString("yyyy-MM-dd") + "runlog.txt";//错误日志记录路径
        if (!File.Exists(logRoad))
        {
            FileStream fs1 = new FileStream(logRoad, FileMode.Create, FileAccess.Write);//创建写入文件 
            StreamWriter sw = new StreamWriter(fs1);
            sw.WriteLine(System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ":" + functionName + ":" + "查詢:" + IdentityId + StartTime + "~" + EndTime + "   ;" + cs);
            sw.WriteLine();
            sw.WriteLine();
            sw.Close();
            fs1.Close();
        }
        else
        {
            FileStream fs = new FileStream(logRoad, FileMode.Append, FileAccess.Write);
            StreamWriter sr = new StreamWriter(fs);
            sr.WriteLine(System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ":" + functionName + ":" + "查詢:" + IdentityId + StartTime + "~" + EndTime + "   ;" + cs);
            sr.WriteLine();
            sr.WriteLine();
            sr.Close();
            fs.Close();
        }
    }
    /// <summary>
    /// 创建错误日志
    /// </summary>
    /// <param name="path">日志地址 在当前工作目录+ErrorLog下</param>
    /// <param name="strPort">接口</param>
    /// <param name="strCostTime">響應耗時</param>
    /// <param name="strPara">參數內容</param>
    public void CreateErrorLogTxt(string portName, string strPort, string strCostTime, string strPara)
    {
        string strPath; //错误文件的路径
        DateTime dt = DateTime.Now;
        try
        {
            //工程目录下 创建日志文件夹 
            //strPath = Thread.GetDomain().BaseDirectory + "\\RESPONSETIME\\" + portName;
            strPath = "D:\\webService\\RESPONSETIME\\" + portName;
            if (Directory.Exists(strPath) == false) //工程目录下 Log目录 '目录是否存在,为true则没有此目录
            {
                Directory.CreateDirectory(strPath); //建立目录　Directory为目录对象
            }
            strPath = strPath + "\\" + dt.ToString("yyyyMM");

            if (Directory.Exists(strPath) == false) //目录是否存在  '工程目录下 Log\月 目录   yyyymm
            {
                Directory.CreateDirectory(strPath); //建立目录//日志文件，以 日 命名 
            }
            strPath = strPath + "\\" + dt.ToString("dd") + ".log";
            StreamWriter fileWriter = new StreamWriter(strPath, true); //创建日志文件
            fileWriter.Write("觸發時間:" + dt.ToString("HH:mm:ss.fff"));
            fileWriter.Write(" 接口:" + strPort);
            fileWriter.Write(" 響應耗時:" + strCostTime);
            fileWriter.WriteLine(" 參數內容:" + strPara);
            fileWriter.Close(); //关闭StreamWriter对象
        }
        catch (Exception ex)
        {
            strPara += "--" + ex.Message;
        }
    }

}
