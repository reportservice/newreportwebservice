﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
///代理报表
/// </summary>
public class ProfitLossData
{

    public string Balance//:1000,//余额
    {
        get;
        set;
    }

    public string AllProfitLoss//:200,//盈亏总额
    {
        get;
        set;
    }
    public string Betting//:1000,//投注金额
    {
        get;
        set;
    }
    public string BonusMoney//:300,//中奖金额
    {
        get;
        set;
    }
    public string Activity//:100,//活动金额
    {
        get;
        set;
    }
    public string Rebate//:52,//返点金额
    {
        get;
        set;
    }
    public string Recharge//2000,//充值金额
    {
        get;
        set;
    }
    public string Withdraw//500,//提现金额
    {
        get;
        set;
    }
}